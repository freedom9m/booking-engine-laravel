<?php 
namespace App\API;

/**
* 
*/
use Config;
class RateChecker
{
	function __construct($option)
	{
        if(isset($option['agodaLink']) && $option['agodaLink']!=''){
            $this->agodaLink = $option['agodaLink'];
        }
        if(isset($option['bookingLink']) && $option['bookingLink']!=''){
            $this->bookingLink = $option['bookingLink'];
        }
        if(isset($option['expediaLink']) && $option['expediaLink']!=''){
            $this->expediaLink = $option['expediaLink'];
        }

		$this->startDate = $option['rateDate'];
		$this->endDate = $option['endDate'];
        $this->noOfNight = \App\Components\Functions::dateDiff($this->startDate, $this->endDate);

        $this->connection = \DB::connection('hotelDB')->getPDO();
	}


    public function booking()
    {
        if(!isset($this->bookingLink)){
            return false;            
        }
    }


	public function agoda()
	{
        $stmt = $this->connection->prepare('set @noOfNight = '.$this->noOfNight)->execute();
        $stmt = $this->connection->prepare('SELECT 
                roomtype.roomtypeid, ratewebsite.*
            FROM
                roomtype
                    LEFT JOIN
                ratemapping ON ratemapping.roomtypeid = roomtype.roomtypeid
                    INNER JOIN
                (select SUM(sellprice)/@noOfNight AS price, idroomtype, ratekey from ratewebsite where ratedate between :startDate and :endDate group by idroomtype having count(*) = @noOfNight) as ratewebsite ON ratewebsite.idroomtype = ratemapping.idroomtype and ratewebsite.ratekey = ratemapping.ratekey
            WHERE
                ratewebsite.ratekey = :keyVal
            GROUP BY ratemapping.idroomtype');
        $stmt->execute([
            ':startDate' => $this->startDate,
            ':endDate' => $this->endDate,
            ':keyVal' => 'agoda'
        ]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAgoda()
    {
        if(!isset($this->agodaLink)){
            return false;            
        }
        $rate = [];
        $begin = new \DateTime( $this->startDate );
        $end = new \DateTime( $this->endDate );

        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt ){
            $rateDate = $dt->format('Y-m-d');
            /*$rateCheckerModel = \App\Models\RateWebsite::with(['rateMapping' => function($query){
                $query->where('ratekey', 'agoda');
                //$query->where('roomtypeid', $roomtyepid);
            }])->where('ratedate', $rateDate)->first();*/
            
            $html = new \App\Components\HtmlDom\HtmlDom($this->agodaLink.'?pingnumber=1&checkin='.$rateDate.'&los=1&adults=2&childs=0&rooms=1'); //los is night
            foreach($html->find('tr.room-type') as $article) :
                if(\App\Models\RateWebsite::where('ratekey', 'agoda')->where('idroomtype', $article->getAttribute('data-id'))->where('ratedate', $rateDate)->count() == 0){
                    $model = new \App\Models\RateWebsite;
                    $model->ratekey         = 'agoda';
                    $text = trim($article->find('.eng-room-name', 0)->plaintext);
                    $model->roomtypename    = substr($text, 1, strlen($text)-2);
                    $model->idroomtype      = $article->getAttribute('data-id');
                    $model->sellprice       = str_replace(',', '', $article->find('.sellprice', 0)->plaintext) * Config::get('hotelModel')->vat;
                    $model->crossout        = str_replace(',', '', $article->find('.crossout', 0)->plaintext) * Config::get('hotelModel')->vat ;
                    $model->currency        = trim(str_replace(',', '', $article->find('.currency', 0)->plaintext)) ;
                    //$dataArray['maxadult']     = $article->find('.occupancy', 0)->getAttribute('data-max');
                    $model->ratedate        = $rateDate;
                    $i=0;
                    foreach($article->find('.select-occupancy', 0)->find('option') as $option):
                      $i++;
                    endforeach;
                    $totalavailable = $i-1;

                    //if($dataArray['roomname_en'] == 'A - Two Cube')
                    //echo ("</br>".$ratedate." - ".$dataArray['sellprice']." - ". $totalavailable);

                    $model->available       = $totalavailable;

                    $model->save();
                }
            endforeach;

        }
    }
}