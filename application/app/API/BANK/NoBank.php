<?php 
namespace App\API\BANK;

/**
* 
*/
class NoBank extends LoadAPI
{
	public $loadAPI = false;
	public $apiPath = null;
	public $bankUrl = null;
	public $testUrl = null;
	public $formName = 'bookingForm';
	public $fields = [];
	public $fieldsRequire = [];
	public $moneyField = null;
	public $refField = null;

}