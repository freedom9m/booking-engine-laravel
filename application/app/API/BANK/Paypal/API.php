<?php 
namespace App\API\BANK\Paypal;

use \DB;
use App\Components\Functions;
use \App\API\BANK\Paypal\PAYPAL;
/**
* 
*/
class API extends \App\API\BANK\LoadAPI
{
	public $loadAPI = true;
	public $apiPath = '\App\API\BANK\PAYPAL';
	public $bankUrl = null;
	public $testUrl = null;
	public $formName = 'paypalForm';
	public $fields = [
		'account',
		'item_name',
		'amount',
		'currency'
	];
	public $fieldsRequire = [
		'account',
		'amount'
	];
	public $moneyField = 'amount';
	public $refField = 'item_name';


	public function runAPI()
	{
		$api = new PAYPAL();
		$api->admin_mail = $this->account;
        $invoiceid = \Input::get($this->refField);
        $amount = \Input::get($this->moneyField);
        $this->{$this->refField} = $invoiceid;
        $currency = $this->currency;
        $api->add_field('business', $this->account); //don't need add this item. if your set the $api->paypal_mail.
        $api->add_field('return', $this->successUrl());
        $api->add_field('cancel_return', $this->cancelUrl());
        $api->add_field('notify_url', $this->serviceUrl());
        $api->add_field('item_name', 'REFERENCE ID:'.$invoiceid);
        $api->add_field('cmd', '_xclick');
        $api->add_field('amount', number_format($amount, 2, '.', ''));
        $api->add_field('currency_code', $currency);
        $api->add_field('rm', '2'); // Return method = POST
        if(\Config::has('params.paypalPartnerCode')){
        	$api->add_field('bn', \Config::get('params.paypalPartnerCode')); // partner code
        }
        $api->submit_paypal_post(); // submit the fields to paypal
	}

	public function responseService()
	{
		$referenceId = \Input::get('referenceId');
		if(\Input::get('payment_status') == 'Completed'/* || \Input::get('st') == 'Completed'*/){
			return ['referenceId' => $referenceId, 'data' => md5('success_'.$referenceId)];
		}
	}

	public function dataFeedService()
	{
		return $this->responseService();
	}
}