<?php 
namespace App\API\BANK;
use \Crypt;
use \App\Models\BookingReference;
/**
* 
*/
class LoadAPI
{
	public $fields = [];
	public $fieldsRequire = [];
	public $noShowFields = [];

	function __construct()
	{
		if(isset($this->fields)){
			foreach($this->fields as $key){
				$this->{$key} = null;
			}
		}
	}

	public function checkField()
	{
		foreach($this->fieldsRequire as $field)
		{
			if($this->{$field} == null)
			{
				return \Redirect::to('configRequire')->send();
			}
		}
	}

	public function successUrl()
	{
		$dataEncrypt = $this->encryptData('success');
		return url('reservation/'.\Config::get('slug')).'/bankResponse?data='.$dataEncrypt;
	}

	public function failUrl()
	{
		$dataEncrypt = $this->encryptData('failure');
		return url('reservation/'.\Config::get('slug')).'/bankResponse?data='.$dataEncrypt;
	}

	public function cancelUrl()
	{
		$dataEncrypt = $this->encryptData('cancel');
		return url('reservation/'.\Config::get('slug')).'/bankResponse?data='.$dataEncrypt;
	}

	public function otherUrl()
	{
		$dataEncrypt = $this->encryptData('other');
		return url('reservation/'.\Config::get('slug')).'/bankResponse?data='.$dataEncrypt;
	}

	public function serviceUrl()
	{
		$referenceId = $this->{$this->refField};
		return url('reservation/'.\Config::get('slug')).'/service?referenceId='.$referenceId;
	}	

	protected function encryptData($code)
	{
		$referenceId = $this->{$this->refField};
		$data = md5($code.'_'.$referenceId).'&referenceId='.$referenceId;
		return $data;
	}
}