<?php 
namespace App\API\BANK\Krungsri;

use \DB;
use App\Components\Functions;
use \Input;
/**
* 
*/
class API extends \App\API\BANK\LoadAPI
{
	public $loadAPI = true;
	public $apiPath = null;
	public $bankUrl = 'https://secureacceptance.cybersource.com/pay';
	public $testUrl = 'https://testsecureacceptance.cybersource.com/pay';
	public $formName = 'payFormCcard';
	public $fields = [
		'org_id',
		'e_merchant',
		'access_key',
		'profile_id',
		'secret_key',
		'reference_number',
		'currency',
		'amount',
		'transaction_type',
	];
	public $fieldsRequire = [
		'org_id',
		'e_merchant',
		'access_key',
		'profile_id',
		'secret_key',
		'reference_number',
		'amount',
		'currency',
		'transaction_type',
	];

	public $moneyField = 'amount';
	public $refField = 'reference_number';

	function sign ($params) {
	  return $this->signData($this->buildDataToSign($params), $this->secret_key);
	}

	function signData($data, $secretKey) {
	    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
	}

	function buildDataToSign($params) {
	        $signedFieldNames = explode(",", $params["signed_field_names"]);
	        foreach ($signedFieldNames as $field) {
	           $dataToSign[] = $field . "=" . $params[$field];
	        }
	        return $this->commaSeparate($dataToSign);
	}

	function commaSeparate ($dataToSign) {
	    return implode(",",$dataToSign);
	}

	public function generateAutoFields($data)
	{
		echo '<input type="hidden" name="transaction_uuid" value="'.uniqid().'">';
		echo '<input type="hidden" name="signed_field_names" value="access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency">';
		echo '<input type="hidden" name="unsigned_field_names">';
		echo '<input type="hidden" name="signed_date_time" value="'.gmdate("Y-m-d\TH:i:s\Z").'">';
		echo '<input type="hidden" name="locale" value="en">';
	}
	
	public function runAPI()
	{
		echo "<html>\n";
		echo "<head><title>Processing Payment...</title></head>\n";
		echo "<body onLoad=\"document.forms['payment_confirmation'].submit();\">\n";

		echo '<form name="payment_confirmation" action="'.$this->bankUrl.'" method="post"/>';
		$params = [];
	    foreach($_REQUEST as $name => $value) {
	        $params[$name] = $value;
	    }
		/*echo '<fieldset id="confirmation">
		    <legend>Review Payment Details</legend>
		    <div>';
            foreach($params as $name => $value) {
                echo "<div>";
                echo "<span class=\"fieldName\">" . $name . "</span><span class=\"fieldValue\">" . $value . "</span>";
                echo "</div>\n";
            }
		echo '</div>
		</fieldset>';*/
	        foreach($params as $name => $value) {
	            echo "<input type=\"hidden\" id=\"" . $name . "\" name=\"" . $name . "\" value=\"" . $value . "\"/>\n";
	        }
	        echo "<input type=\"hidden\" id=\"signature\" name=\"signature\" value=\"" . $this->sign($params) . "\"/>\n";
		//echo '<input type="submit" id="submit" value="Confirm"/>';
			echo "<center><br/><br/>If you are not automatically redirected to ";
			echo "Krungsri Bank within 5 seconds...<br/><br/>\n";
			echo "<input type=\"submit\" value=\"Click Here\"></center>\n";
		echo '</form>';

		echo "</body></html>\n";

	}

	public function responseService()
	{
		$referenceId = Input::has('req_reference_number') ? Input::get('req_reference_number') : null;
		$reasonCode = Input::has('reason_code') ? Input::get('reason_code') : null;
		if(in_array($reasonCode, ['100', '110'])){
			$code = 'success';
		}else{
			$code = 'failure';
		}
		return ['referenceId' => $referenceId, 'data' => md5($code.'_'.$referenceId)];
	}

	public function dataFeedService()
	{
		return $this->responseService();
	}
}