<?php 
namespace App\API\BANK\Bangkok;

use \DB;
use App\Components\Functions;
/**
* 
*/
class API extends \App\API\BANK\LoadAPI
{
	public $loadAPI = false;
	public $apiPath = null;
	public $bankUrl = 'https://ipay.bangkokbank.com/b2c/eng/payment/payForm.jsp';
	public $testUrl = '';
	public $formName = 'payFormCcard';
	public $fields = [
		'merchantId',
		'amount',
		'orderRef',
		'currCode',
		'payType',
		'lang',
		'remark',
		'redirect',
		'orderRef1',
		'orderRef2',
		'orderRef3',
		'orderRef4',
	];
	public $fieldsRequire = [
		'merchantId',
		'amount',
		'orderRef',
		'currCode',
		'payType',
		'lang',
		'remark',
		'redirect',
	];

	public $moneyField = 'amount';
	public $refField = 'orderRef';

	public function generateAutoFields($data)
	{
        echo '<input type="hidden" name="successUrl" value="'.$this->successUrl().'">'.PHP_EOL;
        echo '<input type="hidden" name="failUrl" value="'.$this->failUrl().'">'.PHP_EOL;
        echo '<input type="hidden" name="cancelUrl" value="'.$this->cancelUrl().'">'.PHP_EOL;
	}

	public function dataFeedService()
	{
		$referenceId = \Input::get('Ref');
		if(\Input::get('successcode') == '0'){
			return ['referenceId' => $referenceId, 'data' => md5('success_'.$referenceId)];
		}
	}

}