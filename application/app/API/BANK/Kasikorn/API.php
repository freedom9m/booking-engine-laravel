<?php 
namespace App\API\BANK\Kasikorn;

use \DB;
use App\Components\Functions;
use \Input;
/**
* 
*/
class API extends \App\API\BANK\LoadAPI
{
	public $loadAPI = false;
	public $apiPath = null;
	public $bankUrl = 'https://rt05.kasikornbank.com/pgpayment/payment.aspx';
	public $testUrl = '';
	public $formName = 'payFormCcard';
	public $fields = [
		'MERCHANT2',
		'TERM2',
		'AMOUNT2',
		// 'URL2',
		// 'RESPURL',
		//'IPCUST2',
		//'DETAIL2',
		'INVMERCHANT',
		'FILLSPACE',
		//'CHECKSUM',
		'CHECKSUM_MD5',
	];
	public $fieldsRequire = [
		'MERCHANT2',
		'TERM2',
		'AMOUNT2',
		// 'URL2',
		// 'RESPURL',
		//'IPCUST2',
		//'DETAIL2', //product description
		'INVMERCHANT',
		'FILLSPACE',
		//'CHECKSUM',
		'CHECKSUM_MD5',
	];
	public $noShowFields = [
		'CHECKSUM_MD5',
		// 'URL2',
		'AMOUNT2',
		// 'RESPURL',
		//'DETAIL2',
	];

	public $moneyField = 'AMOUNT2';
	public $refField = 'INVMERCHANT';

	public function generateAutoFields($data)
	{
		$totalAmount = $this->AMOUNT2*100;
		$serverIP = $_SERVER['SERVER_ADDR'];
		$returnData = base64_encode(json_encode($data));
		$dataIntegrate = $this->MERCHANT2;
		$dataIntegrate .= $this->TERM2;
		$dataIntegrate .= $totalAmount;
		$dataIntegrate .= $this->successUrl();
		$dataIntegrate .= $this->serviceUrl();

		$dataIntegrate .= $serverIP;
		$itemName = 'Reservation ReferenceID : '.$data['referenceId'];
		$dataIntegrate .= $itemName;
		$dataIntegrate .= $data['referenceId'];
		$dataIntegrate .= $this->FILLSPACE;
		$dataIntegrate .= $this->CHECKSUM_MD5;
		$dataEncoded  = md5($dataIntegrate);

        echo '<input type="hidden" name="AMOUNT2" value="'.$totalAmount.'">'.PHP_EOL;
        echo '<input type="hidden" name="IPCUST2" value="'.$serverIP.'">'.PHP_EOL;
        echo '<input type="hidden" name="DETAIL2" value="'.$itemName.'">'.PHP_EOL;
        echo '<input type="hidden" name="URL2" value="'.$this->successUrl().'">'.PHP_EOL;
		echo '<input type="hidden" name="RESPURL" value="'.$this->serviceUrl().'">'.PHP_EOL;
        echo '<input type="hidden" name="CHECKSUM" value="'.$dataEncoded.'">'.PHP_EOL;

	}

	public function responseService()
	{
		$referenceId = Input::get('referenceId');
		$responseCode = Input::get('HOSTRESP');
		if(in_array($responseCode, ['00'])){
			$code = 'success';
		}else{
			$code = 'failure';
		}
		return ['referenceId' => $referenceId, 'data' => md5($code.'_'.$referenceId)];
	}

	public function dataFeedService()
	{
		return $this->responseService();
	}

}