<?php

namespace App\Helpers;

use \Request;

class Menu  
{
    public static function active($page)
    {

        if (Request::is($page))
            return 'active';

        return '';
    }
}