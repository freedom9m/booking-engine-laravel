<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Config;
use Request;
use Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        Log::info($e->getMessage(), [
            'url' => Request::url(),
            'input' => Request::all()
        ]);
        if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException){
            return response(view('errors.404'), 404);
        }
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if(Config::has('hotelModel') && Config::get('hotelModel')->debugMode != 1){ // if enable Debug Mode show all error exception
            if($e instanceof NotFoundHttpException)
            {
                return response()->view('errors/404');
            }

            elseif ($e instanceof ErrorException) {
                return response()->view('errors/404');
            }

            elseif ($e instanceof ModelNotFoundException) {
                return response()->view('errors/404');
                //return response()->view('error_log(message)ors/404');
            }

            elseif($e instanceof QueryException)
            {
                return response(view('errors.databaseErr'), 404);
            }
            
            if ($e instanceof \PDOException){
                return response(view('errors.databaseErr'), 404);
            }
        }
        return parent::render($request, $e);
    }
}
