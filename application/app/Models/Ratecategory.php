<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ratecategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ratecategory';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at
}