<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomGuest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'room_guest';

    protected $connection = 'hotelDB';

    public $timestamps = false;
}