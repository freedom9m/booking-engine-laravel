<?php 
namespace models;
/**
* 
*/
class CreditForm extends \app\Model
{
	

    public function rules()
    {
        return [
            
            
            
        ];
    }

    public function attributeLabels()
    {
        return [
            'cardHolder'    => 'Card holder',
            'cardNo'        => 'Card no',
			'cardType'	    => 'Card type',
            'expireMonth'    => 'Expire month',
            'expireYear'    => 'Expire year',
            'cvvCode'       => 'CVV',
            'securityCode'  => 'Security code',
            'referenceId'    => 'Reference ID',
            'contactName'   => 'Contact Name',
            'contactEmail'  => 'Contact Email',
            'summaryPrice'  => 'Price',

        ];
    }
}