<?php 
namespace models;
/**
* 
*/
class ConfirmForm extends \app\model
{

    public function rules()
    {
        return [
            
        ];
    }

    public function attributeLabels()
    {
        return [
        	'referenceId'	=> 'Reference ID',
        	'contactName'	=> 'Contact Name',
        	'contactEmail'	=> 'Contact Email',
        	'summaryPrice'	=> 'Price',
        ];
    }

    public function relation()
    {
        return [
        ];
    }}