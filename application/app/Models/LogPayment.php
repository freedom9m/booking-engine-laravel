<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogPayment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'log_payment';

    protected $primaryKey = 'idlog_payment';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at

}