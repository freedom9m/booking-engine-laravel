<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogControl extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logcontrol';

    protected $primaryKey = 'idlogcontrol';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at

}