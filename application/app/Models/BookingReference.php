<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingReference extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'booking_reference';

    protected $primaryKey = 'referenceid';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at

}