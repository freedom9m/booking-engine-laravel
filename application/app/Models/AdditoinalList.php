<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditoinalList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'additional_list';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at
}