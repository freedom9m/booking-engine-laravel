<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at
}