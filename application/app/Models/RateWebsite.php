<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RateWebsite extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ratewebsite';

    protected $primaryKey = 'idratewebsite';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at

    public function rateMapping()
    {
    	return $this->hasOne('App\Models\RateMapping', 'idroomtype', 'idroomtype');
    }

}