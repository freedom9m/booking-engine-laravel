<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankApiList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bankApiList';

    protected $primaryKey = 'idbankApiList';

    //protected $connection = '';

    public $timestamps = false; // using for columns created_at, updated_at
}