<?php
namespace models; 
/**
* 
*/
class ContactForm extends \app\Model
{
	
    public function rules()
    {
        return [
            [['bugScreenShot'], 'file']
        ];
    }

    public function attributeLabels()
    {
        return [
			'name'			=> 'Name',
			'email'			=> 'Email',
			'subject'		=> 'Subject',
			'body'			=> 'Body',
			'bugPath'		=> 'Bug Path',
			'bugParameters'	=> 'Bug Parameters',
			'bugDetails'	=> 'Bug Details',
			'bugScreenShot' => 'Bug ScreenShot',
        ];
    }


    public function sendMail()
    {
    	if(isset($_POST['contactform'])){
    		$data = $_POST['contactform'];
    		$this->dataCombine($data);
			$mail = new \app\Mailing;
			$params = \app\Base::app()->params;
			$mail->addAddress($params['contactEmail'], $params['contactName']);     // Add a recipient
			$mail->setFrom($this->email, $this->name);

			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $this->subject;
			$mail->Body    = $this->body;

			if(isset($data['bugPath']) && isset($data['bugParameters']) && isset($data['bugDetails']))
			{
				$mail->Body .= '<hr>';
				$mail->Body .= 'Path : ' . $this->bugPath . '<br>';
				$mail->Body .= 'Parameters : ' . $this->bugParameters . '<br>';
				$mail->Body .= 'Details : ' . $this->bugDetails . '<br>';
				if(isset($_FILES['bugScreenShot']) && $_FILES['bugScreenShot']['error'] == UPLOAD_ERR_OK){
					$upload = \app\File::uploadFile($_FILES['bugScreenShot'], \app\Base::app()->webPath.'/screenShot/', ['jpg', 'jpeg', 'png', 'gif', 'gift'], $mail);
					if($upload == 1){

					}else{

					}
				}
			}

			if(!$mail->send()) {
			    \app\Session::set('contact', false);
			} else {
			    \app\Session::set('contact', true);
				\app\Base::redirect($_SERVER["REQUEST_URI"]);
			}
    	}else{
    	}

    }
}