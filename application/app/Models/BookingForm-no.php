<?php 
namespace models;
/**
* 
*/
class BookingForm extends \app\Model
{

    public function rules()
    {
        return [
            
            [['checkindate', 'checkoutdate', 'adult'], 'required'],
            [['checkindate', 'checkoutdate'], 'safe'],
            [['adult', 'child'], 'integer', 'max' => 11],
            [['promotioncode'], 'string', 'max' => 255],
            //[['email'], 'email'],
            
        ];
    }

    public function attributeLabels()
    {
        return [
            'checkindate' => 'Check in',
            'checkoutdate' => 'Check out',
            'adult' => 'Adult',
            'child' => 'Child',
            'promotioncode' => 'Promotion Code',
        ];
    }

}
 ?>