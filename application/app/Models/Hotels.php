<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotels extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hotellist';

    //protected $connection = '';

    public $timestamps = false; // using for columns created_at, updated_at

    public function bankApi()
    {
    	return $this->hasOne('App\Models\BankApiList', 'idbankApiList', 'idbankApiList');
    }

    public function bankApiList()
    {
    	return $this->hasOne('App\Models\BankApiList', 'idhotel', 'id');
    }
}