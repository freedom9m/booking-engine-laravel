<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
	public $incrementing = false; 
	//use ModelEventLogger;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'booking';

    protected $connection = 'hotelDB';

    public $timestamps = false; // using for columns created_at, updated_at
}