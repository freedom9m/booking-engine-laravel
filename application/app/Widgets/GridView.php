<?php 
namespace App\Widgets;

use App\Components\Functions;
use \View;
/**
* 
*/
class GridView
{
	public static $separator, $option, $data, $template, $itemView, $viewData, $widgetId;
	public static function run($option)
	{

		self::$data = $option['data'];
		self::$template = isset($option['template'])?$option['template']:'<div id="{{widgetId}}">'.
			'<div class="summary">{{summary}}</div>'.
			'<div class="items">{{items}}</div>'.
		'</div>';
		self::$separator = isset($option['separator'])?$option['separator']:"\n";
		self::$itemView = $option['itemView'];
		self::$widgetId = isset($option['id'])?$option['id']: Functions::generateRandomString();
        self::$viewData = isset($option['viewData'])?$option['viewData'] : null;
		unset($option);
		$data = self::renderItems();
		return Functions::setTemplate(self::$template, $data);
	}

	public static function renderItems()
	{
		$widgetId = self::$widgetId;
		$summary = 'total '.count(self::$data).' items.';
        $rows = [];
        foreach (array_values(self::$data) as $index => $data) {
            $rows[] = self::renderItem($data, $index);
        }

        return [
        	'widgetId'	=> $widgetId,
        	'summary'	=> $summary,
        	'items'		=> implode(self::$separator, $rows)
        ];
	}

    public static function renderItem($data, $index)
    {
    	// check is function is_callable(self::$itemView)
        if (self::$itemView === null) {
            $content = $index;
        } elseif (is_string(self::$itemView)) {
            $content = View::make(self::$itemView, [
                'data' => $data,
                'index' => $index,
                'widget' => self::$viewData
            ])->render();
        } else {
            $content = call_user_func(self::$itemView, $data, $index, self::$viewData);
        }

        return $content;
    }


}