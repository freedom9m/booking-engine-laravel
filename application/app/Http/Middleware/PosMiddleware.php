<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use Route;
use Debugbar;
use Redirect;

class BookingEngineMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $slug = Route::current()->getParameter('slug'); //รับค่า parameter hotelId จาก route

        $debug = substr($slug, -5, 5);

        if($debug == 'debug'){
            $slug = substr($slug, 0, -5);
        }

        $hotelModel = DB::connection('mysql')->table('hotellist')->where('slug', $slug)->where('bookingEnabled', 1)->first(); //ดึงข้อมูลการเชื่อมต่อ Hotel จากการเชื่อมต่อปกติ

        Config::set('slug', Route::current()->getParameter('slug'));

        if($hotelModel != null && $hotelModel->debugMode == 1){
            if(Route::current()->getParameter('slug') == $hotelModel->slug){
                return Redirect::to('hotel/'.$hotelModel->slug)->send();
            }else{
                Debugbar::enable();
            }
        }else{
            if($hotelModel != null && Route::current()->getParameter('slug') != $hotelModel->slug){
                $hotelModel = null;
            }
            Debugbar::disable();
        }



        if($hotelModel === null && Route::getCurrentRoute()->getPath()!='notFound') { //ถ้าข้อมูล hotel ไม่มี และ route ไม่ตรงกับ notFound ให้วิ่งไปหน้า notFound
            return Redirect::to('notFound')->send();
        }

        $hotelId = $hotelModel->id;

        /*สร้างการเชื่อมต่อฐานข้อมูลใหม่ในชื่อ hotelDB โดยใช้ข้อมูลการเชื่อมต่อจากฐานข้อมูล*/
        $conn = array(
            'driver'    => 'mysql',
            'host'      => $hotelModel->host_database,
            'database'  => $hotelModel->database_name,
            'username'  => $hotelModel->username_database,
            'password'  => $hotelModel->password_database,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        );

        Config::set('hotelModel', $hotelModel);

        Config::set('imgPath', $hotelModel->database_name);

        Config::set('database.connections.hotelDB', $conn);

        Config::set('pathName', $hotelModel->database_name);

        Config::set('template', $hotelModel->template);

        /*if(!DB::connection('hotelDB')->getDatabaseName()) { //ถ้าเชื่อมต่อฐานข้อมูลโรงแรมไม่ได้ให้วิ่งไปหน้า databaseError
            return Redirect::to('databaseErr')->send();
        }*/

        $currency = Config::get('currency.'.$hotelModel->currency);

        Config::set('params.currencyCode', $currency['currencyCode']);

        Config::set('params.currencySign', $currency['currencySign']);

        if(\Input::has('locale')){
            $language = \Input::get('locale');
            $rules = [
                'language' => 'in:en,th' //list of supported languages of your application.
            ];


            $validator = \Validator::make(compact($language),$rules);

            if($validator->passes())
            {
                \Session::put('language',$language);
                \App::setLocale($language);
            }
        }

        if(\Session::has('language')){
            \App::setLocale(\Session::get('language'));
        }

        $printSettingModel = DB::connection('hotelDB')->table('printsetting')->first();

        Config::set('printSettings', $printSettingModel);

        DB::connection('hotelDB')->enableQueryLog();

        return $next($request);
    }

    public function terminate($request, $response)
    {
        // Store or dump the log data...
        $logData = null;
        foreach(DB::connection('hotelDB')->getQueryLog() as $query){
            //query
            //bindings
            //time
            //$sqlCommand = str_replace(array('%', '?'), array('%%', '%s'), $query['query']);
            //$sqlCommand = vsprintf($sqlCommand, $query['bindings']);
            //echo $sqlCommand;
            $sqlCommand = trim($query['query']);
            $statement = strtolower(substr($sqlCommand, 0, 6));
            if(in_array($statement, ['insert', 'update', 'delete'])){
                //$logData .= date('Y-m-d H:i:s') . ' | ' . $sqlCommand . ' | ' . json_encode($query['bindings']) . ' | Query times' . $query['time'] . ' MS'.PHP_EOL;
                $model = new \App\Models\LogControl;
                $model->logtype = 'BookingEngine';
                $model->logtime = date('Y-m-d H:i:s');
                $model->logsql = $sqlCommand;
                $model->logparams = json_encode($query['bindings']);
                $model->save();
                //\File::append(storage_path('logs/queryLog-'.date('Y-m-d').'.log'), $logData);
            }
        }

        // if($logData !=''){
        //     $logFile = fopen(storage_path('logs/queryLog-'.date('Y-m-d').'.log'), 'a+');
        //     //write log to file
        //     fwrite($logFile, $logData);
        //     fclose($logFile);
        // }
    }
}
