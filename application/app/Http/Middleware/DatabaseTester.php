<?php

namespace App\Http\Middleware;

use Closure;

class DatabaseTester
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $conn = array(
            'driver'    => 'mysql',
            'host'      => $request->input('host_database'),
            'database'  => $request->input('database_name'),
            'username'  => $request->input('username_database'),
            'password'  => $request->input('password_database'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        );

        print_r($conn);

        \Config::set('database.connections.DatabaseTester', $conn);

        return $next($request);
    }
}
