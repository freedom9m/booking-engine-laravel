<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use Route;
use Debugbar;
use Redirect;
use Request;
use App;

class LanguageCtrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $language = Route::current()->getParameter('language'); //รับค่า parameter hotelId จาก route

        $rules = [
            'language' => 'in:en,th' //list of supported languages of your application.
        ];

        $validator = \Validator::make(compact($language),$rules);

        if($validator->passes())
        {
            App::setLocale($language);
        }

        return $next($request);
    }
}
