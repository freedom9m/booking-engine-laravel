<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use Route;
use Debugbar;
use Redirect;

class EngineManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        // Store or dump the log data...
        /*$logData = null;
        foreach(DB::connection('hotelDB')->getQueryLog() as $query){
            //query
            //bindings
            //time
            //$sqlCommand = str_replace(array('%', '?'), array('%%', '%s'), $query['query']);
            //$sqlCommand = vsprintf($sqlCommand, $query['bindings']);
            //echo $sqlCommand;
            $sqlCommand = trim($query['query']);
            $statement = strtolower(substr($sqlCommand, 0, 6));
            if(in_array($statement, ['insert', 'update', 'delete'])){
                //$logData .= date('Y-m-d H:i:s') . ' | ' . $sqlCommand . ' | ' . json_encode($query['bindings']) . ' | Query times' . $query['time'] . ' MS'.PHP_EOL;
                $model = new \App\Models\LogControl;
                $model->logtype = 'BookingEngine';
                $model->logtime = date('Y-m-d H:i:s');
                $model->logsql = $sqlCommand;
                $model->logparams = json_encode($query['bindings']);
                $model->save();
                //\File::append(storage_path('logs/queryLog-'.date('Y-m-d').'.log'), $logData);
            }
        }*/

        // if($logData !=''){
        //     $logFile = fopen(storage_path('logs/queryLog-'.date('Y-m-d').'.log'), 'a+');
        //     //write log to file
        //     fwrite($logFile, $logData);
        //     fclose($logFile);
        // }
    }
}
