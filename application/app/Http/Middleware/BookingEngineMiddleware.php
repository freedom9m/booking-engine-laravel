<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Config;
use Route;
use Debugbar;
use Redirect;
use Request;
use Session;
use App;
use Input;
use Validator;
use Swift_SmtpTransport;
use Swift_Mailer;
use Mail;

class BookingEngineMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $slug = Route::current()->getParameter('slug'); //รับค่า parameter hotelId จาก route

        $debug = substr($slug, -5, 5);

        if($debug == 'debug'){
            $slug = substr($slug, 0, -5);
        }

        $hotelModel = DB::connection('mysql')->table('hotellist')->where('slug', $slug)->where('bookingEnabled', 1)->first(); //ดึงข้อมูลการเชื่อมต่อ Hotel จากการเชื่อมต่อปกติ

        Config::set('slug', Route::current()->getParameter('slug'));

        if($hotelModel != null && $hotelModel->debugMode == 1){
            if(Route::current()->getParameter('slug') == $hotelModel->slug){
                return Redirect::to('hotel/'.$hotelModel->slug)->send();
            }else{
                Debugbar::enable();
            }
        }else{
            if($hotelModel != null && Route::current()->getParameter('slug') != $hotelModel->slug){
                $hotelModel = null;
            }
            Debugbar::disable();
        }



        if($hotelModel === null && Route::getCurrentRoute()->getPath()!='notFound') { //ถ้าข้อมูล hotel ไม่มี และ route ไม่ตรงกับ notFound ให้วิ่งไปหน้า notFound
            return Redirect::to('notFound')->send();
        }

        $hotelId = $hotelModel->id;

        /*สร้างการเชื่อมต่อฐานข้อมูลใหม่ในชื่อ hotelDB โดยใช้ข้อมูลการเชื่อมต่อจากฐานข้อมูล*/
        $conn = array(
            'driver'    => 'mysql',
            'host'      => $hotelModel->host_database,
            'database'  => $hotelModel->database_name,
            'username'  => $hotelModel->username_database,
            'password'  => $hotelModel->password_database,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        );

        Config::set('hotelModel', $hotelModel);

        Config::set('imgPath', $hotelModel->database_name);

        Config::set('database.connections.hotelDB', $conn);

        Config::set('pathName', $hotelModel->database_name);

        Config::set('template', $hotelModel->template);

        /*if(!DB::connection('hotelDB')->getDatabaseName()) { //ถ้าเชื่อมต่อฐานข้อมูลโรงแรมไม่ได้ให้วิ่งไปหน้า databaseError
            return Redirect::to('databaseErr')->send();
        }*/

        $currency = Config::get('currency.'.$hotelModel->currency);

        Config::set('params.currencyCode', $currency['currencyCode']);

        Config::set('params.currencySign', $currency['currencySign']);

        if(Input::has('locale')){
            $language = Input::get('locale');
            $rules = [
                'language' => 'in:en,th' //list of supported languages of your application.
            ];


            $validator = Validator::make(compact($language),$rules);

            if($validator->passes())
            {
                Session::put('language',$language);
                App::setLocale($language);
            }
        }

        if(Session::has('language')){
            App::setLocale(Session::get('language'));
        }

        $bankApiModel = DB::table('bankApiList')->where('idhotel', $hotelId)->where('idbankApiList', $hotelModel->idbankApiList)->first();

        Config::set('bankConfig', $bankApiModel);

        $inventoryModel = DB::connection('hotelDB')->table('systemsetting')->first();
        if($inventoryModel == null){
            $inventory = 'inventory';
        }else{
            $inventory = $inventoryModel->be_room_available == 'allotment' ? 'allotment' : 'inventory';
        }

        $printSettingModel = DB::connection('hotelDB')->table('printsetting')->first();

        /*$mailSetting = Config::get('mail');

        $mailConfig = [
            'driver' => $mailSetting['driver'],
            'host' => $mailSetting['host'],
            'port' => $mailSetting['port'],
            'from' => ['address' => $printSettingModel->gm_email, 'name' => $printSettingModel->hotelname],
            'encryption' => $mailSetting['encryption'],
            'username' => $mailSetting['username'],
            'password' => $mailSetting['password'],
            'sendmail' => '/usr/sbin/sendmail -bs',
            'pretend' => false
        ];
        
        Config::set('mail',$mailConfig);*/

        if($hotelModel->customMailer == 1){
            $mailConfig = array(
                'driver' => 'smtp',
                'host' => $hotelModel->mailHost,
                'port' => $hotelModel->mailPort,
                'from' => array('address' => $hotelModel->mailUsername, 'name' => $hotelModel->hotelname),
                'encryption' => $hotelModel->mailEncryption,
                'username' => $hotelModel->mailUsername,
                'password' => $hotelModel->mailPassword,
                'sendmail' => '/usr/sbin/sendmail -bs',
                'pretend' => false
            );
            Config::set('mail', $mailConfig);
            
            $transport = Swift_SmtpTransport::newInstance(Config::get('mail.host'), Config::get('mail.port'), Config::get('mail.encryption')); // Mail::getSwiftMailer()->getTransport();
            $transport->setUsername(Config::get('mail.username'));
            $transport->setPassword(Config::get('mail.password'));
            $mailer = new Swift_Mailer($transport);
            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($mailer);
        }

        Config::set('printSettings', $printSettingModel);

        Config::set('inventory', $inventory);

        DB::connection('hotelDB')->enableQueryLog();

        return $next($request);
    }

    public function terminate($request, $response)
    {
        // Store or dump the log data...
        $logData = null;
        foreach(DB::connection('hotelDB')->getQueryLog() as $query){
            //query
            //bindings
            //time
            //$sqlCommand = str_replace(array('%', '?'), array('%%', '%s'), $query['query']);
            //$sqlCommand = vsprintf($sqlCommand, $query['bindings']);
            //echo $sqlCommand;
            $sqlCommand = trim($query['query']);
            $statement = strtolower(substr($sqlCommand, 0, 6));
            if(in_array($statement, ['insert', 'update', 'delete'])){
                //$logData .= date('Y-m-d H:i:s') . ' | ' . $sqlCommand . ' | ' . json_encode($query['bindings']) . ' | Query times' . $query['time'] . ' MS'.PHP_EOL;
                $model = new App\Models\LogControl;
                $model->logtype = 'BookingEngine';
                $model->logtime = date('Y-m-d H:i:s');
                $model->logsql = $sqlCommand;
                $model->logparams = json_encode($query['bindings']);
                $model->logurl = Request::url();
                $model->save();
                //\File::append(storage_path('logs/queryLog-'.date('Y-m-d').'.log'), $logData);
            }
        }

        // if($logData !=''){
        //     $logFile = fopen(storage_path('logs/queryLog-'.date('Y-m-d').'.log'), 'a+');
        //     //write log to file
        //     fwrite($logFile, $logData);
        //     fclose($logFile);
        // }
    }
}
