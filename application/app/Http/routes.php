<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as'=>'home', 'uses' => 'WebController@index']);
Route::get('language', [ 
    'before' => 'crsf',
    'uses' => 'LanguageController@chooser',
    'as' => 'language-chooser',
]);
Route::get('notFound', function() {
	return view('hotelNotFound');
});
Route::get('configRequire', function() {
    return view('configRequire');
});
Route::get('databaseErr', function() {
	return view('databaseErr');
});
Route::group(['prefix' => 'reservation'], function () {
    Route::get('/', function(){
        return Redirect::route('home');
    });
    Route::group(['middleware'=>'BookingEngine', 'prefix' => '{slug}'], function(){
        //Route::group(['middleware'=>'LanguageCtrl', 'prefix'=>'{language}'], function(){

            // Route::get('/', function ($hotelId)    {
            // 	echo $hotelId;
            //     // Matches The "/accounts/{account_id}/detail" URL
            // });
            //Route::get('rooms', ['as' => 'rooms', 'uses' => 'RoomController@getRoomAvailable']);
            Route::get('/', ['as' => 'reservation', 'uses' => 'RoomController@index']);
            Route::get('getRoomAvailable', ['uses' => 'RoomController@getRoomAvailable']);
            Route::get('getAvaibility', ['uses' => 'RoomController@getAvaibility']);
            Route::post('step1', ['before' => 'crsf', 'uses' => 'RoomController@step1']);
            Route::match(['get', 'post'], 'step2', ['before' => 'crsf', 'uses' => 'RoomController@step2']);
            Route::post('bankProcess', ['before' => 'crsf', 'uses' => 'RoomController@bankProcess']);
            Route::post('step3', ['before' => 'crsf', 'uses' => 'RoomController@step3']);
            Route::any('confirm', ['as' => 'confirm', 'uses' => 'RoomController@confirm']);
            Route::any('cancel', ['as' => 'cancel', 'uses' => 'RoomController@cancel']);
            Route::any('failure', ['as' => 'failure', 'uses' => 'RoomController@failure']);
            Route::match(['get', 'post'], 'bankResponse', ['as'=>'bankResponse', 'uses' => 'RoomController@bankResponse']);
            Route::match(['get', 'post'], 'service', ['uses' => 'RoomController@service']);
            Route::post('thankyou', ['before' => 'crsf', 'uses' => 'RoomController@thankyou']);
            Route::any('style', ['uses' => 'RoomController@style']);
            Route::get('step1', function(){ return Redirect::route('reservation', ['slug' => \Config::get('slug')]); });
            //Route::get('step2', function(){ return Redirect::route('reservation', ['slug' => \Config::get('slug')]); });
            Route::get('bankProcess', function(){ return Redirect::route('reservation', ['slug' => \Config::get('slug')]); });
            Route::get('step3', function(){ return Redirect::route('reservation', ['slug' => \Config::get('slug')]); });
            Route::get('thankyou', function(){ return Redirect::route('reservation', ['slug' => \Config::get('slug')]); });
            // Route::get('domElement', ['uses' => 'TestController@dom']);
            // Route::any('epic', ['uses' => 'RoomController@test']);
            Route::get('demoEmailTemplate/{referenceId}', ['uses' => 'RoomController@demoEmailTemplate']);
        //});
        /*Route::get('testMail', function(){
            $contactInfo = null;
            echo '<pre>'.print_r(Config::get('mail'), 1).'</pre>';
            var_dump(Config::get('mail.encryption'));
            $transport = Swift_SmtpTransport::newInstance(Config::get('mail.host'), Config::get('mail.port'), Config::get('mail.encryption')); // Mail::getSwiftMailer()->getTransport();
            $transport->setUsername(Config::get('mail.username'));
            $transport->setPassword(Config::get('mail.password'));
            $mailer = new Swift_Mailer($transport);
            // Assign it to the Laravel Mailer
            Mail::setSwiftMailer($mailer);
            var_dump(Mail::send('testMail', [], function ($mail) use ($contactInfo) {
                $senderEmail = 'contact@mii-hotel.com';
                $to = 'dev10.calypso@gmail.com';
                $mail->from($senderEmail, 'Nonny');
                $mail->sender($senderEmail, 'Nonny');
                $mail->replyTo($senderEmail, 'Nonny');
                $mail->to($to, 'Email Tester');
                $mail->subject('Test');
            }));

        });*/
    });
});
Route::group(['middleware' => 'EngineManager', 'prefix' => 'engineManager'], function(){
    Route::get('login', ['uses' => 'AuthController@login']);
    Route::post('login', ['uses' => 'AuthController@doLogin']);
    Route::get('logout', ['uses' => 'AuthController@doLogout']);
    Route::group(['middleware'=>'auth'], function(){
        Route::any('/', function(){
            return \Redirect::to('engineManager/hotels')->send();
        });
        Route::any('hotels', ['as' => 'hotelList', 'uses' => 'EngineMgrController@hotels']);
        Route::any('addHotel', ['as' => 'addHotel', 'uses' => 'EngineMgrController@addHotel']);
        Route::any('editHotel/{hotelId}', ['as' => 'editHotel', 'uses' => 'EngineMgrController@editHotel']);
        Route::any('editStyle/{hotelId}', ['as' => 'editStyle', 'uses' => 'EngineMgrController@editStyle']);
        Route::group(['prefix' => 'banks/{hotelId}'], function(){
            Route::get('/', ['uses' => 'EngineMgrController@banks']);
            Route::any('edit/{idbank}', ['uses' => 'EngineMgrController@editBank']);
            Route::any('add', ['uses' => 'EngineMgrController@addBank']);
            Route::post('getApi', ['uses' => 'EngineMgrController@getApi']);
        });
        Route::get('findSlug', ['uses' => 'EngineMgrController@findSlug']);
        Route::post('updateApi', ['before' => 'crsf', 'uses' => 'EngineMgrController@updateHotelApi']);
        Route::post('unRegistered', ['before' => 'crsf', 'uses' => 'EngineMgrController@unRegistered']);
        Route::post('deleteApi', ['uses' => 'EngineMgrController@deleteAPI']);
        Route::post('testConnection', [/*'middleware' => 'DatabaseTester', */'uses' => 'EngineMgrController@testConnection']);
        Route::post('testMail', [/*'middleware' => 'DatabaseTester', */'uses' => 'EngineMgrController@testMail']);
    });
});
Route::group(['prefix' => 'pos'], function(){
    Route::any('/', function(){
        Redirect::to('http://www.centrumcloud.com/pos')->send();
    });
    Route::group(['middleware'=>'BookingEngine', 'prefix' => '{slug}'], function(){
        Route::any('/', ['uses' => 'PosController@index']);
    });
});
// Route::get('test', function(){
//     echo trans('1234 :tes', [':tes' => 'abc']);
//     return view('test');
// });
Route::any('hotel/{slug}', ['uses' => 'WebController@hotel']);