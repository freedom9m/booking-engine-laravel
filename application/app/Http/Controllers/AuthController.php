<?php

namespace App\Http\Controllers;

use \DB;
use \View;
use App\User;
use App\Http\Controllers\Controller;
use \Validator;
use \Input;
use \Auth;
use \Redirect;
use \Request;
use \Session;

class AuthController extends Controller
{

	public function login()
	{
	    // show the form

	    return View::make('login');
	}

	public function doLogin()
	{
		$rules = array(
		    'username'    => 'required', // make sure the email is an actual email
		    'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			Request::flashOnly(['username', Input::get('username')]);
		    return Redirect::to('engineManager/login')
		        ->withErrors($validator) // send back all errors to the login form
		        ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

		    // create our user data for the authentication
		    $userdata = array(
		        'username'  => Input::get('username'),
		        'password'  => Input::get('password')
		    );

		    // attempt to do the login
		    if (Auth::attempt($userdata)) {

		        // validation successful!
		        // redirect them to the secure section or whatever
		        // return Redirect::to('secure');
		        // for now we'll just echo success (even though echoing in a controller is bad)
		        return Redirect::to('engineManager');
		    } else {        
				Request::flashOnly(['username', Input::get('username')]);

		        // validation not successful, send back to form 
		        return Redirect::to('engineManager/login');

		    }

		}
	}

	public function doLogout()
	{
	    Auth::logout(); // log the user out of our application
	    return Redirect::to('engineManager/login'); // redirect the user to the login screen
	}

}