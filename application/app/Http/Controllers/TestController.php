<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Components\HtmlDom\HtmlDom;

class TestController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function dom()
    {
        echo \App\Components\Functions::calcCurrency(1, 'USD', 'EUR');
        print_r((new \App\API\RateChecker(['agodaLink' => \Config::get('hotelModel')->agodaLink, 'rateDate' => date('Y-m-d'), 'endDate' => date('Y-m-d', strtotime('+1 days', strtotime(date('Y-m-d'))))]))->agoda());
    }
}