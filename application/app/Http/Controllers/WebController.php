<?php

namespace App\Http\Controllers;

use \DB;
use App\User;
use App\Http\Controllers\Controller;
use \Redirect;
use \Input;

class WebController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    function __construct()
    {
        \Debugbar::disable();
    }

    public function index()
    {

    	$data = \App\Models\Hotels::where('bookingEnabled', 1)->where('active', 1);
        $keyword = null;
        if(Input::has('keyword')){
            $keyword = trim(Input::get('keyword'));
            $data->where('hotelname', 'like', '%'.$keyword.'%');
        }

        
        /*echo "<pre>";
        print_r($keyword);
        echo "</pre>";*/
        
        
        return view('hotelList', ['hotelList' => $data->paginate(8), 'keyword' => $keyword]);
    }

    

    public function hotel($slug)
    {
        $model = \App\Models\Hotels::where('slug', $slug)->first();
        if($model == null)
        {
            return Redirect::to('notFound');
        }
        return view('hotelDetail', ['model' => $model]);
    }
}