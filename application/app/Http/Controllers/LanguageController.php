<?php 
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Components\Validation;

class LanguageController extends Controller
{
	public function chooser(\Request $request) {
		$language = \Input::get('locale');
		$rules = [
        	'language' => 'in:en,th' //list of supported languages of your application.
        ];


        $validator = Validator::make(compact($language),$rules);

        if($validator->passes())
        {
            Session::put('language',$language);
            App::setLocale($language);
        }
        else
        {/**/ }
	}
}