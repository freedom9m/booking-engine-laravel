<?php

namespace App\Http\Controllers;

use App\User;
use Mail;
use App\Http\Controllers\Controller;
use App\Components\Validation;
use App\Components\BookingComponents;
use App\Components\Functions;
use DB;
use Input;
use Session;
use Config;
use File;
use Redirect;
use Crypt;
use Request;
class RoomController extends BaseReservationController
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

    public function index()
    {
        $checkInDate = null;
        $checkOutDate = null;
        $promotionCode = null;
        $promotionId = null;
        if(Input::has('checkInDate') && Input::has('checkOutDate')){
            $checkInDate = date('Y-m-d', strtotime(Input::get('checkInDate')));
            $checkOutDate = date('Y-m-d', strtotime(Input::get('checkOutDate')));
            if(Input::has('promotionCode')){
                $promotionCode = Input::get('promotionCode');
            }
        }
        if($checkInDate == null || \App\Components\Validation::date($checkInDate) == false)
        {
            $checkInDate = date('Y-m-d');
        }
        if($checkOutDate == null || \App\Components\Validation::date($checkOutDate) == false)
        {
            $checkOutDate = date('Y-m-d', strtotime('+1 days', strtotime($checkInDate)));
        }
        if(Input::has('promotionId')){
        	$promotionId = Input::get('promotionId');
        }
    	return view('booking.index.'.Config::get('template').'.index', [
            'checkInDate' => $checkInDate,
            'checkOutDate' => $checkOutDate,
            'promotionCode' => $promotionCode,
            'promotionId' => $promotionId
        ]);
    }

    public function getRoomAvailable()
    {
		$checkInDate = null;
		$checkOutDate = null;
		$promotionCode = null;
		if(Input::has('checkInDate') && Input::has('checkOutDate')){
			$checkInDate = Input::get('checkInDate');
			$checkOutDate = Input::get('checkOutDate');
			if(Input::has('promotionCode')){
				$promotionCode = Input::get('promotionCode');
			}
		}
		if($checkInDate == null || Validation::date($checkInDate) == false)
		{
			$checkInDate = date('Y-m-d');
		}
		if($checkOutDate == null || Validation::date($checkOutDate) == false)
		{
			$checkOutDate = date('Y-m-d', strtotime('+1 days', strtotime($checkInDate)));
		}

    	$connection = DB::connection('hotelDB');

		$connection->statement($connection->raw('set @checkin := :checkin'), [':checkin' => $checkInDate]);
		$connection->statement($connection->raw('set @checkout := :checkout'), [':checkout' => $checkOutDate]);
		$connection->statement($connection->raw('set @lastnightdate := :lastnightdate'), [':lastnightdate' => date('Y-m-d', strtotime('-1 days', strtotime($checkOutDate)))]);
		$connection->statement($connection->raw('set @numnight := DATEDIFF(@checkout, @checkin)'));
		$connection->statement($connection->raw('set @bookingdate := curdate()'));
		$connection->statement($connection->raw('set @bookingrange := DATEDIFF(@checkin, @bookingdate)'));
		$connection->statement($connection->raw('set @internetrateSourceid = :sourceid'), [':sourceid'=>4]);

		$availableModel = $connection->select('SELECT 
		    roomtype.roomtypeid,
		    roomtype.roomtypename,
		    ratecate.ratecategoryavailable,
		    ratecateclose.closeoutratecategory,
		    roomcategory.*,
		    booking.totalBooking AS inhouse,
		    tariffrate.*
		FROM
		    roomtype
		        LEFT JOIN
		    (SELECT 
		        MIN(available) AS ratecategoryavailable,
		            COUNT(closeout) AS closeoutratecategory,
		            roomtypeid
		    FROM
		        ratecategory
		    WHERE
		        sourceid = @internetrateSourceid
		            AND ratedate BETWEEN @checkin AND @lastnightdate
		    GROUP BY roomtypeid) AS ratecate ON ratecate.roomtypeid = roomtype.roomtypeid
		        LEFT JOIN
		    (SELECT 
		        COUNT(closeout) AS closeoutratecategory, roomtypeid
		    FROM
		        ratecategory
		    WHERE
		        closeout = 1
		            AND sourceid = @internetrateSourceid
		            AND ratedate BETWEEN @checkin AND @lastnightdate
		    GROUP BY roomtypeid) AS ratecateclose ON ratecateclose.roomtypeid = roomtype.roomtypeid
		        LEFT JOIN
		    (SELECT 
		        roomcategory.roomtypeid AS rid,
		            (COUNT(roomcategory.roomno) - COUNT(roomclose.roomno)) AS availablerate,
		            rate.*
		    FROM
		        roomcategory
		    LEFT JOIN rooms ON rooms.roomno = roomcategory.roomno
		        AND rooms.roomstatus = 1
		    LEFT JOIN roomclose ON roomclose.roomno = roomcategory.roomno
		        AND roomclose.closedate BETWEEN @checkin AND @lastnightdate
		    LEFT JOIN (SELECT 
		        ratecategory.roomtypeid AS rcid, COUNT(*) AS closeoutrate
		    FROM
		        ratecategory
		    WHERE
		        closeout = 1
		            AND ratecategory.ratedate BETWEEN @checkin AND @lastnightdate
		    GROUP BY ratecategory.roomtypeid) AS rate ON rate.rcid = roomcategory.roomtypeid
		    GROUP BY roomcategory.roomtypeid) AS roomcategory ON roomcategory.rid = roomtype.roomtypeid
		        LEFT JOIN
		    (SELECT 
		        tariffrate.roomtypeid AS rid,
		            MIN(tariffrate.available) AS availabletariff,
		            tariff.*
		    FROM
		        tariffrate
		    LEFT JOIN (SELECT 
		        tariff.roomtypeid AS tariffid, COUNT(*) AS closeouttariff
		    FROM
		        tariffrate AS tariff
		    WHERE
		        closeout = 1
		            AND tariff.ratedate BETWEEN @checkin AND @lastnightdate
		    GROUP BY tariff.roomtypeid) AS tariff ON tariff.tariffid = tariffrate.roomtypeid
		    WHERE
		        tariffrate.ratedate BETWEEN @checkin AND @lastnightdate
		    GROUP BY tariffrate.roomtypeid) AS tariffrate ON tariffrate.rid = roomtype.roomtypeid
		        LEFT JOIN
		    /*(SELECT 
		        COUNT(*) AS totalBooking, roomtypeid
		    FROM
		        booking
		    WHERE
		        (booking.checkoutdate > @checkin
		            AND booking.checkindate <= @lastnightdate)
		            AND booking.bookingstatus IN (1 , 2, 3, 5, 6, 8)
		    GROUP BY roomtypeid)*/
			(SELECT 
			    COUNT(DISTINCT room_guest.roomno) as totalBooking, roomtype.roomtypeid
			FROM
			    room_guest
			        INNER JOIN
			    roomcategory ON roomcategory.roomno = room_guest.roomno
			        INNER JOIN
			    roomtype ON roomtype.roomtypeid = roomcategory.roomtypeid
			        INNER JOIN
			    booking_guest AS guest ON guest.guestid = room_guest.guestid
			        INNER JOIN
			    booking ON booking.bookingid = guest.bookingid
			WHERE
			    room_guest.indate BETWEEN @checkin AND @lastnightdate
			        AND booking.bookingstatus IN (1 , 2, 3, 5, 6, 8)
			GROUP BY roomtype.roomtypeid
			) AS booking ON booking.roomtypeid = roomtype.roomtypeid
		GROUP BY roomtype.roomtypeid;', []);

		//check define value.
		//print_r($connection->query('select @checkin, @lastnightdate, @numnight, @bookingdate, @bookingrange;'));

		$queryParams = [];
		$displayAvailebleQuery = null;
		if(Config::get('hotelModel')->displayCloseOut != 1){
			$displayAvailebleQuery = ' and closeout!=1 ';
		}

		if(trim($promotionCode)==''){
			$sqlCommand = "select 'nopromotion' as datatype, roomtype.*, null as proid, 'INTERNET RATE' as proname, null as prodescription, occupancy.minadult, null as discounttype, null as discount, ratecategory.minstay as minstay, null as procolor, ratecategory.price as rateprice, ratecategory.policyid, ratecategory.breakfast 
			from roomtype 
			inner join (SELECT sum(doublesell)/@numnight as price, min(doublesell), min(minstay) as minstay, roomtypeid, policyid, breakfast FROM ratecategory WHERE sourceid=@internetrateSourceid AND ratedate BETWEEN @checkin AND @lastnightdate $displayAvailebleQuery group by roomtypeid having count(*) = @numnight and min(doublesell)!=0) as ratecategory
			on ratecategory.roomtypeid = roomtype.roomtypeid
	        left join occupancy on occupancy.roomtypeid = roomtype.roomtypeid
	        where roomtype.activestatus = 1
			union 
			(
				select 'promotion' as datatype, roomtype.*, promotions.proid, promotions.proname, promotions.description as prodescription, occupancy.minadult, promotions.gettype as discounttype, promotions.amount as discount, promotions.minstay as minstay, promotions.textcolor as procolor, tariffrate.price as rateprice, case when promotions.policyid=0 or promotions.policyid='' then tariffrate.policyid else promotions.policyid end as policyid, tariffrate.breakfast
				from roomtype 
					inner join promotions on roomtype.roomtypeid = promotions.roomtypeid 
							/*  and @bookingdate between bookfrom and bookto and arrival <= @checkin AND departure >= @lastnightdate  */
			                AND CASE
									WHEN
										promotions.proname NOT IN ('Last Minute' , 'Early Bird')
									THEN
										@bookingdate BETWEEN promotions.bookfrom AND promotions.bookto
											AND CASE
											WHEN maxstay > 0 THEN (@numnight BETWEEN promotions.minstay AND promotions.maxstay)
											ELSE (@numnight >= minstay)
										END
									WHEN promotions.proname = 'Last Minute' THEN @bookingrange BETWEEN 0 AND promotions.maxstay
									WHEN promotions.proname = 'Early Bird' THEN @bookingrange >= promotions.minstay
									ELSE TRUE
								END
									AND promotions.arrival <= @checkin
									AND promotions.departure >= @lastnightdate
									AND promotions.activestatus = '1'

					inner join (select sum(doublesell)/@numnight as price, min(doublesell), roomtypeid as roomtype_id, policyid, breakfast from tariffrate where ratedate between @checkin and @lastnightdate $displayAvailebleQuery group by roomtypeid having count(*) = @numnight and min(doublesell)!=0) as tariffrate
					on (tariffrate.roomtype_id= roomtype.roomtypeid)
					left join occupancy on occupancy.roomtypeid = roomtype.roomtypeid
	                
	        	where roomtype.activestatus = 1
			)
			union 
			(
				select 'crosspromotion' as datatype, roomtype.*, concat_ws(',', CAST(promotions.proid AS CHAR), cast(prosecond.proid AS CHAR))/*promotions.proid*/, promotions.proname, promotions.description as prodescription, occupancy.minadult, promotions.gettype as discounttype, promotions.amount as discount, promotions.minstay as minstay, promotions.textcolor as procolor, tariffrate.price as rateprice, case when promotions.policyid=0 or promotions.policyid='' then tariffrate.policyid else promotions.policyid end as policyid, tariffrate.breakfast
				from roomtype 
					inner join promotions on roomtype.roomtypeid = promotions.roomtypeid 
							/*  and @bookingdate between bookfrom and bookto and arrival <= @checkin AND departure >= @lastnightdate  */
			                AND CASE
									WHEN
										promotions.proname NOT IN ('Last Minute' , 'Early Bird')
									THEN
										@bookingdate BETWEEN promotions.bookfrom AND promotions.bookto
											AND CASE
											WHEN maxstay > 0 THEN (@numnight BETWEEN promotions.minstay AND promotions.maxstay)
											ELSE (@numnight >= minstay)
										END
									WHEN promotions.proname = 'Last Minute' THEN @bookingrange BETWEEN 0 AND promotions.maxstay
									WHEN promotions.proname = 'Early Bird' THEN @bookingrange >= promotions.minstay
									ELSE TRUE
								END
					inner join mapcrosspromotion on mapcrosspromotion.promotionid_first = promotions.proid
					inner join promotions as prosecond on prosecond.proid = mapcrosspromotion.promotionid_second
					inner join (select sum(doublesell)/@numnight as price, min(doublesell), roomtypeid as roomtype_id, policyid, breakfast from tariffrate where ratedate between @checkin and @lastnightdate $displayAvailebleQuery group by roomtypeid having count(*) = @numnight and min(doublesell)!=0) as tariffrate
					on (tariffrate.roomtype_id= roomtype.roomtypeid)
					left join occupancy on occupancy.roomtypeid = roomtype.roomtypeid
	                
	        	where roomtype.activestatus = 1
					AND promotions.arrival <= @checkin
					AND prosecond.departure >= @lastnightdate
					AND promotions.departure < @lastnightdate /* last night date must more than departure date on first promotion */
					AND promotions.activestatus = '1'
					AND prosecond.activestatus = '1'
			)
			order by sort asc, roomtypeid asc, FIELD(datatype, 'promotion', 'crosspromotion', 'nopromotion') asc, rateprice asc;";

		}else{
			$sqlCommand = "select 'code' as datatype, roomtype.*, promotioncode.proid,concat_ws(' ', 'Discount Code : ', /*promotioncode.name*/promotioncode.code ) as proname, concat_ws(' ', promotioncode.discount, /*promotioncode.description*/promotioncode.discount_type) as prodescription, occupancy.minadult, case when promotioncode.discount_type='% Discount' then 1 when promotioncode.discount_type = 'Price Discount' then 2 when promotioncode.discount_type = 'Free Night' then 3 end as discounttype, promotioncode.discount as discount, /*promotioncode.minstay*/null as minstay, null as procolor, ratecategory.price as rateprice, case when promotioncode.pid=0 or promotioncode.pid='' then ratecategory.policyid else promotioncode.pid end as policyid, ratecategory.breakfast 
			from roomtype 
				inner join promotioncode on roomtype.roomtypeid = promotioncode.roomtypeid and promotioncode.datefrom <= @checkin and promotioncode.dateto >= @lastnightdate and promotioncode.code = :code
				inner join (select sum(doublesell)/@numnight as price, min(doublesell), roomtypeid as roomtype_id, policyid, breakfast from ratecategory where sourceid=@internetrateSourceid and ratedate between @checkin and @lastnightdate $displayAvailebleQuery group by roomtypeid having count(*) = @numnight and min(doublesell)!=0) as ratecategory
				on (ratecategory.roomtype_id= roomtype.roomtypeid)
				left join occupancy on occupancy.roomtypeid = roomtype.roomtypeid
        	where roomtype.activestatus = 1
			order by sort asc, rateprice asc;";

			$queryParams = [':code' => $promotionCode];
		}

		$model = json_decode(json_encode($connection->select($sqlCommand, $queryParams)), true);
    	//return redirect('notFound');
        //return view('user.profile', ['user' => User::findOrFail($id)]);

        return view('booking.index.'.\Config::get('template').'.roomAvailable', [
			'model'=>$model,
			'availableModel' => $availableModel,
		]);
    }

    public function step1() {

		/*if(!isset($_POST['bookingform']))
		{
			$this->redirect(\app\Url::to(['booking/index']));
			return true;
		}*/
		$data = [];
		if(Input::has('checkInDate') && Input::has('checkOutDate')){
			$checkInDate = Input::get('checkInDate');
			$checkOutDate = Input::get('checkOutDate');
			$promotionCode = null;
			if(Input::has('promotionCode')){
				$promotionCode = Input::get('promotionCode');
			}
			$data = [
				'checkInDate' => $checkInDate,
				'checkOutDate' => $checkOutDate,
				'promotionCode' => $promotionCode,
			];
			$data['rooms'] = [];
			foreach($_POST['room'] as $roomtypeid => $array){ 
				foreach($array as $datatype => $array2){ //loop data type ** promotion, no promotion, promotion code 
					foreach($array2 as $proid => $totalroom){ //loop promotion id
						if($totalroom > 0){ // if no of room > 0;
							$mainRoom = [
								'roomtypeid'		=>$roomtypeid, 
								'roomtypeInfo'		=>BookingComponents::getRoomtype($roomtypeid),
								'datatype'			=>$datatype, 
								'proid'				=>$proid, 
								'discountData'		=>BookingComponents::getPromotion($datatype, $proid),
								'additionalServices'=>BookingComponents::getAddtionalServices(),
								'occupancyData'		=>BookingComponents::getOccupancy($roomtypeid),
								'rateInfo'			=>BookingComponents::getRate([
										'roomtypeId'	=>$roomtypeid,
										'isPromotion'	=>(in_array($datatype, ['nopromotion', 'code'])? false : true),
										'checkInDate'	=>$checkInDate,
										'checkOutDate'	=>$checkOutDate
									]
								)
							];


							foreach(range(1, $totalroom) as $no){ //loop no of room
								$data['rooms'][] = $mainRoom;
							}
						}
					}
				}
			}
			return view('booking.step1', ['data'=>$data]);    	
		}

    }

	public function step2()
	{
		$bankConfig = Config::get('bankConfig');
		$bankList = Config::get('banks');
		if($bankConfig  != null and isset($bankList[$bankConfig->bankKey])) {
			$bankApiParams = $bankList[$bankConfig->bankKey];
			$bankApi = new $bankApiParams['apiPath'];
			$bankConfigParaMeter = json_decode($bankConfig->keyValue);
			foreach($bankApi->fields as $field){

				/*if(in_array($field, $bankApi->fieldsRequire) && (!isset($bankConfigParaMeter->{$field}) or $bankConfigParaMeter->{$field} == null)){
					echo '<p>Bank Configuratoin Not Successfully yet</p>';
					echo '<p>Config Require Parameter : '.$field.'</p>';
					die();
				}else{*/

					if(property_exists($bankApi, $field) && isset($bankConfigParaMeter->{$field})){
						$bankApi->{$field} = $bankConfigParaMeter->{$field};
					}
				/*}*/
			}
		}else{
			$bankApi = new \App\API\BANK\NoBank;
		}

		if(!isset($_POST['guest']))
		{
			if(Session::has('referenceId')){
				$referenceId = Session::get('referenceId');
			}else{
				return Redirect::route('reservation', ['slug' => Config::get('slug')]);
			}
		}else{
			$data = [];
			if(isset($_POST['guest'])){
				$data = $_POST['guest'];
			}
			$data['checkInDate'] = Input::get('checkInDate');
			$data['checkOutDate'] = Input::get('checkOutDate');
			$referenceId = BookingComponents::getInvoiceId();
			Session::put('referenceId', $referenceId);
			$abfSetting = [];
			$abfSettingModel = \App\Models\AbfSetting::all();
			foreach($abfSettingModel as $method):
	                $abfSetting[$method->type]=$method;
	        endforeach;

			foreach(Input::get('roomkey') as $roomtypeId => $_dataTypeArr){
				foreach($_dataTypeArr as $dataType => $_promotionArr){
					foreach($_promotionArr as $promotionId => $_roomArr){

						$roomType = BookingComponents::getRoomtype($roomtypeId);
						$discountData = BookingComponents::getPromotion($dataType, $promotionId);
						$rateInfo = BookingComponents::getRate([
									'roomtypeId'	=>$roomtypeId,
									'isPromotion'	=>(in_array($dataType, ['nopromotion', 'code']) ? false : true),
									'checkInDate'	=>$data['checkInDate'],
									'checkOutDate'	=>$data['checkOutDate']
								]
							);
						$occupancyData = BookingComponents::getOccupancy($roomtypeId);

						/*$extraBed=$occupancyData->minadult;
						$maxPlusBed=$occupancyData->maxadult;
						$minChildren=$occupancyData->minchildren;
						$maxChildren=$occupancyData->maxchildren;
						$minInfant=$occupancyData->mininfant;
						$maxInfant=$occupancyData->maxinfant;
						$maxInfantAge=$occupancyData->maxinfantage;
						$maxChildrenAge=$occupancyData->maxchildrenage;
						$minAdultAge=$occupancyData->minadultage;
						$plusExtraInfant=$occupancyData->plusextrainfant;
						$plusExtraChild=$occupancyData->plusextrachild;
						$plusExtraAdult=$occupancyData->plusextraadult;*/

						$extraBed=$occupancyData->minadult;
						$maxPlusBed=$occupancyData->maxadult;
						$minChildren=$occupancyData->minchildren;
						$maxChildren=$occupancyData->maxchildren;
						$maxInfantAge=$occupancyData->maxinfantage;
						$maxChildrenAge=$occupancyData->maxchildrenage;
						$minAdultAge=$maxChildrenAge+1;
						$plusExtraInfant=$occupancyData->plusextrainfant;
						$plusExtraChild=$occupancyData->plusextrachild;
						$plusExtraAdult=$occupancyData->plusextraadult;


						$discountType = array(1=>'Percentage', 2=>'Price Discount', 3=>'Free night');
						$numNight = $rateInfo['numNight'];
						$pricePerNight = $rateInfo['price'];

						if($rateInfo['policyid']==null){
							$policyId = BookingComponents::getPolicy();
						}else{
							$policyId = $rateInfo['policyid'];
						}

						foreach($_roomArr as $index){
	                        $abf_adults=0;
	                        $adults = $data['guest_noofguest'][$roomtypeId][$dataType][$promotionId][$index];
	                        if($adults>$extraBed){
	                            $abf_adults=($adults-$extraBed)*$abfSetting['adults']->price;
	                        }

	                        $child = $data['guest_children'][$roomtypeId][$dataType][$promotionId][$index];

	                        $abf_children=$abfSetting['children']->price*$child>0?0:$abfSetting['children']->price*$child;

							///////////////////////////////////CALCULATE////////////////////////////////////////////
							$freeNight = 0;
							$priceExtra = 0;
							$priceExtraAdult = 0;
							$priceExtraChild = 0;
							$totalExtraBed = 0;
							$totalExtraChild = 0;
							if($data['guest_noofguest'][$roomtypeId][$dataType][$promotionId][$index] - $extraBed > 0){
								$totalExtraBed = $data['guest_noofguest'][$roomtypeId][$dataType][$promotionId][$index] - $extraBed;
							}
							if($data['guest_children'][$roomtypeId][$dataType][$promotionId][$index] - $minChildren+$maxPlusBed > 0){
								$totalExtraChild = $data['guest_children'][$roomtypeId][$dataType][$promotionId][$index];
							}

							$originalPriceExtra = $rateInfo['extraBedPirce'];
							$originalPriceExtraChild = $rateInfo['extraBedChildPrice'];
							if($data['guest_noofguest'][$roomtypeId][$dataType][$promotionId][$index] > $extraBed){
								$priceExtra = ($originalPriceExtra*$numNight)*$totalExtraBed;
							}

							$priceExtraAdult += $priceExtra;
							/*if($data['guest_children'][$roomtypeId][$dataType][$promotionId][$index] > $minChildren){
								$priceExtraChild = $originalPriceExtraChild*$numNight;
							}*/

							$bookingChildrens = [];


	                        if($child > 0){
	                        	$iChild = 0;
	                        	$iExtrabed = $data['guest_noofguest'][$roomtypeId][$dataType][$promotionId][$index];

	                        	foreach($data['guest_ageofchildren'][$roomtypeId][$dataType][$promotionId][$index] as $key => $childAge){
									$extraChildPrice = 0;
						    		if($iExtrabed < $extraBed){
						    			$iExtrabed++;
						    			$note = 'Include at adult';
						    		}else{
							    		if($childAge < $minAdultAge){
							    			$iChild++; 
							    		}
							    		$plus = false;
							    		$isAdult = false;
							    		$note = 'is infant';
							    		if($maxInfantAge >= $childAge && $plusExtraInfant == 1){
							    			$plus = true;
							    			$note = 'is child';
							    		}else if($maxInfantAge < $childAge && $maxChildrenAge >=  $childAge && $plusExtraChild == 1){
							    			$plus = true;
							    			$note = 'is child';
							    		}else if($maxChildrenAge < $childAge && $minAdultAge <= $childAge && $plusExtraAdult == 1){
							    			$isAdult = true;
							    			$plus = true;
							    			$note = 'is Adult';
							    		}
							    		if($plus == true && $isAdult == false && $iChild > $minChildren){
							    			$extraChildPrice = $originalPriceExtraChild * $numNight;
							    			$priceExtraChild += $extraChildPrice;
							    		}else if($plus == true && $isAdult == true){
							    			$extraChildPrice = $originalPriceExtra * $numNight;
							    			$priceExtraChild += $extraChildPrice;
							    		}
						    		}

						    		$bookingChildrens[] = ['age' => $childAge, 'extraPrice' => ($extraChildPrice), 'note' => $note];
	                        	}
	                        }
							$priceAllNight = $pricePerNight*$numNight;
							$priceFirstPromotion = $priceAllNight;
							$newPrice = $priceAllNight;
							$crossPromotion = false;
							if($discountData!=null){
								if(isset($discountData['crossPromotion']) && $discountData['crossPromotion']!=null){
									$crossPromotion = true;
									$totalNightToLastDayOfPromotion = Functions::dateDiff($data['checkInDate'], $discountData['departure'])+1; //function diff will minus 1 day
									$priceFirstPromotion = $pricePerNight*$totalNightToLastDayOfPromotion;
									$priceLastPromotion = $priceAllNight-$priceFirstPromotion;
									$totalNightToLastDayOfBooking = Functions::dateDiff($discountData['departure'], $data['checkOutDate'])-1; //minus check out day
								}

								$policyId = $discountData['policyid'];

								$discountCalculated = BookingComponents::calculateDiscount([
									'discountData' => $discountData,
									'numNight' => $crossPromotion == true ? $totalNightToLastDayOfPromotion : $numNight,
									'priceAllNight' => $priceFirstPromotion,
									'pricePerNight' => $pricePerNight,
									'newPrice' => $newPrice,
									'freeNight' => $freeNight,
								]);
								extract($discountCalculated);
								$priceFirstPromotion = $discountPrice;
								$discountPrice = $discountPrice;

								if($crossPromotion == true){
									$discountCrossPromotionCalculated = BookingComponents::calculateDiscount([
										'discountData' => $discountData['crossPromotion'],
										'numNight' => $totalNightToLastDayOfBooking,
										'priceAllNight' => $priceLastPromotion,
										'pricePerNight' => $pricePerNight,
										'newPrice' => $newPrice,
										'freeNight' => null,
									]);

									$newPriceLastPromotion = $discountCrossPromotionCalculated['newPrice'];
									$discountPriceLastPromotion = $discountCrossPromotionCalculated['discountPrice'];
									$freeNightLastPromotion = $discountCrossPromotionCalculated['freeNight'];

									$newPrice += $newPriceLastPromotion;			
									$discountPrice += $discountPriceLastPromotion;
									$freeNight += $freeNightLastPromotion;
								}

								/*if($discountData['discounttype']==3){ //free night
									$freeNight = Functions::freeNightCalculate([
										'totalNight'	=> $numNight,
										'stay'			=> $discountData['minstay'],
										'freeNight'		=> $discountData['discount'],
									]);

									// for discount extra
									//$priceExtra = $priceExtra == 0 ? 0 : $priceExtra - ($originalPriceExtra*$freeNight);
									//$priceExtraChild = $priceExtraChild == 0 ? 0 : $priceExtraChild - ($originalPriceExtraChild*$freeNight);
									$newPrice = $priceAllNight - ($pricePerNight*$freeNight);
									$discountPrice = $priceAllNight - $newPrice;
								}else if($discountData['discounttype']==2){ //by price
									$discountPrice = $discountData['discount']*$numNight;
									$newPrice = $priceAllNight - $discountPrice;
								}else if($discountData['discounttype']==1){ //percentage
									// for discount extra
									//$priceExtra = $priceExtra == 0 ? 0 : $priceExtra-(($discountData['discount']/100)*$priceExtra);
									//$priceExtraChild = $priceExtraChild == 0 ? 0 : $priceExtraChild-(($discountData['discount']/100)*$priceExtraChild);
									
									$newPrice = $priceAllNight - ($discountData['discount']/100)*$priceAllNight;
									$discountPrice = $priceAllNight - $newPrice;
								}*/
							}
							$extraPrice = $priceExtraAdult + $priceExtraChild;
							$newPrice = $newPrice + $extraPrice;
							$priceWithOutTax = $newPrice / Config::get('hotelModel')->vat;
							$priceTax  = $newPrice - $priceWithOutTax;
							//echo Functions::dateDiff($rateInfo['checkInDate'], $rateInfo['checkOutDate']);

							$totalPrice = $priceWithOutTax;

							//////////////////////////////////////////////////////////////////////////////////////////////////////////

							$bookingId = BookingComponents::getBookingId();

							$bookingModel = new \App\Models\Booking; 
							$bookingModel->bookingid = $bookingId;
							$bookingModel->agentid = $referenceId;
							$bookingModel->sourceid = 4; // have to change
							$bookingModel->roomtypeid = $roomtypeId;
							$bookingModel->requestdate = date('Y-m-d');
							$bookingModel->requesttime = date('H:i:s');
							$bookingModel->checkindate = $data['checkInDate'];
							$bookingModel->checkoutdate = $data['checkOutDate'];
							$bookingModel->noofnight = Functions::dateDiff($data['checkInDate'], $data['checkOutDate']);
							$bookingModel->adult = $data['guest_noofguest'][$roomtypeId][$dataType][$promotionId][$index];
							$bookingModel->child = $data['guest_children'][$roomtypeId][$dataType][$promotionId][$index];
							$bookingModel->child_age = '';
							//$bookingModel->infant = $infant;
							$bookingModel->extrabed = $totalExtraBed;
							$bookingModel->extrachild = $totalExtraChild;
							$bookingModel->totalextra = $extraPrice;
							$bookingModel->totalprice = $totalPrice;
							$bookingModel->totaltax = $priceTax;
							/*$bookingModel->totalextra = $totalextra; *******/
							$bookingModel->total = $newPrice;
							$bookingModel->deposit = 0.00; // howmany card pay
							$bookingModel->createby = 'Booking Engine';
							//$bookingModel->overbooking = $overbooking;
							$bookingModel->paymenttype = 'Unknow';
							/*$bookingModel->checkintime = $checkintime;
							$bookingModel->checkouttime = $checkouttime;*/
							$bookingModel->bookingstatus = 9;
							$bookingModel->abf_adults = $abf_adults;
							$bookingModel->abf_children = $abf_children;
							$bookingModel->commission = 0.00; 
							$bookingModel->price_type = Config::get('params.currencyCode');
							$bookingModel->proid = $promotionId;

							$bookingModel->policyid = $policyId;
							$bookingModel->save();


							$guestModel = new \App\Models\Guest;
							$guestModel->guestid = $bookingId;
							$guestModel->prefix = Input::get('prefix');
							$guestModel->firstname = Input::get('firstname');
							$guestModel->lastname = Input::get('lastname');
							$guestModel->address = Input::get('address');
							$guestModel->city = Input::get('city');
							$guestModel->zip = Input::get('zip');
							$guestModel->countryofpassport = Input::get('countryofpassport');
							$guestModel->phone = Input::get('phone');
							$guestModel->email = Input::get('email');
							$guestModel->customernote = Input::get('specialrequest');
							$guestModel->guest_prefix = $data['guest_prefix'][$roomtypeId][$dataType][$promotionId][$index];
							$guestModel->guest_firstname = $data['guest_firstname'][$roomtypeId][$dataType][$promotionId][$index];
							$guestModel->guest_lastname = $data['guest_lastname'][$roomtypeId][$dataType][$promotionId][$index];
							$guestModel->guest_country = $data['guest_country'][$roomtypeId][$dataType][$promotionId][$index];

							$guestModel->save();

							if(isset($data['additionalService'][$roomtypeId][$dataType][$promotionId][$index])){
								foreach($data['additionalService'][$roomtypeId][$dataType][$promotionId][$index] as $additionalId => $val){
									if($val>0){
										$additionalData = BookingComponents::getAddtionalServicesData($additionalId);
										$additionalServiceModel = new \App\Models\AdditoinalList;
										$additionalServiceModel->additionalid = $additionalId;
										$additionalServiceModel->guestid = $bookingId;
										$additionalServiceModel->bookingid = $bookingId;
										$additionalServiceModel->unit = $val;
										$additionalServiceModel->price = $additionalData->price;
										$additionalServiceModel->total = $additionalData->price*$val;
										$additionalServiceModel->create_date = date('Y-m-d H:i:s');
										$additionalServiceModel->payment_status = 0;
										$additionalServiceModel->ack_status = 0;

										/*$additionalServiceModel->flightno = ;
										$additionalServiceModel->flighttime = ;*/
										$additionalServiceModel->save();
									}
								}
							}

							if(count($bookingChildrens) > 0){
								foreach($bookingChildrens as $childData){
									$bookingChildren = new \App\Models\BookingChildren;
									$bookingChildren->bookingid = $bookingId;
									$bookingChildren->age = $childData['age'];
									$bookingChildren->extraprice = $childData['extraPrice'];
									$bookingChildren->save();
								}
							}

							$bookingGuest = new \App\Models\BookingGuest;
							$bookingGuest->bookingid = $bookingId;
							$bookingGuest->guestid = $bookingId;
							$bookingGuest->level = 1;

							$bookingGuest->save();

							$bookingPromotionModel = new \App\Models\BookingPromotion;
							$bookingPromotionModel->bookingid = $bookingId;
							$bookingPromotionModel->proid = $promotionId;
							$bookingPromotionModel->proname = $discountData['name'] == '' ? 'No Promotion' : $discountData['name'];
							$bookingPromotionModel->promotiontype = $dataType;
							$bookingPromotionModel->policyid = $policyId;
							if($crossPromotion == true){
								$bookingPromotionModel->crosspromotionid = $discountData['crossPromotion']['proid'];
								$bookingPromotionModel->crosspromotionname = $discountData['crossPromotion']['name'];
							}

							$bookingPromotionModel->save();


	                        $begin = new \DateTime($data['checkInDate']);

	                        $end = new \DateTime(date("Y-m-d", strtotime($data['checkOutDate'])));
	                        $interval = \DateInterval::createFromDateString('1 day');
	                        $period = new \DatePeriod($begin, $interval, $end);

	                        $i=0;
	                        $startFree = $numNight-$freeNight;
	                        $countNumnight = $startFree;

	                        $priceWithoutExtra = $newPrice - $extraPrice;
	                        
	                        foreach ( $period as $dt ){
	                            $i++;
	                            if($freeNight > 0 and $i>$startFree){
	                                $bkPriceExtra = $priceExtraAdult/$numNight;
	                                $bkPriceExtraChild = $priceExtraChild/$numNight;
	                                // for discount extra
	                                //$bkPriceExtra = 0;
	                                //$bkPriceExtraChild = 0;
	                                $bkExtraTotal = $bkPriceExtra + $bkPriceExtraChild;
	                                $bkTotalAmount = $bkExtraTotal;
	                                $bkPrice = 0;
	                                $bkTax = 0;
	                            }else{
	                                $bkPriceExtra = $priceExtraAdult/$numNight;
	                                $bkPriceExtraChild = $priceExtraChild/$numNight;
	                                $bkExtraTotal = ($priceExtraAdult+$priceExtraChild)/$numNight;
	                            	$bkTotalAmount = $priceWithoutExtra/$countNumnight + $bkExtraTotal;
	                                $bkPrice = $bkTotalAmount / Config::get('hotelModel')->vat;//$bkTotalAmount-$bkExtraTotal-$bkTax;
	                                $bkTax = $bkTotalAmount - $bkPrice;
	                            }

								$inDate = $dt->format( "Y-m-d H:i:s" );


	                            if($crossPromotion == true){
	                            	if($inDate > date('Y-m-d', strtotime('+1 days', strtotime($discountData['departure'])))){
		                                // $bkTax = $priceTax/$countNumnight;
		                                // $bkPriceExtra = $priceExtra/$countNumnight;
		                                // $bkPriceExtraChild = $priceExtraChild/$countNumnight;
		                                // $bkExtraTotal = ($priceExtra+$priceExtraChild)/$countNumnight;
		                                $bkTotalAmount = $newPriceLastPromotion/$totalNightToLastDayOfBooking;
		                                $bkPrice = $bkTotalAmount-$bkExtraTotal-$bkTax;
	                            	}else{
		                                $bkTotalAmount = $priceFirstPromotion/$totalNightToLastDayOfPromotion;
		                                $bkPrice = $bkTotalAmount-$bkExtraTotal-$bkTax;
	                            	}
	                            }else{

		                        }

								$bookingPriceModel = new \App\Models\BookingPrice;
								$bookingPriceModel->bookingid = $bookingId;
								$bookingPriceModel->pricedate = $inDate;
								$bookingPriceModel->price = $bkPrice;
								$bookingPriceModel->tax = $bkTax;
								$bookingPriceModel->extraadult = $bkPriceExtra;
								$bookingPriceModel->extrachild = $bkPriceExtraChild;
								$bookingPriceModel->extratotal = $bkExtraTotal;
								$bookingPriceModel->totalamount = $bkTotalAmount;
								$bookingPriceModel->abf = BookingComponents::getAbf(in_array($dataType, ['nopromotion', 'code']), $dt->format('Y-m-d'), $roomtypeId);
								$bookingPriceModel->save();
							}
						}
					}
				}
			}

		}
		/*$model = new \App\Models\ConfirmForm;
		$model->referenceId = $referenceId;*/
		$model = [
			'referenceId' => $referenceId
		];

		return view('booking.step2', ['model'=>$model, 'data'=>BookingComponents::getBookingData($referenceId), 'bankApi' => $bankApi]);
    }

    public function step3()
    {
    	$referenceId = Input::get('referenceId');
    	$contactName = Input::get('contactName');
    	$contactEmail = Input::get('contactEmail');
    	$totalPrice = Input::get('totalPrice');
    	return view('booking.step3', [
    		'referenceId' => $referenceId,
    		'contactName' => $contactName,
    		'contactEmail' => $contactEmail,
    		'totalPrice' => $totalPrice,
    	]);
    }

    public function bankResponse()
    {
    	$referenceId = Input::has('referenceId') ? Input::get('referenceId') : null;
    	$data = Input::has('data') ? Input::get('data') : null;

		$bankConfig = Config::get('bankConfig');
		$bankList = Config::get('banks');
		if($bankConfig  != null and isset($bankList[$bankConfig->bankKey])) {
			$bankApiParams = $bankList[$bankConfig->bankKey];
			$bankApi = new $bankApiParams['apiPath'];
		}else{
			$bankApi = new \App\API\BANK\NoBank;
		}
		$model = new \App\Models\LogPayment;
		$model->url = Request::url();
		$model->urldata = json_encode($_GET);
		if(Request::isMethod('post')){
			$model->data = json_encode($_POST);
		}
    	if(method_exists($bankApi, 'responseService')){
    		$response = $bankApi->responseService();
    		if($response != null){
	    		$referenceId = $response['referenceId'];
	    		$data = $response['data'];
    		}
    	}else{
    		$response = [];
    	}
    	if($referenceId != null){
	    	$model->referenceid = $referenceId;
	    	$model->logtime = date('Y-m-d H:i:s');
	    	$model->save();
	    	if($bankApi instanceof \App\API\BANK\NoBank){ //no api
	    		$reservationData = [
	    			'referenceId' => $referenceId,
					'status' => 2,
					'cardHolder' => Input::get('cardHolder'),
					'cardType' => Input::get('cardType'),
					'cardNo' => Input::get('cardNo'),
					'expireMonth' => Input::get('expireMonth'),
					'expireYear' => Input::get('expireYear'),
	    		];
	    		$this->finalUpdateBooking($reservationData);
	    		return Redirect()->route('confirm', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
	    	}else{
		    	if($data == md5('success_'.$referenceId)){
		    		$reservationData = [
		    			'referenceId' => $referenceId,
						'status' => 5,
						'cardHolder' => null,
						'cardType' => null,
						'cardNo' => null,
						'expireMonth' => null,
						'expireYear' => null,
		    		];
		    		$this->finalUpdateBooking($reservationData);
	    			return Redirect()->route('confirm', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
		    	}else if ($data == md5('failure_'.$referenceId)){
	    			return Redirect()->route('failure', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
		 	   	}else if ($data == md5('cancel_'.$referenceId)){
	    			return Redirect()->route('cancel', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
		    	}else if ($data == md5('other_'.$referenceId)){
	    			//return Redirect::to('reservation/' . Config::get('slug') . '/cancel?referenceId='.$referenceId);
		    	}else{
	    			return Redirect()->route('failure', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
		    	}
	    	}
    	}else{
			return Redirect::route('reservation', ['slug' => Config::get('slug')]);
    	}
    }

    public function service() //datafeed service
    {
    	$data = Input::has('data') ? Input::get('data') : null;

		$bankConfig = Config::get('bankConfig');
		$bankList = Config::get('banks');
		if($bankConfig  != null and isset($bankList[$bankConfig->bankKey])) {
			$bankApiParams = $bankList[$bankConfig->bankKey];
			$bankApi = new $bankApiParams['apiPath'];
		}else{
			$bankApi = new \App\API\BANK\NoBank;
		}
		$model = new \App\Models\LogPayment;
		$model->url = Request::url();
		$model->urldata = json_encode($_GET);
		if(Request::isMethod('post')){
			$model->data = json_encode($_POST);
		}
		$referenceId = null;
    	if(method_exists($bankApi, 'dataFeedService')){
			$response = $bankApi->dataFeedService();
			if($response != null){
	    		$referenceId = $response['referenceId'];
	    		$data = $response['data'];
			}
		}
    	$model->referenceid = $referenceId;
    	$model->logtime = date('Y-m-d H:i:s');
    	$model->save();
    	if($referenceId != null){
	    	if($data == md5('success_'.$referenceId)){
	    		$reservationData = [
	    			'referenceId' => $referenceId,
					'status' => 5,
					'cardHolder' => null,
					'cardType' => null,
					'cardNo' => null,
					'expireMonth' => null,
					'expireYear' => null,
	    		];
	    		$this->finalUpdateBooking($reservationData);
    			//return Redirect()->route('confirm', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
	    	}else if ($data == md5('failure_'.$referenceId)){
    			//return Redirect()->route('failure', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
	 	   	}else if ($data == md5('cancel_'.$referenceId)){
    			//return Redirect()->route('cancel', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
	    	}else if ($data == md5('other_'.$referenceId)){
    			//return Redirect::to('reservation/' . Config::get('slug') . '/cancel?referenceId='.$referenceId);
	    	}else{
    			//return Redirect()->route('failure', ['slug' => Config::get('slug'), 'referenceId' => $referenceId]);
	    	}
    	}
    }

    public function finalUpdateBooking($data = [])
    {
    	$referenceId = $data['referenceId'];
    	$status = $data['status'];
    	$cardHolder = $data['cardHolder'];
		$cardType = $data['cardType'];
		$cardNo = $data['cardNo'];
		$expireMonth = $data['expireMonth'];
		$expireYear = $data['expireYear'];

		$pdo = DB::connection('hotelDB')->getPDO();
		$dataRef = \App\Models\Booking::where('agentid', $referenceId)->first();
		if(!in_array($dataRef->bookingstatus, [5, 2])){
			foreach(\App\Models\Booking::where('agentid', $referenceId)->get() as $method)
			{
				if(!in_array($method->bookingstatus, [5, 2])){ //5 confirm & paid, 2 confirm

					$stmt = $pdo->prepare('set @checkin := :checkin');
					$stmt->execute([':checkin' => $method->checkindate]);
					$stmt = $pdo->prepare('set @checkout := :checkout');
					$stmt->execute([':checkout' => $method->checkoutdate]);
					$stmt = $pdo->prepare('select rc.roomno 
			            from roomcategory as rc,rooms as r 
			            where rc.roomno=r.roomno 
			            and rc.roomtypeid='.$method->roomtypeid.'
			            and rc.roomno not in (
			                select room_guest.roomno 
			                from room_guest
			                inner join guest on guest.guestid=room_guest.guestid
			                inner join booking_guest on booking_guest.guestid=guest.guestid
			                inner join booking on booking.bookingid=booking_guest.bookingid/* and booking.bookingstatus IN ( 1, 2, 3, 5, 6, 8 ) */
			                inner join rooms on room_guest.roomno=rooms.roomno and rooms.roomstatus=1
			                where room_guest.indate between @checkin and @checkout
			            )

			            and rc.roomno not in (
			                select roomno from roomclose where closedate between @checkin and @checkout
			            )

			            and r.roomstatus=1
			            order by r.roomno limit 1');
					/*
					for 'and booking.bookingstatus IN ( 1, 2, 3, 5, 6, 8 ) '
					if remove relation key must enable
					*/
					$stmt->execute();
					$room = $stmt->fetchColumn();
		            $begin = new \DateTime($method->checkindate);
		            $end = new \DateTime($method->checkoutdate);
		            $interval = \DateInterval::createFromDateString('1 day');
		            $period = new \DatePeriod($begin, $interval, $end);
			        foreach ( $period as $dt ):
						if($room!=null){
			            	$roomGuestModel = new \App\Models\RoomGuest;
			            	$roomGuestModel->roomno = $room;
			            	$roomGuestModel->guestid = $method->bookingid;
			            	$roomGuestModel->indate = $dt->format( "Y-m-d H:i:s" );
			            	$roomGuestModel->save();
				        }
				        if(Config::get('inventory')=='allotment'){
					        $bookingPromotionModel = \App\Models\BookingPromotion::where('bookingid', $method->bookingid)->first();
				            foreach ( $period as $dt ):
				            	//if($bookingPromotionModel->promotiontype == 'nopromotion'){
				            	$stmt = $pdo->prepare('update ratecategory set available = available-1 where roomtypeid = :roomtypeid and sourceid = 4 and ratedate = :ratedate');
				            	$stmt->execute([
				            		':roomtypeid'=>$method->roomtypeid, 
				            		':ratedate'=>$dt->format('Y-m-d')
				            	]);

				            	$stmt = $pdo->prepare('update tariffrate set available = available-1 where roomtypeid = :roomtypeid and ratedate = :ratedate');
				            	$stmt->execute([
				            		':roomtypeid'=>$method->roomtypeid, 
				            		':ratedate'=>$dt->format('Y-m-d')
				            	]);
				            	//}
				            endforeach;
				        }
			        endforeach;


	            	/*if using allotment have to update tariffrate if is promotion or ratecategory if internet rate
	            	minus 1 room available per day per roomtype*/


			        /*update credit card*/
					$model = \App\Models\Booking::where('bookingid', $method->bookingid)->update([
						'cardholdername' => $cardHolder,
						'cardtype' => $cardType,
						'upcnumber' => str_replace(' ', null, $cardNo),
						'upcexpire' => $expireMonth.'-'.$expireYear,
						'bookingstatus' => $status,
						]);

					$stmt = $pdo->prepare('update additional_list set payment_status=1 where bookingid = :bookingid');
					$stmt->execute([':bookingid'=>$method->bookingid]);
				}
			}

			$bookingData = BookingComponents::getBookingData($referenceId);
			$contactInfo = BookingComponents::getContactInfo($referenceId);

			Mail::send('mailingTemplate.bookingBlaytong', ['data'=>$bookingData, 'contactInfo' => $contactInfo], function ($mail) use ($contactInfo) {
				$pattern = '/[;,]/';
				$senderEmails = array_map('trim', preg_split($pattern, Config::get('printSettings')->gm_email));
				list($senderEmail) = $senderEmails;

	            $mail->from($senderEmail, Config::get('printSettings')->hotelname);
	            $mail->sender($senderEmail, Config::get('printSettings')->hotelname);
	            $mail->replyTo($senderEmail, Config::get('printSettings')->hotelname);
	            $mail->to($contactInfo->email, $contactInfo->prefix.''.$contactInfo->firstname.' '.$contactInfo->lastname);
	            $mail->cc(array_map('trim', explode(',', Config::get('printSettings')->email)));
	            if(trim(Config::get('printSettings')->gm_email)!=''){
	            	$mail->bcc($senderEmails);
	            }
	            $mail->subject('Booking Confirmation');
	        });

			if(Session::has('referenceId')){
	        	Session::forget('referenceId');
	        }
	    }
        return true;
    }

    public function demoEmailTemplate($slug, $referenceId)
    {
    	// $referenceId = '160826003';
		$bookingData = BookingComponents::getBookingData($referenceId);
		$contactInfo = BookingComponents::getContactInfo($referenceId);
		return view('mailingTemplate.bookingBlaytong', ['data'=>$bookingData, 'contactInfo' => $contactInfo]);
    }

    public function confirm()
    {
    	$referenceId = Input::get('referenceId');
		return view('booking.confirm', ['referenceId' => $referenceId]);
    }

    public function cancel()
    {
    	$referenceId = Input::get('referenceId');
		return view('booking.cancel', ['referenceId' => $referenceId]);
    }

    public function failure()
    {
    	$referenceId = Input::get('referenceId');
		return view('booking.failure', ['referenceId' => $referenceId]);
    }

    public function getAvaibility()
    {
		$startDate = Input::get('startDate');
		if($startDate < date('Y-m-d')){
			$startDate = date('Y-m-d');
		}
		$endDate = date("Y-m-d", strtotime('+10 days', strtotime($startDate)));
		$allRoomtype = Input::get('allRoomtype');

        $begin = new \DateTime($startDate);
        $end = new \DateTime($endDate);



        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        $avaibility = [];

        $roomtype = null;


        $roomtype = \App\Models\Roomtype::where('activestatus', 1);
        if(!empty($allRoomtype)){
	        $roomtype->where('roomtypeid', $allRoomtype);
        }
        $roomtypeList = [];
        $dayList = [];
        $i = 0;
		foreach($roomtype->get() as $method){
			$roomtypeList[$method->roomtypeid] = $method->roomtypename;
	        foreach ( $period as $dt ){
				$inDate = $dt->format( "Y-m-d" );
	        	if($i==0){
	        		$dayList[$inDate] = ['num'=>$dt->format('j'), 'day'=>substr($dt->format('D'), 0, 2)];
	        	}
				$avaibility[$method->roomtypeid][$inDate] = ['avaibility' => $this->checkAvailable($inDate, $method->roomtypeid)];
			}
			$i++;
		}

		echo json_encode([
			'startDate'=>$startDate,
			'startDateText'=>date('j M Y', strtotime($startDate)),
			'endDateText'=>date('j M Y', strtotime('-1 days', strtotime($endDate))),
			'dayInfo'=>$dayList, 
			'roomInfo'=>$roomtypeList, 
			'avaibility'=>$avaibility
		]);
    }

    protected function checkAvailable($date, $idroomtype)
	{

		$connection = DB::connection('hotelDB')->getPDO();

		$stmt = $connection->prepare('set @checkin := :checkin');
		$stmt->execute([':checkin' => $date]);
		$stmt = $connection->prepare('set @internetrateSourceid = :sourceid');
		$stmt->execute([':sourceid' => 4]);
		$stmt = $connection->prepare('SELECT 
			    roomtype.roomtypeid,
			    roomtype.roomtypename,
			    ratecate.ratecategoryavailable,
			    ratecateclose.closeoutratecategory,
			    roomcategory.*,
		    	booking.totalBooking AS inhouse,
			    tariffrate.*
			FROM
			    roomtype
			        LEFT JOIN
				(SELECT MIN(available) AS ratecategoryavailable, count(closeout) AS closeoutratecategory, roomtypeid
					FROM ratecategory WHERE sourceid = @internetrateSourceid AND ratedate = @checkin
					GROUP BY roomtypeid
				) AS ratecate ON  ratecate.roomtypeid = roomtype.roomtypeid
			    	LEFT JOIN
                (SELECT count(closeout) as closeoutratecategory, roomtypeid
					FROM ratecategory WHERE closeout = 1 AND sourceid = @internetrateSourceid AND ratedate = @checkin
					GROUP BY roomtypeid
				) as ratecateclose on  ratecateclose.roomtypeid = roomtype.roomtypeid
			    	LEFT JOIN
			    (SELECT 
			        roomcategory.roomtypeid AS rid,
			            (COUNT(roomcategory.roomno) - COUNT(roomclose.roomno)) AS availablerate, rate.*
				    FROM
				        roomcategory
				    LEFT JOIN rooms ON rooms.roomno = roomcategory.roomno
				        AND rooms.roomstatus = 1
				    LEFT JOIN roomclose ON roomclose.roomno = roomcategory.roomno
				        AND roomclose.closedate = @checkin
				    LEFT JOIN (SELECT 
				        ratecategory.roomtypeid AS rcid, COUNT(*) AS closeoutrate
				    FROM
				        ratecategory
				    WHERE
				        closeout = 1
				            AND ratecategory.ratedate = @checkin
				    GROUP BY ratecategory.roomtypeid
				) AS rate ON rate.rcid = roomcategory.roomtypeid
			    GROUP BY roomcategory.roomtypeid) AS roomcategory ON roomcategory.rid = roomtype.roomtypeid
			        LEFT JOIN
			    (SELECT 
			        tariffrate.roomtypeid AS rid,
			            MIN(tariffrate.available) AS availabletariff,
			            tariff.*
				    FROM
				        tariffrate
				    LEFT JOIN (SELECT 
				        tariff.roomtypeid AS tariffid, COUNT(*) AS closeouttariff
				    FROM
				        tariffrate AS tariff
				    WHERE
				        closeout = 1
				            AND tariff.ratedate = @checkin
				    GROUP BY tariff.roomtypeid
				) AS tariff ON tariff.tariffid = tariffrate.roomtypeid
			    WHERE
			        tariffrate.ratedate = @checkin
			    GROUP BY tariffrate.roomtypeid) AS tariffrate ON tariffrate.rid = roomtype.roomtypeid
		        LEFT JOIN
			    (SELECT 
			        COUNT(*) AS totalBooking, roomtypeid
			    FROM
			        booking
			    WHERE
			        (booking.checkoutdate > @checkin
			            AND booking.checkindate <= @lastnightdate)
			            AND booking.bookingstatus IN (1 , 2, 3, 5, 6, 8)
			    GROUP BY roomtypeid) AS booking ON booking.roomtypeid = roomtype.roomtypeid
			WHERE roomtype.roomtypeid = :idroomtype
			GROUP BY roomtype.roomtypeid;');
		$stmt->execute([':idroomtype'=>$idroomtype]);
		$availableModel = $stmt->fetch(\PDO::FETCH_ASSOC);
        

        if(Config::get('inventory')=='allotment'){ // allotment
        	$totalavailable=$availableModel['availabletariff'];
        	/*if($availableModel['closeouttariff']>0){
        		$totalavailable=0;
        	}*/
        }else{
            $totalavailable=($availableModel['availablerate']-$availableModel['inhouse']);
        	/*if($availableModel['closeoutrate']>0){
        		$totalavailable=0;
        	}*/
        }
        if($availableModel['closeouttariff']>0 || $availableModel['closeoutrate']>0){
        	$totalavailable = 0;
        }
		return $totalavailable;
	}

	public function style()
	{
		$hotelConf = Config::get('hotelModel');
		$style = $hotelConf->style.PHP_EOL;
		if ($hotelConf->header != null && File::exists('hotelImages/'.$hotelConf->id.'/'.$hotelConf->header)){
			$headerIco = asset('hotelImages/'.$hotelConf->id.'/'.$hotelConf->header);
			if(in_array($hotelConf->template, ['table'])){
				$style .= 'body {
					background: url('.$headerIco.') fixed  center; 
					-webkit-background-size: 100%;
					-moz-background-size: 100%;
					-o-background-size: 100%;
					background-size: 100%;
				}';
			}else{
				$style .= ' #header {background: url('.$headerIco.') no-repeat; background-size: 100%;}';
			}
		}
				
		return \Response::make($style, 200)
        ->header('Content-Type', 'text/css');
	}

	public function bankProcess()
	{
		
		$bankConfig = Config::get('bankConfig');
		$bankList = Config::get('banks');
		if($bankConfig  != null and isset($bankList[$bankConfig->bankKey])) {
			$bankApiParams = $bankList[$bankConfig->bankKey];
			$bankApi = new $bankApiParams['apiPath'];
			$bankConfigParaMeter = json_decode($bankConfig->keyValue);
			foreach($bankApi->fields as $field){

				// if(in_array($field, $bankApi->fieldsRequire) && (!isset($bankConfigParaMeter->{$field}) or $bankConfigParaMeter->{$field} == null)){
				// 	echo '<p>Bank Configuratoin Not Successfully yet</p>';
				// 	echo '<p>Config Require Parameter : '.$field.'</p>';
				// 	die();
				// }else{

					if(property_exists($bankApi, $field) && isset($bankConfigParaMeter->{$field})){
						$bankApi->{$field} = $bankConfigParaMeter->{$field};
					}
				// }
			}

			$bankApi->runAPI();
		}else{
			$bankApi = new \App\API\BANK\NoBank;
		}
	}

    public function test()
    {
    	var_dump(Config::has('hotelModel'));
    	echo Request::url();
    	die();
    	dd($_GET);
    	print_r(DB::connection('hotelDB')->table('roomtype')
        ->join('booking', function ($join) {
            $join->on('roomtype.roomtypeid', '=', 'booking.roomtypeid')
            ->where('booking.commission', '=', 1035);
        })
        //->join('room_guest', )
        ->get());
    	die();
    	print_r(\App\Models\Roomtype::where('roomtypeid', 1508310003 )->get());
    }
}