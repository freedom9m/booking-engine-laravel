<?php

namespace App\Http\Controllers;

use \DB;
use \View;
use App\User;
use App\Http\Controllers\Controller;
use \Validator;
use \Input;
use \Auth;
use \Redirect;
use \Request;
use \Session;
use \File;

class EngineMgrController extends Controller
{
	public function index()
	{
		return View('engineManager.index', []);
	}

	public function hotels()
	{
		$allowed = ['hotelname'];
	    $sort = in_array(Input::get('sort'), $allowed) ? Input::get('sort') : 'id';
	    $order = Input::get('order') === 'asc' ? 'asc' : 'desc'; // default desc
	    $queryStr = null;
	    $nextOrder = 'asc';
	    if($order == 'asc'){
	    	$nextOrder = 'desc';
	    }

	    $server = \App\Models\Hotels::orderBy($sort, $order);
	    $keyword = null;
	    if(Input::has('keyword')){
	    	$keyword = Input::get('keyword');
	    	$server->where('hotelname', 'like', '%'.$keyword.'%');
	    }
		$model = $server->paginate(20);
		return View('engineManager.hotel.index', ['hotels' => $model, 'sort'=>$sort, 'order'=>$nextOrder, 'queryStr' => $queryStr, 'keyword' => $keyword]);
	}

	public function addHotel(Request $request)
	{
		$model = new \App\Models\Hotels;
		if(Request::isMethod('post')){
			if($this->saveHotel($model))
			{
				return Redirect::to('engineManager/editHotel/'.$model->id);
			}
		}
		return View('engineManager.hotel.formHotel', ['model' => $model, 'formType' => 'New']);
	}

	public function editHotel(Request $request, $idHotel)
	{
		$model = \App\Models\Hotels::find($idHotel);
		if(Request::isMethod('post')){
			if($this->saveHotel($model))
			{
				return Redirect::to('engineManager/editHotel/'.$model->id);
			}
		}
		return View('engineManager.hotel.formHotel', ['model' => $model, 'formType' => 'Edit']);
	}

	public function saveHotel($model)
	{
		$model->hotelname = Input::get('hotelname');
		$model->host_database = Input::get('host_database');
		$model->username_database = Input::get('username_database');
		$model->password_database = Input::get('password_database');
		$model->database_name = Input::get('database_name');
		$model->bookingEnabled = Input::get('bookingEnabled');
		$model->description = Input::get('description');
		$model->slug = Input::get('slug');
		$model->star = Input::get('star');
		$model->style = Input::get('style');
		$model->template = Input::get('template') == null ? 'default' : Input::get('template');
		$model->customMailer = Input::get('customMailer');
		$model->rateCloseBoth = Input::get('rateCloseBoth');
		$model->displayCloseOut = Input::get('displayCloseOut');
		$model->debugMode = Input::get('debugMode');
		$model->active = Input::get('active');
		$model->currency = Input::get('currency');
		$model->vat = Input::get('vat');
		if(Input::get('customMailer')!=null){
			$model->mailHost = Input::get('mailHost');
			$model->mailPort = Input::get('mailPort');
			$model->mailUsername = Input::get('mailUsername');
			$model->mailPassword = Input::get('mailPassword');
			$model->mailEncryption = (Input::get('mailEncryption') == '' ? null : Input::get('mailEncryption'));
		}
		
		if($model->save()){
			Session::flash('saveHotelSuccess', 'Data has been saved!');
			if(Input::file('image')){
			  	$input = array('image' => Input::file('image'));
			    $rules = array('image' => 'mimes:jpg,png,jpeg');  // validate file type image
			    ///$rules['image'] = 'mimes:jpg,png,gif,svg,jpeg';
			    $validator = Validator::make($input, $rules);
			    if($validator->fails())
			    { 
					Session::flash('uploadError', 'File allowed only .jpg, .png '); 
			    }
			    else{

			        $file = Input::file('image');
			        $destinationPath = 'hotelImages/'.$model->id;
			        $filename = 'logo.' . Input::file('image')->guessExtension();
			        if($model->logo!=null){
			        	File::delete($destinationPath.DIRECTORY_SEPARATOR.$model->logo);
			        }
			        if(Input::file('image')->move($destinationPath, $filename)){
			        	$model->logo = $filename;
			        }else{
			        	$model->logo = null;
						Session::flash('uploadError', 'File cannot upload file'); 
			        }
			        $model->save();
			    }			
			}
			if(Input::file('imageHeader')){
			  	$input = array('image' => Input::file('imageHeader'));
			    $rules = array('image' => 'mimes:jpg,png,jpeg');  // validate file type image
			    ///$rules['image'] = 'mimes:jpg,png,gif,svg,jpeg';
			    $validator = Validator::make($input, $rules);
			    if($validator->fails())
			    { 
					Session::flash('uploadError', 'File allowed only .jpg, .png '); 
			    }
			    else{

			        $file = Input::file('imageHeader');
			        $destinationPath = 'hotelImages/'.$model->id;
			        $filename = 'header.' . Input::file('imageHeader')->guessExtension();
			        if($model->header!=null){
			        	File::delete($destinationPath.DIRECTORY_SEPARATOR.$model->header);
			        }
			        if(Input::file('imageHeader')->move($destinationPath, $filename)){
			        	$model->header = $filename;
			        }else{
			        	$model->header = null;
						Session::flash('uploadError', 'File cannot upload file'); 
			        }
			        $model->save();
			    }			
			}
			return true;
		}else{
			Session::flash('saveHotelError', 'Error! something went wrong.'); 
			return false;
		}
	}

	public function editStyle(Request $request, $idHotel)
	{
		$model = \App\Models\Hotels::find($idHotel);
		if(Request::isMethod('post')){
			$model->style = Input::get('style');
			if($this->saveHotel($model))
			{
				return Redirect::to('engineManager/editStyle/'.$model->id);
			}
		}
		return View('engineManager.hotel.formStyle', ['model' => $model]);
	}

	public function findSlug()
	{
		
		if(Input::has('slug')){
			if(trim(Input::get('slug')) != null){
				$model = \App\Models\Hotels::where('slug', Input::get('slug'));
				if(Input::get('hotelId')!=''){
					$model->where('id', '!=', Input::get('hotelId'));
				}
				if($model->count() == 0){
					echo 1;
				}else{
					echo 0;
				}
			}else{
				echo 'null';
			}
		}else{
			echo 'null';
		}
	}

	public function banks($idHotel)
	{
		$hotelModel = \App\Models\Hotels::find($idHotel);
		$model = \App\Models\BankApiList::where('idhotel', $idHotel);

		return View('engineManager.bank.index', ['hotelModel' => $hotelModel, 'model' => $model]);
	}

	public function addBank($idHotel)
	{
		$hotelModel = \App\Models\Hotels::find($idHotel);
		$model = new \App\Models\BankApiList;
		if(Request::isMethod('post')){
			if($this->saveApi($model))
			{
				return Redirect::to('engineManager/banks/'.$idHotel.'/edit/'.$model->idbankApiList);
			}
		}
		return View('engineManager.bank.formBank', ['hotelModel' => $hotelModel, 'model' => $model, 'formType' => 'New']);
	}

	public function editBank($idHotel, $idbank)
	{
		$hotelModel = \App\Models\Hotels::find($idHotel);
		$model = \App\Models\BankApiList::find($idbank);
		if(Request::isMethod('post')){
			if($this->saveApi($model))
			{
				return Redirect::to('engineManager/banks/'.$idHotel.'/edit/'.$model->idbankApiList);
			}
		}
		return View('engineManager.bank.formBank', ['hotelModel' => $hotelModel, 'model' => $model, 'formType' => 'Edit']);
	}

	public function saveApi($model)
	{
		$model->apiName = Input::get('apiName');
		$model->idhotel = Input::get('idhotel');
		$model->bankKey = Input::get('bankKey');
		$model->keyValue = json_encode(Input::has('keyValue') ? Input::get('keyValue') : []);
		if($model->save()){
			Session::flash('saveApiSuccess', 'Data has been saved!');
			return true;
		}else{
			Session::flash('saveApiError', 'Error! something went wrong.'); 
			return false;
		}
	}

	public function updateHotelApi()
	{
		try{
			if(Input::has('idhotel') && Input::has('apiId')){
				$model = \App\Models\Hotels::find(Input::get('idhotel'));
				$model->idbankApiList = Input::get('apiId');
				$model->save();
				
				echo 1;
			}
	    }
	    catch(Exception $e){
	       // do task when error
	       echo $e->getMessage();   // insert query
	    }
	}

	public function unRegistered()
	{
		try{
			if(Input::has('idhotel')){
				$model = \App\Models\Hotels::find(Input::get('idhotel'));
				$model->idbankApiList = null;
				$model->save();
				
				echo 1;
			}
	    }
	    catch(Exception $e){
	       // do task when error
	       echo $e->getMessage();   // insert query
	    }
	}

	public function testConnection()
	{
		try {
		$DBH = new \PDO('mysql:host='.Input::get('host_database').';dbname='.Input::get('database_name'), Input::get('username_database'), Input::get('password_database'));
            echo 1;
        }
		catch(\PDOException $e) {
    		echo $e->getMessage();
        }
	}

	public function testMail()
	{
		try{
	        $transport = \Swift_SmtpTransport::newInstance(Input::get('mailHost'), Input::get('mailPort'), Input::get('mailEncryption'));
	        $transport->setUsername(Input::get('mailUsername'));
	        $transport->setPassword(Input::get('mailPassword'));
	        $mailer = \Swift_Mailer::newInstance($transport);
	        $mailer->getTransport()->start();
	        echo 1;
	    } catch (Swift_TransportException $e) {
	        echo $e->getMessage();
	    } catch (Exception $e) {
	      	echo $e->getMessage();
	    }
	}

	public function getApi()
	{
		if(Input::has('bankKey'))
		{
			if(Input::get('apiId') == null){
				$model = null;
			}else{
				$model = \App\Models\BankApiList::find(Input::get('apiId'));
			}


			return View('engineManager.bank.formApi', ['model' => $model, 'bankKey' => Input::get('bankKey')]);
		}else{
		}
	}

	public function deleteAPI()
	{
		if(Input::has('itemId')){
			$model = \App\Models\BankApiList::find(Input::get('itemId'))->delete();
			if($model){
				echo 1;
			}else{
				echo 0;
			}
		}
	}



}