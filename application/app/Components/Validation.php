<?php 
namespace App\Components;
use \DateTime;
/**
* 
*/
class Validation/* extends AnotherClass*/
{
	
	public static function date($date)
	{
	    $d = DateTime::createFromFormat('Y-m-d', $date);
	    return $d && $d->format('Y-m-d') == $date;
	}
}
