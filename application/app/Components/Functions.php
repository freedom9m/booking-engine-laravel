<?php 
namespace App\Components;

use Request;
/**
* 
*/
class Functions
{
	
	public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") { //Kilometers
	    return ($miles * 1.609344);
	  } else if ($unit == "N") { //Miles
	    return ($miles * 0.8684);
	  } else { //Nautical Miles
	    return $miles;
	  }
	}

	public static function checkServerSSL($domainName = null)
	{
		/*$w = stream_get_wrappers();
		print_r($w);
		return in_array('https', $w);
		echo 'openssl: ',  extension_loaded  ('openssl') ? 'yes':'no', "<br>\n";
		echo 'http wrapper: ', in_array('http', $w) ? 'yes':'no', "<br>\n";
		echo 'https wrapper: ', in_array('https', $w) ? 'yes':'no', "<br>\n";
		echo 'wrappers: <pre>', var_dump($w), "<br>";*/

		$url ='https://'.($domainName == null ? Request::server('SERVER_NAME') : $domainName);

		if($fp = tmpfile())
		{
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_STDERR, $fp);
		    curl_setopt($ch, CURLOPT_CERTINFO, 1);
		    curl_setopt($ch, CURLOPT_VERBOSE, 1);
		    curl_setopt($ch, CURLOPT_HEADER, 1);
		    curl_setopt($ch, CURLOPT_NOBODY, 1);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		    $result = curl_exec($ch);
		    //curl_errno($ch)==0 or die("Error:".curl_errno($ch)." ".curl_error($ch));
		    /*var_dump(curl_errno($ch)==0);
		    */
			if(curl_errno($ch)){
			    fseek($fp, 0);//rewind
			    $str='';
			    while(strlen($str.=fread($fp,8192))==8192);
			    return $str;
			    fclose($fp);
		    }else{
		    	echo "Error:".curl_errno($ch)." ".curl_error($ch);
		    }
		}
		return false;


		/*

        flush();
		$stream = @stream_context_create (["ssl" => ["capture_peer_cert" => true]]);
		$read = @fopen($url, "rb", false, $stream);
		var_dump($read);
		if($read)
		{
			$cont = stream_context_get_params($read);
			print_r($cont);
			$var = isset($cont["options"]["ssl"]["peer_certificate"]) ? $cont["options"]["ssl"]["peer_certificate"] : null;
			return (!is_null($var)) ? true : false;
		}else{
			return false;
		}*/
	}

	public static function removeDirectory($dir)
	{
	    $files = array_diff(scandir($dir), ['.','..']);
	    foreach ($files as $file) {
	        (is_dir("$dir/$file")) ? self::removeDirectory("$dir/$file") : unlink("$dir/$file");
	    }
	    return rmdir($dir);
	}

    public static function randomNumber(){
	  $length = 7;
	  $chars = array_merge(range(0,9));
	  shuffle($chars);
	  $password = implode(array_slice($chars, 0,$length));
	  return $password;    	
    }

    public static function tfTostring($boolean){
      return $boolean==true?'true':'false';
    }

	public static function objectGroupData($json, $obj, $options){ 
	  $text=array();
	  $array=json_decode($json, true);
	  if(count($array)!=0){
		foreach($array as $i => $val){
		    $valKey=null;
		    $valValue=null;
		    $options['hasCondition']=isset($options['hasCondition'])?$options['hasCondition']:false;
		    if($options['hasCondition']==true){
		      $valKey=$i;
		      $valValue=$val;
		    }else{
		      $valKey=$val;
		    }
		      
		    if(array_key_exists($valKey, $obj)){
		        $value='';
		        $showOutput=1;
		        if(is_array($obj[$valKey])){
		          $conditionText='';
		          if($options['hasCondition']==true && $obj[$valKey][$options['conditionKey']]!=null && $valValue!=null){
		            $condition=json_decode($obj[$valKey][$options['conditionKey']], true);
		            $conditionText=array_key_exists($valValue, $condition)?$condition[$valValue]:'';
		            if(isset($options['hiddenConditionKey'])){
		           	  $showOutput=in_array($valValue, json_decode($obj[$valKey][$options['hiddenConditionKey']], true))?0:1;//-1 is found
		            }
		          }
		          $value=$conditionText.' '.$obj[$valKey][$options['valueKey']];
		        }else{
		          $value=$obj[$valKey];
		          $showOutput=1;
		        }
		        if($showOutput!=0){
			        $text[]=$value;
		        }
		    }
		}
	  }  	
	  return join(',', $text);

	}

	public static function objectOneData($key, $obj, $valkey=null){
		$text='';
		if($obj!=null){
			if(isset($obj[$key])){
				if($valkey!=null){
				    $text=$obj[$key][$valkey];
				}else{
				    $text=$obj[$key];
				}
			}
		}
	  return $text;
	}

	public static function booleanTF($array){
		$data=array();
		foreach ($array as $key => $value) {
			if(is_array($value)){
				$data[$key]=self::booleanTF($value);
			}else{
				$data[$key]=filter_var($value, FILTER_VALIDATE_BOOLEAN);
			}
		}
		return $data;

	}

	/*public static function listData($models,$valueField)
	{
		if($models!=null){
			$listData=array();
			foreach($models as $model):
				$value=CHtml::value($model,$valueField);
				$listData[$value]=CJSON::decode(CJSON::encode($model));
			endforeach;
		}else{
			$listData=null;
		}

		return $listData;
	}*/

	public static function checkLink($link) {
        flush();
        $fp = @fopen($link, "r");
        @fclose($fp);
        if (!$fp) {
            return false;
        } else {
            return true;
        }
    }

    public static function vatCalculator($price, $vat) { 
	    return ($price/100)*$vat;
	} 

	public static function freeNightCalculate($option = [])
	{
		$totalNight = $option['totalNight'];
		$stay = $option['stay'];
		$freeNight = $option['freeNight'];

		if($stay!=0){
			$calc = (int) $totalNight/$stay;
		}else{
			$calc = 1;
		}
		$free = (int) $calc * $freeNight;

		return $free;
	}

	public static function dateDiff($strDate1, $strDate2) {
	    return (strtotime($strDate2) - strtotime($strDate1)) / ( 60 * 60 * 24 );  // 1 day = 60*60*24
	}

	public static function convertMemory($size)
	{
	    $unit=array('b','kb','mb','gb','tb','pb');
	    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	public static function generateRandomString($length = 10) 
	{
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}

	public static function setTemplate($template, $dataVariable)
	{	
		return str_replace(array_map(
	            function ($v, $k) { 
	                return '{{'.$k.'}}'; 
	            }, 
	            $dataVariable, 
	        	array_keys($dataVariable)
            ), $dataVariable, $template);
	}


	public static function calcCurrency($value=0, $fromCurrency='USD', $toCurrency='EUR', $round=true) {
 		if($fromCurrency === $toCurrency){
 			return $value;
 		}
 		/*$content = file_get_contents('https://www.google.com/finance/converter?a='.$value.'&from='.$fromCurrency.'&to='.$toCurrency);

 		$doc = new \DOMDocument;
 		@$doc->loadHTML($content);
 		$xpath = new \DOMXpath($doc);

 		$result = $xpath->query('//*[@id="currency_converter_result"]/span')->item(0)->nodeValue;*/

 		$doc = new \App\Components\HtmlDom\HtmlDom('https://www.google.com/finance/converter?a='.$value.'&from='.$fromCurrency.'&to='.$toCurrency);

 		$result = $doc->find('#currency_converter_result', 0)->find('span', 0)->plaintext;
 		
 		return str_replace(' '.$toCurrency, '', $result);
	}

}
