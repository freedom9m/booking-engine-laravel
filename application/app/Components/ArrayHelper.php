<?php 
namespace App\Components;
class ArrayHelper
{
  /*i'm for got it*/
  public static function getValue($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }
        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }
        if (is_array($array) && array_key_exists($key, $array)) {
            return $array[$key];
        }
        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }
        if (is_object($array) && isset($array->$key)) {
            return $array->$key;
        } elseif (is_array($array)) {
            return array_key_exists($key, $array) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }

    /*this one too*/
  	public static function getColumn($array, $name, $keepKeys = true)
    {
        $result = [];
        if ($keepKeys) {
            foreach ($array as $k => $element) {
                $result[$k] = static::getValue($element, $name);
            }
        } else {
            foreach ($array as $element) {
                $result[] = static::getValue($element, $name);
            }
        }
        return $result;
    }

    /*sort array by ?*/
	public static function multisort(&$array, $key, $direction = SORT_ASC, $sortFlag = SORT_REGULAR)
    {
        $keys = is_array($key) ? $key : [$key];
        if (empty($keys) || empty($array)) {
            return;
        }
        $n = count($keys);
        if (is_scalar($direction)) {
            $direction = array_fill(0, $n, $direction);
        } elseif (count($direction) !== $n) {
            throw new InvalidParamException('The length of $direction parameter must be the same as that of $keys.');
        }
        if (is_scalar($sortFlag)) {
            $sortFlag = array_fill(0, $n, $sortFlag);
        } elseif (count($sortFlag) !== $n) {
            throw new InvalidParamException('The length of $sortFlag parameter must be the same as that of $keys.');
        }
        $args = [];
        foreach ($keys as $i => $key) {
            $flag = $sortFlag[$i];
            $args[] = static::getColumn($array, $key);
            $args[] = $direction[$i];
            $args[] = $flag;
        }
        $args[] = &$array;
        call_user_func_array('array_multisort', $args);
    }
    

    public static function sortArray($array= array(), $keyDepends = 'depends')
	{
	  	$depend = array();
	  	foreach ($array as $key => $value)
	  	{
		    if (isset($value[$keyDepends]))
		    {
		      	$depend[$key] = $value[$keyDepends];
		    }
		      else
		    {
		      $depend[$key] = null;
		    }

	 	}
	  	//array_multisort($depend, SORT_ASC, $array);
	  	return $depend;
	}

    /*sort array by depends key*/
    public static function sortDepends($array = array(), $keyDepends = 'depends')
    {
        $rearranged = array();
        $arrayLeft = $array;
        do {
          $resolved = false; // Check for rounds without any move
          reset($arrayLeft);
          while (list($key, $val) = each($arrayLeft)) {
            $move = false;
            if (isset($val[$keyDepends]) && !empty($val[$keyDepends])) {
              // Dependencies count must be not greater then rearranged array length
              $depends = count($val[$keyDepends]) <= count($rearranged);

              // All dependencies must be set in rearranged array already
              while ($depends && (list(, $dep_key) = each($val[$keyDepends]))) {
                if (!array_key_exists($dep_key, $rearranged)) {
                  $depends = false;
                }
              }

              $move = $depends;
            } else {
              // Independent items moves to top
              $move = true;
            }

            if ($move) {
              $rearranged[$key] = $val;
              unset($arrayLeft[$key]);
              $resolved = true;
            }
          }
        }
        while ($resolved && !empty($arrayLeft));

        if (empty($arrayLeft)) {
          return $rearranged;
        } else {
          return array('error'=>'There are some unresolved dependencies');
        }
    }


    /*public static function sortByKeyValue($data, $sortKey, $sort_flags=SORT_ASC)
    {
        if (empty($data) or empty($sortKey)) return $data;

        $ordered = array();
        foreach ($data as $key => $value)
            $ordered[$value[$sortKey]] = $value;
        ksort($ordered, $sort_flags);

        return array_values($ordered); // array_values() added for identical result with multisort*
    }*/

    /*sort array by value*/
    public static function sortByKeyValue($data, $field, $sort_flags=SORT_ASC)
    {
        $code = "return strnatcmp(\$a['$field'], \$b['$field']);";
        $sort = '$a,$b';
        if($sort_flags == SORT_DESC){
            $sort = '$b,$a';
        }
        usort($data, create_function($sort, $code));
        return $data;
    }


}