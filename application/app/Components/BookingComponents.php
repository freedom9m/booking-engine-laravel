<?php 
namespace App\Components;

use \DB;
use App\Components\Functions;
/**
* 
*/
class BookingComponents
{
	public static function getSetting($settingKey)
	{
		$model = \App\Models\Settings::where('settingkey', 'maximumBookingPertime')->first();
		if($model->_rowCount!=0 && isset($model->value)){
			return $model->value;
		}
		return 5;
	}

	public static function getCrossPromotion($promotionId)
	{
		$sql = 'select * from mapcrosspromotion inner join promotions on promotions.proid = mapcrosspromotion.promotionid_first where mapcrosspromotion.promotionid_first = :proid';
		$stmt = DB::connection('hotelDB')->getPDO()->prepare($sql);
		$stmt->execute([':proid'=>$proid]);

		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	public static function getPromotion($dataType, $proid)
	{
		$pdo = DB::connection('hotelDB')->getPDO();
		$data = null;
		$crossPromotionId = null;
		if(preg_match('/,/', $proid)){
			$crossId = explode(',', $proid);
			$proid = $crossId[0];
			$crossPromotionId = $crossId[1];
		}
		if(in_array($dataType, ['code', 'promotion', 'crosspromotion'])){
			if($dataType == 'code')
			{
				$sql = 'select 
						code as name, 
						concat_ws(\' \', discount, discount_type) as description,
						code, 
						discount, 
						minstay, 
						case when promotioncode.discount_type=\'% Discount\' then 1 when promotioncode.discount_type = \'Price Discount\' then 2 when promotioncode.discount_type = \'Free Night\' then 3 end as discounttype,
						pid as policyid
				 from promotioncode where proid = :proid';
			}else if(in_array($dataType, ['promotion', 'crosspromotion'])){
				$sql = 'select proname as name, description, null as code, amount as discount, minstay, gettype as discounttype, policyid, arrival, departure, textcolor from promotions where proid = :proid';
			}

			$stmt = $pdo->prepare($sql);
			$stmt->execute([':proid'=>$proid]);

			$data = $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		if($crossPromotionId != null && $dataType == 'crosspromotion'){
			$stmt = $pdo->prepare('select proid, proname as name, description, null as code, amount as discount, minstay, gettype as discounttype, policyid, arrival, departure, textcolor from promotions where proid = :proid');
			$stmt->execute([':proid' => $crossPromotionId]);
			$data['crossPromotion'] = $stmt->fetch(\PDO::FETCH_ASSOC);
		}


		return $data;
	}

	public static function getRoomtype($roomtypeid)
	{
		return DB::connection('hotelDB')->table('roomtype')->leftJoin('occupancy', 'occupancy.roomtypeid', '=', 'roomtype.roomtypeid')->where('roomtype.roomtypeid', $roomtypeid)->first();
	}

	public static function getRate($option)
	{
		$roomtypeId = $option['roomtypeId'];
		$isPromotion = $option['isPromotion'];
		$checkInDate = $option['checkInDate'];
		$checkOutDate = $option['checkOutDate'];
		$lastNightDate = date('Y-m-d', strtotime('-1 days', strtotime($checkOutDate)));

		$numnight = Functions::dateDiff($checkInDate, $checkOutDate);

		if($isPromotion == true){
			$table = 'tariffrate';
			//$sql = 'SELECT sum(doublesell)/'.$numnight.' as price, sum(extrabedadultsell)/'.$numnight.' as extraBedPirce, sum(extrabedchildsell)/'.$numnight.' as extraBedChildPrice, policyid FROM tariffrate WHERE roomtypeid = :roomtypeid and ratedate between :checkInDate AND :lastNightDate group by roomtypeid';
		}else{
			$table = 'ratecategory';
			//$sql = 'SELECT sum(doublesell)/'.$numnight.' as price, sum(extrabedadultsell)/'.$numnight.' as extraBedPirce, sum(extrabedchildsell)/'.$numnight.' as extraBedChildPrice, policyid FROM ratecategory WHERE roomtypeid = :roomtypeid and sourceid='.'4'.' AND ratedate between :checkInDate AND :lastNightDate group by roomtypeid';
		}

		//$params = [':roomtypeid'=>$roomtypeId, ':checkInDate'=>$checkInDate, ':lastNightDate'=>$lastNightDate];

		$option['numNight'] = $numnight;

		$data = DB::connection('hotelDB')->table($table)->select(DB::connection('hotelDB')->raw('sum(doublesell)/'.$numnight.' as price, sum(extrabedadultsell)/'.$numnight.' as extraBedPirce, sum(extrabedchildsell)/'.$numnight.' as extraBedChildPrice, policyid'))->where('roomtypeid', $roomtypeId)->whereBetween('ratedate', [$checkInDate, $lastNightDate]);

		if($isPromotion != true){
			$data->where('sourceid', 4);
		}

		$data->groupBy('roomtypeid');

		$res = $data->first();
		
		if($res == null){
			$res = [];
		}else{
			$res = json_decode(json_encode($res), true);
		}
		return array_merge($res, $option);
	}

	public static function getAmenity($roomtypeId){
		$data = DB::connection('hotelDB')->table('amenity')->select(DB::connection('hotelDB')
			->raw('concat_ws(\'\', group_concat(\'<li>\', description SEPARATOR \'</li>\'), \'</li>\') as amenities'))->where('roomtypeid', $roomtypeId)->first();
		return $data==null ? null : $data->amenities;
	}

	public static function getAbf($isPromotion, $date, $roomTypeId)
	{
		if($isPromotion == true){
			//$sql = 'SELECT breakfast FROM tariffrate WHERE roomtypeid = :roomtypeid and ratedate = :rateDate group by roomtypeid';
			$table = 'tariffrate';
		}else{
			//$sql = 'SELECT breakfast FROM ratecategory WHERE roomtypeid = :roomtypeid and sourceid=4 AND ratedate = :rateDate group by roomtypeid';
			$table = 'ratecategory';
		}
		$model = DB::connection('hotelDB')->table($table)->where('roomtypeid', $roomTypeId)->where('ratedate', $date);

		if($isPromotion == false) {
			$model->where('sourceid', 4);
		}

		$model->groupBy('roomtypeid');

		$data = $model->first();

		return (isset($data->breakfast) && $data->breakfast == 1) ? 'YES' : 'NO';
	}

	public static function getPolicy($pid = null)
	{
		if($pid == null){
			$data = DB::connection('hotelDB')->table('policy')->orderBy('pid', 'asc')->first();
			if($data!=null){
				return $data->pid;
			}
			return null;
		}else{
			return (DB::connection('hotelDB')->table('policy')->where('pid', $pid)->first());
		}
	}

	public static function getAddtionalServices()
	{
		$sql = 'select * from additional where status_flg = 1';
		return DB::connection('hotelDB')->select($sql);
	}

	public static function getAddtionalServicesData($id)
	{
		return DB::connection('hotelDB')->table('additional')->where('additionalid', $id)->first();
	}

	public static function getBookingId()
	{
		$bookingId = date('ymd0001');
		$bookingIdInTable = (DB::connection('hotelDB')->table('booking')->where('requestdate', DB::connection('hotelDB')->raw('curdate()'))->orderBy('bookingid', 'desc')->first());
		if($bookingIdInTable != null){
			$bookingId = $bookingIdInTable->bookingid+1;
		}
		return $bookingId;
	}

	public static function getInvoiceId()
	{
	    $sql = 'SELECT MAX(agentid*1) AS inv FROM booking WHERE sourceid = 4 and requestdate=curdate() AND total NOT IN (0)';
	    $model = DB::connection('hotelDB')->table('booking')->where('sourceid', 4)->where('requestdate', DB::connection('hotelDB')->raw('curdate()'))->select(DB::connection('hotelDB')->raw(' MAX(agentid*1) AS inv'))->first();
        $invId = $model->inv;
        if ($invId == 0) {
            return date('ymd001');
        } else {
            $inv = $invId + 1;
            return $inv;
        }
	}

	public static function getContactInfo($referentId)
	{
		$model = \App\Models\Booking::where('agentid', $referentId)->first();

		return \App\Models\Guest::where('guestid', $model->bookingid)->first();
	}

	public static function getBookingData($referentId)
	{

		$bookings = \App\Models\Booking::where('agentid', $referentId)->get();
		$data = [];
		foreach($bookings as $booking){
			$bookingId = $booking->bookingid;
			$guest = \App\Models\Guest::where('guestid', $bookingId)->first();

			$bookingPrice = \App\Models\BookingPrice::where('bookingid', $bookingId)->get();

			$additionalService = \App\Models\AdditoinalList::where('bookingid', $bookingId)->get();

			$bookingPromotion = \App\Models\BookingPromotion::where('bookingid', $bookingId)->first();

			$bookingChildren = \App\Models\BookingChildren::where('bookingid', $bookingId)->get();

			$policy = self::getPolicy($bookingPromotion->policyid);

			$promotionId = [$bookingPromotion->proid];
			if($bookingPromotion->crosspromotionid != null){
				$promotionId[] = $bookingPromotion->crosspromotionid;
			}

			$promotion = self::getPromotion($bookingPromotion->promotiontype, join(',', $promotionId));
			//$promotion = self::getPromotion($bookingPromotion->promotiontype, $bookingPromotion->proid);

			$data[] = [
				'booking' 			=> $booking,
				'bookingChildren'	=> $bookingChildren,
				'guest'				=> $guest,
				'bookingPrice'		=> $bookingPrice,
				'additional'		=> $additionalService,
				'promotion'			=> $promotion,
				'roomtype'			=> self::getRoomtype($booking->roomtypeid),
				'policy'			=> $policy
			];
		}
		return $data;
	}

	public static function getOccupancy($roomtypeId)
	{
		return DB::connection('hotelDB')->table('occupancy')->where('roomtypeid', $roomtypeId)->first();
	}

	public static function calDiscount($data = ['rateprice'=>0, 'discounttype'=>0, 'discount'=>0])
	{
		$price = $data['rateprice'];
		$oldPrice = null;
		if($data['discounttype'] != null){
			$oldPrice = $price;
			$discount = $data['discount'];
			if($data['discounttype'] == 1){ //% Discount
				$price = $price - ($price * ($discount/100));
			}else if($data['discounttype'] == 2){ //Price Discount
				$price = $price - $discount;
			}else if($data['discounttype'] == 3){ //Free Night
				$oldPrice = null;
			}
		}
		return ['price'=>$price, 'oldPrice'=>$oldPrice];
	}

	public static function calculateDiscount($data)
	{
		extract($data);
		if($discountData['discounttype']==3){ //free night
			$freeNight = Functions::freeNightCalculate([
				'totalNight'	=> $numNight,
				'stay'			=> $discountData['minstay'],
				'freeNight'		=> $discountData['discount'],
			]);

			$newPrice = $priceAllNight - ($pricePerNight*$freeNight);
			$discountPrice = $priceAllNight - $newPrice;
		}else if($discountData['discounttype']==2){ //by price
			$discountPrice = $discountData['discount'] * $numNight;
			$newPrice = $priceAllNight - $discountPrice;
		}else if($discountData['discounttype']==1){ //percentage
			// for discount extra
			//$priceExtra = $priceExtra-(($discountData['discount']/100)*$priceExtra);
			//$priceExtraChild = $priceExtraChild-(($discountData['discount']/100)*$priceExtraChild);

			$newPrice = $priceAllNight - ($discountData['discount']/100)*$priceAllNight;
			$discountPrice = $priceAllNight - $newPrice;
		}

		return ['freeNight' => $freeNight, 'newPrice' => $newPrice, 'discountPrice' => $discountPrice];

	}
	
}