jQuery.fn.isOnScreen = function(scope){
    var element = this;
    if(!element){
        return;
    }
    var target = $(element);
    scope = $(scope || window);
    var scrollTop = scope.scrollTop(),
        scrollBot = scrollTop + scope.height(),
        elTop = target.offset().top,
        elBottom = elTop + scope.outerHeight(),
        visibleTop = elTop < scrollTop ? scrollTop : elTop,
        visibleBottom = elBottom > scrollBot ? scrollBot : elBottom,
        visibleHeight = visibleBottom - visibleTop;
    return (visibleHeight > 0);
};

jQuery.fn.visibleHeight = function(scope){
    var element = this;
    if(!element){
        return;
    }
    var target = $(element);
    scope = $(scope || window);
    var scrollTop = scope.scrollTop(),
        scrollBot = scrollTop + scope.height(),
        elTop = target.offset().top,
        elBottom = elTop + scope.outerHeight(),
        visibleTop = elTop < scrollTop ? scrollTop : elTop,
        visibleBottom = elBottom > scrollBot ? scrollBot : elBottom,
        visibleHeight = visibleBottom - visibleTop;
    return visibleHeight;
}


/*var visibleY = function(el){
    var top = el.getBoundingClientRect().top, rect, el = el.parentNode;
    do {
        rect = el.getBoundingClientRect();
        if (top <= rect.bottom === false)
            return false;
        el = el.parentNode;
    } while (el != document.body);
    // Check its within the document viewport
    return top <= document.documentElement.clientHeight;
};*/

//visibleY(document.getElementById('bookNowBtn'))

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

String.prototype.replaceText = function(find, replace) {
	return this.replace(new RegExp(find, 'g'), replace);
};
