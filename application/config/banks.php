<?php 
return [
	'DEFAULT' => [
		'bankName' => 'No Bank',
		'apiPath' => '\App\API\BANK\NoBank',
	],
	'BKK' => [
		'bankName' => 'Bangkok Bank',
		'apiPath' => '\App\API\BANK\Bangkok\API',
	],
	'KRUNGSRI' => [
		'bankName' => 'Krungsri Bank',
		'apiPath' => '\App\API\BANK\Krungsri\API',
	],
	'PAYPAL' => [
		'bankName' => 'PAYPAL',
		'apiPath' => '\App\API\BANK\Paypal\API',
	],
	'KASIKORN' => [
		'bankName' => 'Kasikorn',
		'apiPath' => 'App\API\BANK\Kasikorn\API',
	],

];