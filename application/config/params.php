<?php 
return [
	'contactEmail' => 'dev10.calypso@gmail.com',
	'contactName' => 'Support',
	'hotelEmail' => 'support@centrumrenovation.com',
	'hotelName' => null,
	'title' => 'Property Management System - ',
	'pictureDir' => '/',
	'enablePromotionCode' => true,
	'pmsLink' => '/pmsStorage/', //happy3image  demo
	'rateHotelId' => 1, //agoda rate hotel ID
	'avaibilityChart' => true,
	'agentRateCompare' => true,

	'tripadvisorCode' => '',

	'paypalPartnerCode' => 'centruminnovations_SP',

	'countries' => ["Thailand", "Adelie Land", "Afghanistan", "Alaska", "Albania", "Andorra", "Angola", "Anguilla", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ascension", "Australia", "Austria", "Azerbaijan", "Azores", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Canary Islands", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chagos Islands", "Chile", "China", "Christmas Island", "Cocos Keeling Islands", "Cook Islands", "Colombia", "Comoros", "Congo", "Costa Rica", "Cote d`Ivoire", "Croatia", "Crozet Archipelago", "Cuba", "Cyphrus", "Czech Republic", "Diego Garcia", "Djibouti", "Denmark", "Domiica", "Dominican Republic", "Easter Island", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guiana", "Guinea", "Guinea-Bissau", "Guyana", "Honduras", "Hong Kong", "Hungary", "Haiti", "Hawaii", "Howland Island", "ICAO", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jarvis Island", "Johnston Island", "Jordan", "Kazakhstan", "Kenya", "Kerguelen", "Kiribati", "Korea", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Madagascar", "Madeira", "Malaysia", "Malawi", "Maldives", "Mali", "Malta", "Marshall Islands", "Marion Island", "Martinique", "Mauritania", "Mayotte", "Mauritius", "Mexico", "Micronesia", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Marianas", "Norway", "Oman", "Pakistan", "Palau", "Palmyra Island", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Phoenix Islands", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Rodrigues", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Paul and Amsterdam", "Saint Pierre and Miquelon", "Samoa (Americal)", "Samoa (Western)", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swan Islands", "Swaziland", "Sweden", "Switzerland", "Syria", "Tanzania", "Taiwan", "Tajikistan", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tristan da cunha", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican", "Venezuela", "Viet Nam", "Virgin Islands (US)", "Virgin Islands (Bristish)", "Wake Island", "Wallis and Futuna", "Western Sahara", "Yemen", "Yugoslavia", "Zaire", "Zambia", "Zanzibar", "Zimbabwe"],
	

	'internetrateSourceid' => 4,


	'langList' => ['en'=>'EN', 'th'=>'TH'],

	'templates' => ['default', 'block', 'table']
];