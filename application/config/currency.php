<?php

return [
	'THB' => ['currencyCode' => 'THB', 'currencySign' => '฿'],
	'USD' => ['currencyCode' => 'USD', 'currencySign' => '$'],
	'SGD' => ['currencyCode' => 'SGD', 'currencySign' => 'S$'],
	'EUR' => ['currencyCode' => 'EUR', 'currencySign' => '€'],
	'JPY' => ['currencyCode' => 'JPY', 'currencySign' => '¥'],
	'TWD' => ['currencyCode' => 'TWD', 'currencySign' => 'TWD'],
];