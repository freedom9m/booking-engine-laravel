<?php 
return [
	'reservation' => 'Reservation',
	'help' => 'Help',
	'contactus' => 'Contact US',

	'quickReserv1' => 'QUICK',
	'quickReserv2' => 'RESERVATION',

	'to' => 'to',

	'pcQuestion1' => 'Do you have a',
	'pcQuestion2' => 'promotion code?',

	'findAvaibilityBtn1' => 'CHECK AVAILABILITY',
	'findAvaibilityBtn2' => 'Best price guaranteed',

	'showDetailBtn' => 'Room details and pictures',
	'detail' => 'Detail',
	'select' => 'SELECT',

	'chooseRoomText' => 'Choose your room',
	'recommendText' => 'You can book multiple room type and promotion at once.', // 'You get the best rates as there is no agent: you are on the hotel website',

	'closeBtn' => 'Close',

	'night' => 'night',

	'checkIn' => 'Arrival',
	'checkOut' => 'Departure',
	'promotionCode' => 'Code',
	'searchBtn' => 'Search',
	'avaibilityChartBtn' => 'Availability chart',
	'promotion' => 'Promotion',
	'policy' => 'Policy',
	'pricePerNight' => 'Price per night',
	'noOfRoom' => 'No. Of Room',
	'maximumRooms' => 'Maximum rooms',
	'totalRooms' => 'Total Rooms',
	'rooms' => 'rooms',
	'totalPrice' => 'Total',
	'bookNowBtn' => 'Book now',
	'breakfastIncluded' => 'Breakfast Included',

	'amentities' => 'Amentities',

	'closeOut' => 'CLOSE OUT',
	'soldOut' => 'SOLD OUT',
	'noRoomAvailable' => 'No have room available',

	'rateFrom' => 'Start from', //'Rate from',

	'rateBtn-show' => 'Show all promotion',
	'rateBtn-hide' => 'Hide',

	'costFor1Night' => ' / Night', //'Cost for 1 night',

	'fillFormText' => 'Please fill your information to require form',

	'copyFieldBtn' => 'copy as guest name',
	'copyAboveBtn' => 'copy name from above',





	'priceBeforeDiscount' => 'Price Before Discount',
	'discount' => 'Discount',
	'priceAfterDiscount' => 'Price After Discount',
	'extraBed' => 'Extra bed',
	'extraBedAdult' => 'Extra bed(Adult)',
	'extraBedChild' => 'Extra bed(Child)',
	'additionalServiceText' => 'Additional Services',
	'notselected' => 'Not Selected', 
	'manageAdditionalServiceBtn' => 'Add Additional Service',
	'hideBtn' => 'hide',
	'totalPriceNoTax' => 'Total Price (without Tax & Service)',
	'totalTaxAndService' => 'Tax & Service Charge',

	'specialRequestText' => 'SPECIAL REQUEST (OPTIONAL)',
	'specialRequestMessage' => 'Note: Your special requests are subject to availability and cannot be guaranteed. Early check-in and late check-out are subject to availability and may incur additional charges. Nonetheless, we will try our best to honour your special requests.',

	'continueBtn' => 'CONTINUE',
	
	'prefix' => 'Prefix',
	'firstname' => 'Firstname',
	'lastname' => 'Lastname',
	'address' => 'Address',
	'city' => 'City',
	'zip' => 'Zipcode',
	'phone' => 'Phone',

	'roomNo' => 'Room No.',
	'noOfGuest' => 'No.Guest',
	'children' => 'Children',
	'childrenAge' => 'Children Age',
	'countryOfPassport' => 'Country of Passport',
	'email' => 'Email',
	'confirmEmail' => 'Confirm Email',

	'dear' => 'Dear',
	'fillWarning' => 'Please fill out your Credit card info and submit form to confirm this booking.',
	'referenceId' => 'Reference ID',
	'totalRooms' => 'Total Rooms',
	'contactname' => 'Contact Name',
	'bookingId' => 'Booking ID',
	'guestName' => 'Guest Name',
	'roomType' => 'Room Type',
	'noOfNight' => 'No. of Night',
	'dateOfArrival' => 'Date of Arrival',
	'dateOfDeparture' => 'Date of Departure',
	'specialRequest' => 'Special Request',
	'roomCharge' => 'Room Charge',
	'price' => 'Price',
	'totalOfBooking' => 'Total of Booking',

	'confirmContinueText' => 'please check at policy',
	'additionalServiceLessTotalText' => 'Warning! additional service cannot less than 0.',

	'confirmContinueText' => 'Please check the box to accept the booking conditions and policies before submitting this booking.',


	'cardHolder' => 'Card Holder',
	'cardType' => 'Card Type',
	'cardNo' => 'Card No',
	'cardExpireMonth' => 'Expire month',
	'cardExpireYear' => 'Expire Year',
	'cardCVV' => 'CVV',

	'reservationText' => 'Your reservation Reference ID is',
	'reservationThankyouText' => 'Thank you for your reservation. Your reservation is confirmed. Please be informed that all special requests are based on availabilities. ',
	'reservationTransactionFiledText' => 'Transaction error please contact Bank',
	'thankyouText' => 'Thank you for choosing ',

	'reservationCancel' => 'Transaction cancel',
	'reservationCancelText' => 'Transaction cancel',

	'reservationFailure' => 'Transaction Failure',
	'reservationFailureText' => 'Transaction Failure',

];