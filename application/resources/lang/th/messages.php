<?php 
return [
	'reservation' => 'สำรองห้องพัก',
	'help' => 'ช่วยเหลือ',
	'contactus' => 'ติดต่อเรา',

	'quickReserv1' => 'QUICK',
	'quickReserv2' => 'RESERVATION',

	'to' => 'ถึง',

	'pcQuestion1' => 'รหัสโปรโมชัน',
	'pcQuestion2' => 'ใช้งาน',

	'findAvaibilityBtn1' => 'ตรวจสอบห้องว่าง',
	'findAvaibilityBtn2' => 'การันตีด้วยราคาที่ดีที่สุด',

	'showDetailBtn' => 'แสดงข้อมูลห้องและรูปภาพ',
	'detail' => 'รายละเอียด',
	'select' => 'เลือก',

	'chooseRoomText' => 'เลือกห้องที่ต้องการ',
	'recommendText' => 'คุณสามารถจองได้หลายห้องและหลายโปรโมชันได้ในครั้งเดียว', // 'คุณจะได้รับราคาที่ดีที่สุดโดยไม่ผ่านนายหน้าได้ในเว็บไซต์ของโรงแรม',

	'closeBtn' => 'ปิด',

	'night' => 'คืน',

	'checkIn' => 'เช็คอิน',
	'checkOut' => 'เช็คเอ้า',
	'promotionCode' => 'รหัสโปรฯ',
	'searchBtn' => 'ค้นหา',
	'avaibilityChartBtn' => 'ตารางห้องว่าง',
	'promotion' => 'โปรโมชัน',
	'policy' => 'เงื่อนไขการจอง',
	'pricePerNight' => 'ราคาต่อห้อง',
	'noOfRoom' => 'จำนวนห้อง',
	'maximumRooms' => 'สูงสุด',
	'totalRooms' => 'จำนวนห้องทั้งหมด',
	'rooms' => 'ห้อง',
	'totalPrice' => 'รวม',
	'bookNowBtn' => 'จองเลย',
	'breakfastIncluded' => 'รวมอาหารเช้า',

	'amentities' => 'สิ่งอำนวยความสะดวก',

	'closeOut' => 'ปิดการขาย',
	'soldOut' => 'ไม่มีห้องว่าง',
	'noRoomAvailable' => 'No have room available',

	'rateFrom' => 'เริ่มต้นที่ ', //'ราคาขั้นต่ำ',

	'rateBtn-show' => 'แสดงโปรโมชันทั้งหมด',
	'rateBtn-hide' => 'ซ่อน',

	'costFor1Night' => ' / คืน', //'Cost for 1 night',

	'fillFormText' => 'Please fill your information to require form',

	'copyFieldBtn' => 'คัดลองไปยังข้อมูลห้อง',
	'copyAboveBtn' => 'คัดลอกจากข้อมูลผู้ติดต่อ',





	'priceBeforeDiscount' => 'ราคาก่อนลด',
	'discount' => 'ส่วนลด',
	'priceAfterDiscount' => 'ราคาหลังลด',
	'extraBed' => 'เตียงเสริม',
	'extraBedAdult' => 'เตียงเสริม(ผู้ใหญ่)',
	'extraBedChild' => 'เตียงเสริม(เด็ก)',
	'additionalServiceText' => 'บริการเสริม',
	'notselected' => 'ยังไม่ได้เลือก', 
	'manageAdditionalServiceBtn' => 'เพิ่มบริการเสริม',
	'hideBtn' => 'ซ่อน',
	'totalPriceNoTax' => 'ราคา (ไม่รวมภาษีและค่าบริการ)',
	'totalTaxAndService' => 'ภาษีและค่าบริการ',

	'specialRequestText' => 'SPECIAL REQUEST (OPTIONAL)',	
	'specialRequestMessage' => 'Note: Your special requests are subject to availability and cannot be guaranteed. Early check-in and late check-out are subject to availability and may incur additional charges. Nonetheless, we will try our best to honour your special requests.',

	'continueBtn' => 'ดำเนินการต่อ',
	
	'prefix' => 'คำนำหน้า',
	'firstname' => 'ชื่อ',
	'lastname' => 'นามสกุล',
	'address' => 'ที่อยู่',
	'city' => 'เมือง',
	'zip' => 'รหัสไปรษณีย์',
	'phone' => 'เบอร์โทร',

	'roomNo' => 'ห้องที่.',
	'noOfGuest' => 'จำนวนผู้พัก',
	'children' => 'จำนวนเด็ก',
	'childrenAge' => 'อายุเด็ก',
	'countryOfPassport' => 'ประเทศ',
	'email' => 'อีเมล์',
	'confirmEmail' => 'ยืนยันอีเมล์',

	'dear' => 'ถึง',
	'fillWarning' => 'Please fill out your Credit card info and submit form to confirm this booking.',
	'referenceId' => 'หมายเลขอ้างอิง',
	'totalRooms' => 'จำนวนห้องทั้งหมด',
	'contactname' => 'ชื่อผุ้ติดต่อ',
	'bookingId' => 'หมายเลขบุ๊คกิ้ง',
	'guestName' => 'ชื่อผู้พัก',
	'roomType' => 'ประเภทห้อง',
	'noOfNight' => 'จำนวนคืนที่พัก',
	'dateOfArrival' => 'วันเข้าพัก',
	'dateOfDeparture' => 'วันออก',
	'specialRequest' => 'คำขอเพิ่มเติม',
	'roomCharge' => 'ค่าบริการ',
	'price' => 'ราคา',
	'totalOfBooking' => 'จำนวนบุ๊คกิ้งทั้งหมด',

	'confirmContinueText' => 'กรุณาคลิกยืนยันการทำรายการ',
	'additionalServiceLessTotalText' => 'คำเตือน บริการเสริมไม่สามารถเลือกน้องกว่า 0 ได้',

	'confirmContinueText' => 'Please check the box to accept the booking conditions and policies before submitting this booking.',


	'cardHolder' => 'ชื่อเจ้าของบัตร',
	'cardType' => 'ประเภทบัตร',
	'cardNo' => 'หมายเลขบัตร',
	'cardExpireMonth' => 'เดือนหมดอายุ',
	'cardExpireYear' => 'ปีหมดอายุ',
	'cardCVV' => 'รหัสความปลอดภัย',

	'reservationText' => 'หมายเลขอ้างอิงการทำรายการของคุณคือ',
	'reservationThankyouText' => 'Thank you for your reservation. Your reservation is confirmed. Please be informed that all special requests are based on availabilities. ',
	'reservationTransactionFiledText' => 'การทำรายการไม่สมบูรณ์ กรุณาติดต่อธนาคาร',
	'thankyouText' => 'Thank you for choosing ',

	'reservationCancel' => 'Transaction cancel',
	'reservationCancelText' => 'Transaction cancel',

	'reservationFailure' => 'Transaction Failure',
	'reservationFailureText' => 'Transaction Failure',

];