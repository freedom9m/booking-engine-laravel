<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/bootstrap/css/bootstrap.min.css')}}"/> 
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pos.css')}}"/> 
    <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/html5-form-validation/dist/jquery.validation.css')}}">
    @yield('headerScript')
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top pos-nav">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	        </button>
	        <a class="navbar-brand" href="#">SYSTEM ARUMIRAI</a>
	    </div>
	    <div id="navbar" class="collapse navbar-collapse">
	        @if (Auth::guest())
	        <ul class="nav navbar-nav">
	            <li><a href="{{ url('engineManager/login') }}">Login</a></li>
	        </ul>
	        @else
	        <ul class="nav navbar-nav">
	            <li class="{{ Menu::active('arai') }}"><a href="{{ url('/') }}">#</a></li>
	        </ul>
	        <ul class="nav navbar-nav navbar-right">
	            <li><a href="{{ url('engineManager/logout') }}">Logout</a></li>
	        </ul>
	        @endif
	    </div><!--/.nav-collapse -->
	  </div>
	</nav>
    @yield('content')
</body>
<script src="{{asset('assets/packages/jquery/jquery-2.2.2.min.js')}}"></script>
<script src="{{asset('assets/packages/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/packages/html5-form-validation/dist/jquery.validation.min.js')}}"></script>
@yield('footerScript')
</html>
