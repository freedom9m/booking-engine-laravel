<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    	<?php 
		$hotelModel = \Config::get('hotelModel');
        
    	 ?>
        <title><?php echo $hotelModel->hotelname ?> - @yield('title')</title>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/bootstrap/css/bootstrap.min.css')}}"/>   
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/jqueryUi/jquery-ui.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/html5-form-validation/dist/jquery.validation.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('reservation/'.\Config::get('slug').'/style')}}">
        <link rel="stylesheet" href="{{asset('assets/templates/'.\Config::get('template').'/style.css')}}">
        @yield('headerScript')
    </head>
    <body>
        <?php $settingsParams = \Config::get('printSettings') ?>
        <header id="header" role="banner">
            <div class="container"><a href="http://{{str_replace(['http://', 'https://'], ['', ''], Config::get('printSettings')->website)}}"><img src="{{ \Config::get('params.pmsLink') }}{{\Config::get('imgPath')}}/{{ $settingsParams->printlogo }}" alt="" id="hotelLogo"></a></div>
        </header>
        <div id="containerBox">
            <form action="{{ URL::route('language-chooser') }}" method="get" style="display: none;">
                <select name="locale" id="">
                    <option value="en">en</option>
                    <option value="th">th</option>
                </select>
                <button type="submit">btn</button>
            </form>
            @yield('content')
        </div>
        <div class="container">
            <div id="footerBox">
                <div class="clearfix">
                    <div class="pull-left">
                        © 2015 CALYPSO SYSTEM All rights reserved.
                    </div>
                    <div class="pull-right">
                        CALYPSO SYSTEM BOOKING ENGINE
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="{{asset('assets/packages/jquery/jquery-2.2.2.min.js')}}"></script>
    <script src="{{asset('assets/packages/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/packages/jqueryUi/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/packages/html5-form-validation/dist/jquery.validation.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    @yield('footerScript')
</html>
