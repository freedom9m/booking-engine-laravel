<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/bootstrap/css/bootstrap.min.css')}}"/> 
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/site.css')}}"/> 
    <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/html5-form-validation/dist/jquery.validation.css')}}">

    <style>
        body{
            padding-top: 69px;
        }
        .contextMenu {
            z-index: 1000;
            position: absolute;
            min-width: 150px;
            overflow: hidden;
            border: 1px solid #CCC;
            white-space: nowrap;
            font-family: sans-serif;
            background: #FFF;
            color: #333;
            border-radius: 5px;
        }

        .contextMenu ul {
            list-style: none;
            padding: 0;
        }
        .contextMenu li {
            padding: 8px 12px;
            cursor: pointer;
        }

        .contextMenu li:hover {
            background-color: #DEF;
        }
    </style>  
    @yield('headerScript')
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Engine Manager</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
        @if (Auth::guest())
        <ul class="nav navbar-nav">
            <li><a href="{{ url('engineManager/login') }}">Login</a></li>
        </ul>
        @else
        <ul class="nav navbar-nav">
            <li class="{{ Menu::active('home') }}"><a href="{{ url('/') }}" target="_blank">Booking Engine</a></li>
            <li class="{{ Menu::active('engineManager') }}"><a href="{{ url('engineManager') }}">Home</a></li>
            <li class="{{ Menu::active('engineManager/hotels') }}"><a href="{{ url('engineManager/hotels') }}">Hotels</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('engineManager/logout') }}">Logout</a></li>
        </ul>
        @endif
    </div><!--/.nav-collapse -->
</div>
</nav>
<?php 
    /*echo \App\Components\Menu::create(function($menu) {
        $menu->add('a', 'Index', URL::route('home'), 1, 'dashboard');
        $menu->add('d', 'bb', URL::route('home'), 1, 'dashboard');
        $menu->add('users', 'Users', URL::route('engineHome'), 100, 'users');
    })->render();*/
 ?>
<?php 
 ?>
<div class="container-fluid">
    @yield('content')
</div>
</body>
<script src="{{asset('assets/packages/jquery/jquery-2.2.2.min.js')}}"></script>
<script src="{{asset('assets/packages/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/packages/html5-form-validation/dist/jquery.validation.min.js')}}"></script>
@yield('footerScript')
</html>
