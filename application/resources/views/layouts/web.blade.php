<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mighty Cloud - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/bootstrap/css/bootstrap.min.css')}}"/> 
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/site.css')}}"/> 
    @yield('headerScript')
</head>
<body>
<nav class="navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Mighty Cloud</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}">Hotels</a></li>
            </ul>
        </div>
    </div>
    <div class="headerBox">
        <img src="{{asset('assets/images/bgtop.jpg')}}" class="headerImg">
    </div>
</nav>

<div class="container">
    @yield('content')
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about">
                    <a href="http://www.calypsosystem.com" target="_blank"><img class="aboutLogo" src="{{asset('assets/images/calypso.png')}}" alt="Calypso System, PMS SYSTEM"></a>
                    <a href="http://www.csloxinfo.com" target="_blank"><img class="aboutLogo" src="{{asset('assets/images/csloxinfo.gif')}}" alt="CS LOXINFO, Internet Datacenter Clound"></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-3">
                        <div><a href="#">Privacy Policy</a></div>
                        <div><a href="#">Terms Of Use</a></div>
                        <div><a href="#">Travel Blog</a></div>
                        <div><a href="#">Media Room</a></div>
                        <div><a href="#">Worldwide Hotels</a></div>
                    </div>
                    <div class="col-sm-3">
                        <div><a href="#">Hotel Partners</a></div>
                        <div><a href="#">Affiliate Partners</a></div>
                        <div><a href="#">Careers</a></div>
                        <div><a href="#">FAQ</a></div>
                    </div>
                    <div class="col-sm-3 last-box">
                        <div><a href="#">FACEBOOK</a></div>
                        <div><a href="#">TWITTER</a></div>
                        <div><a href="#">GOOGLE PLUS</a></div>
                        <div><a href="#">LINKEDIN</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyRight">All material herein © {{date('Y')}} Calypso System., All Rights Reserved. Web design, managed & marketing by Calypso System.</div>
</footer>
</body>
<script src="{{asset('assets/packages/jquery/jquery-2.2.2.min.js')}}"></script>
<script src="{{asset('assets/packages/bootstrap/js/bootstrap.min.js')}}"></script>
@yield('footerScript')
</html>
