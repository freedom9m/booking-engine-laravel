{!! Form::open(array('url' => '', 'id' => 'bookingForm', 'method' => 'get')) !!}
<div class="searchBox">
	<div class="boxHeader">
		Find your hotels
	</div>
	<div class="boxContent">
		Where do you want to go ?
		{{ Form::text('keyword', $keyword, ['class' => 'form-control', 'placeholder' => 'keyword']) }}

		<button class="btn btn-search">Find hotel</button>
	</div>
</div>
{!! Form::close() !!}
