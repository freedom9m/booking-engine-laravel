@extends('layouts.main')

@section('title', 'Engine Manager')

@section('headerScript')

@endsection

@section('content')
	<div class="clearfix">
		<div class="pull-left">
		</div>
		<div class="pull-right">
			<a href="{{ url('engineManager/addHotel') }}">New Hotel</a>
		</div>
	</div>
	{!! Form::open(array('method' => 'get')) !!}
		<div class="input-group" style="max-width:400px">
			<span class="input-group-addon">Search</span>
			{{ Form::text('keyword', $keyword, ['class' => 'form-control', 'placeholder' => 'keyword']) }}
			<div class="input-group-btn"><button class="btn">Find hotel</button></div>
		</div>
	{!! Form::close() !!}
	<br>
	<div class="table-responsive">
		<table class="table table-bordered table-condensed table-striped">
			<thead>
				<tr>
					<th style="width: 7%">Slug</th>
					<th>Name<!-- <a href="{{URL::to('engineManager/hotels?sort=hotelname'.$queryStr)}}"></a> --></th>
					<th style="width: 10%">Debug Mode</th>
					<th style="width: 10%">Booking Enabled</th>
					<th style="width: 10%">BANKS API</th>
					<th style="width: 10%;">Bank Using</th>
				</tr>
			</thead>
			<tbody class="contentBox">
		 	@foreach($hotels as $method)
				<tr>
					<td>
						@if($method->debugMode==1)
						<a href="{{ url('reservation', [$method->slug]) }}debug" target="_blank">{{ $method->slug }}</a>
						@else
						<a href="{{ url('reservation', [$method->slug]) }}" target="_blank">{{ $method->slug }}</a>
						@endif
					</td>
					<td><a href="{{ url('engineManager/editHotel', [$method->id]) }}">{{ $method->hotelname }}</a></td>
					<td>
						@if($method->debugMode==1)
						<span class="bookingActive">Yes</span>
						@else
						<span class="bookingNoActive">No</span>
						@endif
					</td>
					<td>
						@if($method->bookingEnabled==1)
						<span class="bookingActive">Yes</span>
						@else
						<span class="bookingNoActive">No</span>
						@endif
					</td>
					<td>{{ $method->bankApiList()->count() }}</td>
					<td><a href="{{ url('engineManager/banks', [$method->id]) }}">{{ isset(\Config::get('banks')[$method->bankApi['bankKey']]['bankName']) ? \Config::get('banks')[$method->bankApi['bankKey']]['bankName'] : 'No use' }}</a></td>
				</tr>
		 	@endforeach
	 		</tbody>
	 	</table>
	</div>
 	{{ $hotels->render() }}
@endsection

@section('footerScript')
<script>
	$(document).ready(function() {
		/*$('body').on('contextmenu', '.contentBox tr', function(event) {
            event.preventDefault();
            $('.contextMenu').remove();
            var element = document.createElement('div');
            element.className = 'contextMenu';
            element.style.top = event.pageY + 'px';
            element.style.left = event.pageX + 'px';
            //element.style.zIndex = 999;
            element.innerHTML = '<ul>'
                    +'<li>Sub 1</li>'
                    +'<li>Sub 2</li>'
                    +'<li>Sub 3</li>'
                    +'<li>Sub 4</li>'
                    +'<li>Sub 5</li>'
                +'</ul>';

            $('body').append(element);
        });


        $(document).click(function(event) { 
            if(!$(event.target).closest('.contextMenu').length && !$(event.target).is('.contextMenu')) {
                if($('.contextMenu').length>0) {
                    $('.contextMenu').remove();
                }
            }        
        });*/
	});

</script>

@endsection