@extends('layouts.main')

@section('title', 'Engine Manager')

@section('headerScript')

@endsection

@section('content')
<ol class="breadcrumb">
  <li><a href="{{ url('engineManager/hotels') }}">Hotels</a></li>
  <li><a href="#">{{$model->hotelname}}</a></li>
  <li><a href="#">edit style</a></li>
</ol>
<div class="row" style="max-width: 1000px; margin: 0 auto">
	<div class="col-md-3">
		<ul>
  			<li><a href="{{ url('engineManager/hotels') }}">Hotels</a></li>
			<li><a href="{{ url('engineManager/editHotel/'.$model->id)}}">Colors Manager</a></li>
			<li><a href="#">Edit style</a></li>
		</ul>
	</div>
	<div class="col-md-9">
		{!! Form::open(['id' => 'styleForm', 'files'=>true]) !!}

				{{ Form::label('description', 'Description', ['class' => 'control-label']) }}
				{{ Form::textarea('description', $model->description, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'description', 'rows' => 3]) }}
			<div class="panel-footer"><button type="submit" id="submitBtn" class="btn" >save</button></div>

		{!! Form::close() !!}		
	</div>
@endsection

@section('footerScript')

@endsection