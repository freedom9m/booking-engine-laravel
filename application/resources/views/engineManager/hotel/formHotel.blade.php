@extends('layouts.main')

@section('title', 'Engine Manager')

@section('headerScript')
<link rel="stylesheet" href="{{asset('assets/packages/codemirror/lib/codemirror.css')}}">
<link rel="stylesheet" href="{{asset('assets/packages/codemirror/addon/dialog/dialog.css')}}">
<link rel="stylesheet" href="{{asset('assets/packages/codemirror/addon/search/matchesonscrollbar.css')}}">
<link rel="stylesheet" href="{{asset('assets/packages/codemirror/addon/display/fullscreen.css')}}">
<link rel="stylesheet" href="{{asset('assets/packages/codemirror/theme/monokai.css')}}">
@endsection

@section('content')
<ol class="breadcrumb">
  <li><a href="{{ url('engineManager/hotels') }}">Hotels</a></li>
  <li class="active"><a href="#">{{ $formType }}</a></li>
</ol>
<div class="row" style="max-width: 1000px; margin: 0 auto">
	@if($model->id !== null)
	<div class="col-md-3">
		<ul>
  			<li><a href="{{ url('engineManager/hotels') }}">Hotels</a></li>
			<li><a href="{{ url('engineManager/banks/'.$model->id)}}">Banks APIS</a></li>
			<li><a href="{{ url('engineManager/banks/'.$model->id.'/add') }}">New API</a></li>
		</ul>
	</div>
	<div class="col-md-9">
	@else
	<div class="col-md-12">
	@endif
		@if(Session::has('saveHotelSuccess'))
		<p class="alert alert-success">{{ Session::get('saveHotelSuccess') }}</p>
		@endif
		@if(Session::has('saveHotelError'))
		<p class="alert alert-danger">{{ Session::get('saveHotelError') }}</p>
		@endif
		@if(Session::has('uploadError'))
		<p class="alert alert-danger">{{ Session::get('uploadError') }}</p>
		@endif
		{!! Form::open(['id' => 'hotelForm', 'files'=>true]) !!}
		<div class="panel panel-default">
			<div class="panel-heading">{{$formType}} Hotel {{ $model->hotelname }}</div>
			<div class="panel-body">
				<div>
					{{ Form::label('hotelname', 'Hotel name', ['class' => 'control-label']) }}
					{{ Form::text('hotelname', $model->hotelname, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Hotel Name']) }}
				</div>
				<hr>
				<div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation"><a href="#database-tab" aria-controls="database-tab" role="tab" data-toggle="tab">Database</a></li>
				    <li role="presentation"><a href="#mail-tab" aria-controls="mail-tab" role="tab" data-toggle="tab">Mail</a></li>
				    <li role="presentation" class="active"><a href="#engine-tab" aria-controls="engine-tab" role="tab" data-toggle="tab">Booking Engine</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane" id="database-tab">
				    	<div style="max-width: 300px;">
							<h4>Connection</h4>
							{{ Form::label('host_database', 'Host', ['class' => 'control-label']) }}
							{{ Form::text('host_database', $model->host_database, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Database Name']) }}
							{{ Form::label('username_database', 'Username', ['class' => 'control-label']) }}
							{{ Form::text('username_database', $model->username_database, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Username']) }}
							{{ Form::label('password_database', 'Password', ['class' => 'control-label']) }}
							{{ Form::text('password_database', $model->password_database, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Password']) }}
							{{ Form::label('database_name', 'Database Name', ['class' => 'control-label']) }}
							{{ Form::text('database_name', $model->database_name, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Database Name']) }}
							<br>
							<button class="btn btn-info" type="button" id="testConnectionBtn">Test Connection</button> <span id="connectionRes"></span>
						</div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="mail-tab">
				    	<div style="max-width: 300px;">
							<h4>Mail Configuration</h4>
							{{ Form::label('customMailer', 'Custom Mailer', ['class' => 'control-label']) }}
							{{ Form::checkbox('customMailer', 1, $model->customMailer) }}
							<br>
							{{ Form::label('mailHost', 'SMTP Server', ['class' => 'control-label']) }}
							{{ Form::text('mailHost', $model->mailHost, ['class' => 'form-control form-mail', 'data-validation' => '[NOSPACE]', 'data-validation-label' => 'SMTP Server']) }}
							{{ Form::label('mailPort', 'SMTP Port', ['class' => 'control-label']) }}
							{{ Form::text('mailPort', $model->mailPort, ['class' => 'form-control form-mail', 'data-validation' => '[NOSPACE]', 'data-validation-label' => 'SMTP Port']) }}
							{{ Form::label('mailUsername', 'Username', ['class' => 'control-label']) }}
							{{ Form::text('mailUsername', $model->mailUsername, ['class' => 'form-control form-mail', 'data-validation' => '[NOSPACE]', 'data-validation-label' => 'Username']) }}
							{{ Form::label('mailPassword', 'Password', ['class' => 'control-label']) }}
							{{ Form::text('mailPassword', $model->mailPassword, ['class' => 'form-control form-mail', 'data-validation' => '[NOSPACE]', 'data-validation-label' => 'Password']) }}
							{{ Form::label('mailEncryption', 'Encryption', ['class' => 'control-label']) }}
							{{ Form::select('mailEncryption', array('' => 'NONE', 'ssl' => 'SSL', 'tls' => 'TLS'), $model->mailEncryption, ['class' => 'form-control form-mail', 'data-validation' => '[NOSPACE]', 'data-validation-label' => 'Encryption']) }}
							<br>
							<button class="btn btn-info form-mail" type="button" id="testMailBtn">Test Connection</button> <span id="mailRes"></span>
						</div>
				    </div>
				    <div role="tabpanel" class="tab-pane active" id="engine-tab">
						<h4>Booking Engine</h4>
						{{ Form::checkbox('bookingEnabled', 1, $model->bookingEnabled) }}
						{{ Form::label('bookingEnabled', 'Enable Booking Engine', ['class' => 'control-label']) }} 
						{{ Form::checkbox('active', 1, $model->active) }}
						{{ Form::label('active', 'Active', ['class' => 'control-label']) }} 
						{{ Form::checkbox('debugMode', 1, $model->debugMode) }}
						{{ Form::label('debugMode', 'Debug Mode', ['class' => 'control-label']) }} *** Debug mode will show db error report
						<br>
						{{ Form::label('slug', 'SLUG', ['class' => 'control-label']) }} <span style="color: red">** booking url</span>
						<div>{{ url('reservation') }}/</div>
						<div>{{ Form::text('slug', $model->slug, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY,NOSPACE,MIXED]', 'data-validation-label' => 'Slug', 'placeholder' => $model->slug, 'style' => 'max-width: 300px;']) }} </div>
						<div id="slugRes"></div>
						<h4>Booking Settings</h4>
						{{ Form::checkbox('rateCloseBoth', 1, $model->rateCloseBoth) }}
						{{ Form::label('rateCloseBoth', 'Close Both', ['class' => 'control-label']) }} ** room will close both when rate close
						<br>
						{{ Form::checkbox('displayCloseOut', 1, $model->displayCloseOut) }}
						{{ Form::label('displayCloseOut', 'Display Close Out Room', ['class' => 'control-label']) }}
						<br>
						{{ Form::label('currency', 'Currency', ['class' => 'control-label']) }}
						<?php 
						$currency = [];
						foreach (\Config::get('currency') as $key => $value) {
							$currency[$key] = $value['currencyCode'];
						}
						 ?>
						{{ Form::select('currency', $currency, $model->currency) }}
						<br>
						{{ Form::label('vat', 'Vat', ['class' => 'control-label']) }}
						<div>{{ Form::text('vat', $model->vat, ['class' => 'form-control', 'data-validation' => '[NUMERIC]', 'data-validation-label' => 'Vat', 'placeholder' => $model->vat, 'style' => 'max-width: 300px;']) }} </div>
						<hr>

						{{ Form::label('description', 'Description', ['class' => 'control-label']) }}
						{{ Form::textarea('description', $model->description, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'description', 'rows' => 3]) }}
						
						{{ Form::label('star', 'Star', ['class' => 'control-label']) }}
						@for($i = 1; $i<=5; $i++)
							{{ Form::radio('star', $i, $i == $model->star, ['id' => 'star_'.$i]) }} {{ Form::label('star_'.$i, $i, ['class' => 'control-label']) }}
						@endfor
						<hr>
						{{ Form::label('template', 'Template', ['class' => 'control-label']) }}
						<div class="row">
						@foreach(\Config::get('params.templates') as $name)
							<div class="col-xs-3">
								<a href="{{asset('assets/templates/'.$name.'/template.png')}}" target="_blank"><img src="{{asset('assets/templates/'.$name.'/template.png')}}" style="width: 100%;"></a>
								{{ Form::radio('template', $name, $name == $model->template, ['id'=>'template_'.$name]) }} <label for="template_{{$name}}">{{ucfirst($name)}}</label>
							</div>
						@endforeach
						</div>

						{{ Form::label('style', 'style', ['class' => 'control-label']) }}
						{{ Form::textarea('style', $model->style, ['class' => 'form-control', 'data-validation' => '', 'data-validation-label' => 'style', 'rows' => 5]) }}

						** F11 = full screen, ESC or F11 = exit full screen, CTRL+F = search

						<hr>
						{{ Form::label('image', 'Hotel Logo', ['class' => 'control-label']) }}
						{!! Form::file('image') !!}
						<br>
						@if (File::exists('hotelImages/'.$model->id.'/'.$model->logo) && $model->logo != '')
							<a href="{{ asset('hotelImages/'.$model->id.'/'.$model->logo)}}" target="_blank">
								<img src="{{ asset('hotelImages/'.$model->id.'/'.$model->logo)}}", style="width: 100%; max-width: 150px;">
							</a>
						@endif
						<hr>
						{{ Form::label('imageHeader', 'Booking Header', ['class' => 'control-label']) }} ** using as background for "table" template
						{!! Form::file('imageHeader') !!}
						<br>
						@if (File::exists('hotelImages/'.$model->id.'/'.$model->header) && $model->header != '')
							<a href="{{ asset('hotelImages/'.$model->id.'/'.$model->header)}}" target="_blank">
								<img src="{{ asset('hotelImages/'.$model->id.'/'.$model->header)}}", style="width: 100%;">
							</a>
						@endif
				    </div>
				  </div>
				</div>


			</div>
			<div class="panel-footer"><button type="submit" id="submitBtn" class="btn" {{$formType == 'new' ? 'disabled' : null}}>save</button></div>
		</div>	
		{!! Form::close() !!}		
	</div>
</div>

@endsection

@section('footerScript')
<script src="{{asset('assets/packages/codemirror/lib/codemirror.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/mode/css/css.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/addon/dialog/dialog.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/addon/search/searchcursor.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/addon/search/search.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/addon/scroll/annotatescrollbar.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/addon/search/matchesonscrollbar.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/addon/search/jump-to-line.js')}}"></script>
<script src="{{asset('assets/packages/codemirror/addon/display/fullscreen.js')}}"></script>
<script>
	$(document).ready(function() {
		function mailerChecked()
		{
			if($('#customMailer').is(':checked')){
				$('.form-mail').prop('disabled', false);
			}else{
				$('.form-mail').prop('disabled', true);
			}			
		}

		mailerChecked();

		$('body').on('change', '#customMailer', function(e){
			mailerChecked();
		});

		var editor = CodeMirror.fromTextArea(document.getElementById("style"), {
		 	mode: "text/css",
		  	lineNumbers: true,
		  	extraKeys: {
				"Alt-F": "findPersistent",
				"F11": function(cm) {
		      		cm.setOption("fullScreen", !cm.getOption("fullScreen"));
		    	},
		    	"Esc": function(cm) {
		      		if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
		    	}
		  	},
          	viewportMargin: Infinity,
          	theme: 'monokai',

		});

		$('#hotelForm').validate({
    		/*dynamic: {
		        settings: {
		            trigger: "keyup"
		        },
		    },*/
		    submit: {
		    	settings: {
		    		inputContainer: ".form-group"
		    	},
		    },
		});

		$('body').on('keyup', '#slug', function(){
			$.ajax({
				url: '{{url('engineManager/findSlug')}}',
				type: 'get',
				dataType: 'html',
				data: {slug: $('#slug').val(), hotelId: '{{$model->id}}'},
			})
			.done(function(res) {
				if(res==0){
					$('#submitBtn').prop('disabled', true);
					$('#slugRes').html('<span style="color: red">data exists</span>');
				}else if(res == 'null'){
					$('#submitBtn').prop('disabled', true);
					$('#slugRes').html('<span style="color: red">Cannot be null</span>');
				}else{
					$('#submitBtn').prop('disabled',  false);
					$('#slugRes').html('<span style="color: green">available</span>');
				}
			})
			.fail(function() {
				$('#submitBtn').prop('disabled', true);
				Alert('error cannot get information');
			})
			.always(function() {
			});

			
		});

		$('body').on('click', '#testConnectionBtn', function(){
			$.ajax({
				url: '{{ url('engineManager/testConnection') }}',
				type: 'POST',
				dataType: 'html',
				data: $('#hotelForm').serialize(),
				beforeSend: function(){
					$('#connectionRes').html('loading.....');
				}
			})
			.done(function(res) {
				if(res==1){
					$('#submitBtn').prop('disabled', false);
					$('#connectionRes').html('<span style="color: green">Connection working</span>');
				}else{
					$('#submitBtn').prop('disabled', true);
					$('#connectionRes').html('<span style="color: red">'+res+'</span>');
				}
			})
			.fail(function() {
				$('#submitBtn').prop('disabled', true);
				$('#connectionRes').html('<span style="color: red">Connection not working</span>');
			})
			.always(function() {
			});

		});

		$('body').on('click', '#testMailBtn', function(){
			$.ajax({
				url: '{{ url('engineManager/testMail') }}',
				type: 'POST',
				dataType: 'html',
				data: $('#hotelForm').serialize(),
				beforeSend: function(){
					$('#mailRes').html('loading.....');
				}
			})
			.done(function(res) {
				if(res==1){
					$('#submitBtn').prop('disabled', false);
					$('#mailRes').html('<span style="color: green">Connection working</span>');
				}else{
					$('#submitBtn').prop('disabled', true);
					$('#mailRes').html('<span style="color: red">'+res+'</span>');
				}
			})
			.fail(function() {
				$('#submitBtn').prop('disabled', true);
				$('#mailRes').html('<span style="color: red">Connection not working</span>');
			})
			.always(function() {
			});

		});
	});	
</script>
@endsection
