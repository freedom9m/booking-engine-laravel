<?php 
$banks = \Config::get('banks');
if(isset($banks[$bankKey])){
	$apiData = $banks[$bankKey];
	$api = new $apiData['apiPath'];
	$fieldData = [];
	if($model != null){
		$fieldData = json_decode($model->keyValue);
	}
	$fieldsRequire = $api->fieldsRequire;
?>
	@foreach($api->fields as $key)
		@if(!in_array($key, [$api->refField, $api->moneyField]))

		{{ Form::label('keyValue['.$key.']', $key, ['class' => 'control-label']) }}
		{{ Form::text('keyValue['.$key.']', isset($fieldData->{$key}) ? $fieldData->{$key} : null, ['class' => 'form-control', 'data-validation' => '['.(in_array($key, $fieldsRequire) ? 'NOTEMPTY' : '').']', 'data-validation-label' => $key]) }}
		@endif
	@endforeach

<?php
}
?>
