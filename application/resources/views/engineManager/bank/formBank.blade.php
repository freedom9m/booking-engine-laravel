<?php 
$bankParams = [];
foreach(\Config::get('banks') as $key => $arr)
{
	$bankParams[$key] = $arr['bankName'];
}


 ?>
@extends('layouts.main')

@section('headerScript')

@endsection

@section('content')
<ol class="breadcrumb">
  <li><a href="{{ url('engineManager/hotels') }}">Hotels</a></li>
  <li><a href="{{ url('engineManager/editHotel/'.$hotelModel->id) }}">{{ $hotelModel->hotelname }}</a></li>
  <li><a href="{{ url('engineManager/banks/'.$hotelModel->id) }}">Bank APIS</a></li>
  <li class="active"><a href="#">{{ $formType }}</a></li>
</ol>
@if(Session::has('saveApiSuccess'))
<p class="alert alert-success">Data has been saved!</p>
@endif
@if(Session::has('saveApiError'))
<p class="alert alert-danger">Error! something went wrong.</p>
@endif

	{!! Form::open(['id' => 'bankForm']) !!}
		<div class="panel panel-default">
			<div class="panel-heading">{{$formType}} API</div>
			<div class="panel-body">
				{{ Form::hidden('idhotel', $hotelModel->id) }}
				{{ Form::hidden('idbankApiList', $model->idbankApiList) }}
				{{ Form::label('apiName', 'API NAME', ['class' => 'control-label', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'apiName']) }}
				{{ Form::text('apiName', $model->apiName, ['class' => 'form-control form-api', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'API NAME']) }}
				{{ Form::label('bankKey', 'Select API', ['class' => 'control-label', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Select API']) }}
		    	{{ Form::select('bankKey', $bankParams, $model->bankKey, ['class' => 'form-control form-api']) }} 		
				
				<h4>field Values</h4>
				<div style="color: red">** warning</div>
				<div id="bankApiContent"></div>

			</div>
			<div class="panel-footer"><button type="submit" class="btn">save</button></div>
		</div>	
	{!! Form::close() !!}

@endsection

@section('footerScript')
	<script>
		function loetApi()
		{
			$.ajax({
				url: '{{ url('engineManager/banks/'.$hotelModel->id.'/getApi') }}',
				type: 'POST',
				dataType: 'html',
				data: {_token: $('input[name="_token"]').val(), apiId: $('input[name="idbankApiList"]').val(), bankKey: $('#bankKey').val()},
				beforeSend: function(){
					$('#bankApiContent').html('Loading');
				}
			})
			.done(function(data) {
				$('#bankApiContent').html(data);
			})
			.fail(function() {
				$('#bankApiContent').html('Error cannot load information');
			})
			.always(function() {
				
			});
			
		}

		$(document).ready(function() {
			loetApi();
			$('body').on('change', '#bankKey', function(){
				loetApi();
			});
		});
	</script>
@endsection