@extends('layouts.main')

@section('headerScript')
<style>
	.active-item {
    	background-color: #FFF3B6 !important;
	}
</style>
@endsection

@section('content')
<ol class="breadcrumb">
  <li><a href="{{ url('engineManager/hotels/') }}">Hotels</a></li>
  <li><a href="{{ url('engineManager/editHotel/'.$hotelModel->id) }}">{{ $hotelModel->hotelname }}</a></li>
  <li><a href="#">Bank APIS</a></li>
</ol>
<?php 
$sslChecker = \App\Components\Functions::checkServerSSL();
 ?>
@if($sslChecker)
	<!--  /*Request::secure()*/ -->
	<div class="alert alert-danger"><h4 style="margin: 0">{{$sslChecker}}<!-- Warning! this server no have <a href="https://en.wikipedia.org/wiki/Transport_Layer_Security" target="_blank">SSL Protocol</a>, financial transactions should have it. --></h4></div>
@endif
<div style="max-width: 1000px; margin: 0 auto">
	<div class="clearfix">
		<div class="pull-left">
			Total Bank APIS {{ $model->count() }}
		</div>
		<div class="pull-right">
			<a href="{{ url('reservation/'.$hotelModel->slug) }}" target="_blank">Test Booking</a> | 
			<a href="{{ url('engineManager/banks/'.$hotelModel->id.'/add') }}">New API</a>
		</div>
	</div>
	{!! Form::open(['id' => 'bankForm']) !!}
	{{ Form::hidden('idhotel', $hotelModel->id) }}
	<div class="table-responsive">
		<table class="table table-bordered table-condensed table-striped custom-border">
			<thead>
				<tr>
					<th>API NAME</th>
					<th>Bank Name</th>
					<th style="width: 5%; text-align: center;">Using</th>
					<th style="width:10%; text-align: center;">Operation</th>
				</tr>
			</thead>
			<tbody>
			<?php $bankParams = \Config::get('banks'); ?>
			@if($model->count() > 0)
			@foreach($model->get() as $method)
				<tr data-id="{{ $method->idbankApiList }}" class="{{ $hotelModel->idbankApiList == $method->idbankApiList ? 'active-item' : '' }}">
					<td>{{ $method->apiName }}</a></td>
					<td>{{ isset($bankParams[$method->bankKey]) ? $bankParams[$method->bankKey]['bankName'] : 'Unknow' }}</td>
					<td align="center">{{ Form::radio('apiId', $method->idbankApiList, $hotelModel->idbankApiList == $method->idbankApiList, ['class' => 'usingBank']) }}</td>
					<td align="center"><a href="{{ url('engineManager/banks/'.$hotelModel->id.'/edit/'.$method->idbankApiList) }}"><span class="glyphicon glyphicon-edit"></span></a> | <a href="javascript:void(0);" class="deleteItem"><span class="glyphicon glyphicon-trash"></span></a></td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="100%" align="center">No data record</td>
				</tr>
			@endif
			</tbody>
		</table>
	</div>
	{!! Form::close() !!}
	<div align="right"><button class="btn btn-warning" type="button" id="resetAPI">Reset to default(no api)</button></div>
</div>
@endsection

@section('footerScript')
	<script>
		$(document).ready(function() {
			$('body').on('click', '#resetAPI', function(){
				if(confirm('Are you sure to reset api to default payment gateway?'))
				{
					$.ajax({
						url: '{{ url('engineManager/unRegistered')}}',
						type: 'POST',
						dataType: 'html',
						data: $('#bankForm').serialize(),
					})
					.done(function(res) {
						if(res == 1){
							$('.active-item').removeClass('active-item');
							$('.usingBank').prop('checked', false);
						}else{
							alert('Error update');
						}
					})
					.fail(function() {
					})
					.always(function() {
						
					});
					
				}
			});


			var currentVal = $('.usingBank:checked').val();
			$('body').on('change', '.usingBank', function(){
				var this_ = $(this);
				var newVal = $(this).val();
				if(confirm('Confirm Change'))
				{
					$.ajax({
						url: '{{ url('engineManager/updateApi')}}',
						type: 'POST',
						dataType: 'html',
						data: $('#bankForm').serialize(),
					})
					.done(function(res) {
						if(res == 1)
						{
							$('.active-item').removeClass('active-item');
							this_.parent().parent().addClass('active-item');
							alert('Engine Updated');
						}else{
							alert('Error! cannot update data');
						}
						currentVal = newVal;
					})
					.fail(function() {
						alert('Error! cannot update data');
						$('.usingBank[value="'+currentVal+'"]').prop('checked', true); //.button("refresh")
					})
					.always(function() {
						
					});
					
				}
				else
				{
					$('.usingBank[value="'+currentVal+'"]').prop('checked', true); //.button("refresh")
				}
			});


			$('body').on('click', '.deleteItem', function(){
				var this_ = $(this).parent().parent();
				var textConfirm = '';
				if(this_.hasClass('active-item')){
					textConfirm = 'This record currenly active ';
				}
				if(confirm(textConfirm+' Are you sure to delete this item?')){
					$.ajax({
						url: '{{ url('engineManager/deleteApi') }}',
						type: 'post',
						dataType: 'html',
						data: {itemId: this_.data('id')},
					})
					.done(function(data) {
						if(data == 1){
							alert('data has beed deleted');
							this_.remove();
						}else{
							alert('Error! cannot delete data');
						}
					})
					.fail(function() {
						alert('Error! cannot delete data');
					})
					.always(function() {
					});
					
				}
			});
		});
	</script>
@endsection