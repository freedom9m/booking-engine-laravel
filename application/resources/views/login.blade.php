
@extends('layouts.main')

@section('title', 'Hotel Lists')

@section('headerScript')
<style>
	.loginBox {
		position: relative;
		margin: 50px auto;
		width: 100%;
		max-width: 300px;
		box-sizing: border-box;
		box-shadow: 2px 2px 5px 1px rgba(0, 0, 0, 0.2);
		border-radius: 3px;
		padding: 15px;
	}
	.input-group-material {
		position: relative;
		margin-top: 30px;
		margin-bottom: 5px;
	}

	input:-webkit-autofill,
	input:-webkit-autofill:hover,
	input:-webkit-autofill:focus,
	input:-webkit-autofill:active {
		transition: background-color 5000s ease-in-out 0s;
	}

	.input-group-material input {
		background: none;
		width: 100%;
		font-size: 18px;
		padding: 5px;
		display: block;
		border: none;
		border-bottom: 1px solid #18aa8d;
	}

	.input-group-material input:focus {
		outline: none;
	}


	/* LABEL ======================================= */

	.input-group-material label {
		color: #999;
		font-size: 18px;
		font-weight: normal;
		position: absolute;
		pointer-events: none;
		left: 5px;
		top: 5px;
		transition: 0.2s ease all;
		-moz-transition: 0.2s ease all;
		-webkit-transition: 0.2s ease all;
	}


	/* active state */

	.input-group-material input:focus ~ label,
	.input-group-material input:valid ~ label {
		top: -20px;
		font-size: 14px;
		color: #18aa8d;
	}


	/* BOTTOM BARS ================================= */

	.input-group-material .bar {
		position: relative;
		display: block;
		width: 100%;
	}

	.input-group-material .bar:before,
	.input-group-material .bar:after {
		content: '';
		height: 2px;
		width: 0;
		bottom: 1px;
		position: absolute;
		background: #18aa8d;
		transition: 0.2s ease all;
		-moz-transition: 0.2s ease all;
		-webkit-transition: 0.2s ease all;
	}

	.input-group-material .bar:before {
		left: 50%;
	}

	.input-group-material .bar:after {
		right: 50%;
	}


	/* active state */

	.input-group-material input:focus ~ .bar:before,
	.input-group-material input:focus ~ .bar:after {
		width: 50%;
	}


	/* HIGHLIGHTER ================================== */

	.input-group-material .highlight {
		position: absolute;
		height: 60%;
		width: 100px;
		top: 25%;
		left: 0;
		pointer-events: none;
		opacity: 0.5;
	}


	/* active state */

	.input-group-material input:focus ~ .highlight {
		-webkit-animation: inputHighlighter 0.3s ease;
		-moz-animation: inputHighlighter 0.3s ease;
		animation: inputHighlighter 0.3s ease;
	}


	/* ANIMATIONS ================ */

	@-webkit-keyframes inputHighlighter {
		from {
			background: #5264AE;
		}
		to {
			width: 0;
			background: transparent;
		}
	}

	@-moz-keyframes inputHighlighter {
		from {
			background: #5264AE;
		}
		to {
			width: 0;
			background: transparent;
		}
	}

	@keyframes inputHighlighter {
		from {
			background: #5264AE;
		}
		to {
			width: 0;
			background: transparent;
		}
	}
</style>
@endsection

@section('content')


{{ Form::open(array('url' => 'engineManager/login', 'autocomplete' => 'off')) }}
<div class="loginBox">
	<h1>Login</h1>

	<!-- if there are login errors, show them here -->
	<p>
		{{ $errors->first('username') }}
		{{ $errors->first('password') }}
	</p>

	<div class="input-group-material">      
		{{ Form::text('username', Input::old('username'), ['required' => true]) }}
		<span class="highlight"></span>
		<span class="bar"></span>
		<label>Username</label>
	</div>

	<div class="input-group-material">      
		{{ Form::password('password', ['required' => 'required'])}}
		<span class="highlight"></span>
		<span class="bar"></span>
		<label>Password</label>
	</div>

	<p>{{ Form::submit('LOGIN', ['class' => 'btn']) }}</p>	
</div>

{{ Form::close() }}
@endsection

@section('footerScript')


@endsection