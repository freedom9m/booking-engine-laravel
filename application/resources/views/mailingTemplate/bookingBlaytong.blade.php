<div style="font-family: sans-serif;">
<?php
$currency = \Config::get('params.currencyCode');
$i=0;
$totalBooking = count($data);
$totalPrice = 0;
$printSettings = Config::get('printSettings');
$path = Storage::disk('pms')->getDriver()->getAdapter()->applyPathPrefix(Config::get('pathName').DIRECTORY_SEPARATOR.$printSettings->printlogo);
$hotelLogo = null;
$leftContent = null;
$rightContent = null;
$bottomContent = null;
if(file_exists($path)){
	//$imageData = file_get_contents($path);
	//$type = pathinfo($printSettings->printlogo, PATHINFO_EXTENSION);
	//$hotelLogo = 'data:image/' . $type . ';base64,' . base64_encode($imageData);
	$hotelLogo = Config::get('app.url').DIRECTORY_SEPARATOR.'pmsStorage'.DIRECTORY_SEPARATOR.Config::get('pathName').DIRECTORY_SEPARATOR.$printSettings->printlogo;
}
$payAlready = false;
foreach($data as $array){
	//print_r($array);
	extract($array);
	if($i==0){
		$contactName = $guest->prefix.' '.$guest->firstname.' '.$guest->lastname;
		$contactEmail = $guest->email;
		/*echo '<div id="contactBox">';
		echo '<div style="margin-top: 5px"><h3>'.$printSettings->hotelname.'</h3></div>';
		echo '<div style="margin-top: 5px"><strong>Dear. '.$contactName.'</strong></div>';
		echo '<div style="margin-top: 5px"><strong>Reference ID :</strong> '.$booking->agentid.'</div>';
		echo '<div style="margin-top: 5px"><strong>TOTAL Rooms. :</strong> '.$totalBooking.'</div>';
		echo '<div style="margin-top: 5px"><strong>Contact Name :</strong> '.$contactName.'</div>';
		echo '</div>';*/

		echo '<table cellpadding="0" cellspacing="0" style="width: 100%; font-size: 12px;">
		<tr>
			<td style="width: 30%;"><img src="'.$hotelLogo.'" alt="'.$printSettings->hotelname.'" style="max-width: 180; max-height: 100px;"></td>
			<td style="width: 70%; text-align: right;">
				<div style="font-size: 20px;"><strong>'.$printSettings->hotelname.'</strong></div>
				<div>'.$printSettings->address.'</div>
				<div>Tel : '.$printSettings->tel.' | Fax: '.$printSettings->fax.' | Email : '.$printSettings->gm_email.'</div>
			</td>
		</tr>
		</table>';

		echo '<div style="padding: 10px; border: 1px solid #ccc; text-align: center; font-weight: bold; font-size: 20px;"><span>Voucher No.</span> <span style="color: #b10b0b;">'.$booking->agentid.'</span></div>';

		// GUEST DETAILS
		$leftContent = '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #6d6d6d; color: #fff; font-weight: bold; font-size: 15px; margin-top: 5px;">GUEST DETAILS</div>';
		$leftContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #f7f7f7;">
				<table cellpadding="0" cellspacing="0" style="width: 100%; font-size: 14px !important;">
					<tr>
						<td style="width: 40%;"><strong>Guest Name :</strong></td>
						<td style="width: 60%;">'.$contactName.'</td>
					</tr>
					<tr>
						<td style="width: 40%;"><strong>Email :</strong></td>
						<td style="width: 60%;">'.$contactEmail.'</td>
					</tr>
					<tr>
						<td style="width: 40%;"><strong>Phone Number :</strong></td>
						<td style="width: 60%;">'.$booking->phone.'</td>
					</tr>
					<tr>
						<td style="width: 40%;"><strong>Country :</strong></td>
						<td style="width: 60%;">'.$guest->countryofpassport.'</td>
					</tr>
					<!-- <tr>
						<td style="width: 40%;"><strong>Nationality :</strong></td>
						<td style="width: 60%;"></td>
					</tr> -->
				</table>
			</div>';


		// RESERVATION DETAILS
		$leftContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #6d6d6d; color: #fff; font-weight: bold; font-size: 15px; margin-top: 5px;">RESERVATION DETAILS</div>';
		$leftContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #f7f7f7;">
				<table cellpadding="0" cellspacing="0" style="width: 100%; font-size: 14px !important;">
					<tr>
						<td style="width: 40%;"><strong>Reference ID# :</strong></td>
						<td style="width: 60%;">'.$booking->agentid.'</td>
					</tr>
					<tr>
						<td><strong>Period of Stay : </strong></td>
						<td>'.date('d M Y', strtotime($booking->checkindate)).' to '.date('d M Y', strtotime($booking->checkoutdate)).'('.$booking->noofnight.' night'.($booking->noofnight > 1 ? 's' : '').')</td>
					</tr>
					<tr>
						<td><strong>Number of Rooms :</strong></td>
						<td>'.$totalBooking.'</td>
					</tr>
				</table>
			</div>';
	}
	
	$noOfNight = \App\Components\Functions::dateDiff($booking->checkindate, $booking->checkoutdate);
	$totalAdditionalService = 0;
	if(count($additional) > 0){
		foreach($additional as $ads){
			$totalAdditionalService += $ads->total;
		}
	}

	$totalExtra = $booking->totalextra;
	$totalExtraAdult = $totalExtra;
	$totalExtraChild = 0;

	$extraPerNight = $totalExtra / $noOfNight;

	if(count($bookingChildren) > 0){
		foreach($bookingChildren as $childData){
			$totalExtraAdult -= $childData->extraprice;
			$totalExtraChild += $childData->extraprice;
		}
	}

	$totalOfBooking = $booking->total+$totalAdditionalService;
	$taxAndServiceCharge = $totalOfBooking-($totalOfBooking/Config::get('hotelModel')->vat);

	$totalPrice += $totalOfBooking;

	//print_r($booking);
	$summary = [
		'bookingid' 				=> $booking->bookingid,
		'no'						=> $i+1,
		'guestName'					=> $guest->guest_prefix.' '.$guest->guest_firstname.' '.$guest->guest_lastname,
		'checkInDate'				=> $booking->checkindate,
		'checkOutDate'				=> $booking->checkoutdate, //date('Y-m-d', strtotime('-1 days', strtotime($booking->checkoutdate))),
		'noOfNight'					=> $noOfNight,
		'roomTypeName'				=> $roomtype->roomtypename,
		'promotion'					=> $array['promotion'],
		'totalPrice'				=> $booking->totalprice - $booking->totalextra,
		'totalExtra'				=> $booking->totalextra,
		'totalExtraAdult'			=> $totalExtraAdult,
		'totalExtraChild'			=> $totalExtraChild,
		'totalAdditionalService'	=> $totalAdditionalService,
		'taxAndServiceCharge'		=> $taxAndServiceCharge,
		'totalOfBooking'			=> $totalOfBooking,
		'additional'				=> $additional,
		'specialRequest'			=> $guest->customernote,
		'currency'					=> $currency,
		'noOfAdults'				=> $booking->adult,
		'noOfChilds'				=> $booking->child,
		'extraBed'					=> (($booking->extrabed > 0 || $booking->extrachild > 0)? true : false)
	];

	$i++;
	$nextRoomType = null;
	$nextPromotion = null;
	if(isset($data[$i]['roomtype']->roomtypeid)){
		$nextRoomType = $data[$i]['roomtype']->roomtypeid;
	}
	if(isset($data[$i]['roomtype']->promotion)){
		$nextPromotion = $promotion;
	}

	if($nextRoomType != $roomtype->roomtypeid || $nextPromotion != $promotion){
		$summary['policy'] = $policy;
	}

	if($booking->bookingstatus == 5){ // confirm & paid
		$payAlready = true;
	}

	// ROOM DETAIL
	$leftContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #a9a9a9; color: #fff; font-size: 15px;">Room No.'.$i.'</div>';

	// ROOM PAYMENT DETAIL
	if($i == 1){
		$rightContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #6d6d6d; color: #fff; font-weight: bold; font-size: 15px; margin-top: 5px;">PAYMENT DETAILS</div>';
	}
	$roomPaymentDetail = null;
	$mealPlan = false;
	foreach($bookingPrice as $method){
		$priceDay = $method->totalamount;
		if($method->totalamount > 0){
			$priceDay -= $method->extratotal;
		}
		if(strtolower($method->abf) == 'yes'){
			$mealPlan = true;
		}else{

		}
		$roomPaymentDetail .= '<tr>
			<td style="padding: 0 5px; ">'.date('d M Y', strtotime($method->pricedate)).'</td>
			<td style="text-align: right; padding: 0 5px; ">'.$currency.' '.number_format($priceDay, 2).'</td>
		</tr>';
	}
	if($totalExtraAdult > 0){
		$roomPaymentDetail .= '<tr>
			<td style="padding: 0 5px; ">Extra (Adult)</td>
			<td style="text-align: right; padding: 0 5px; ">'.$currency.' '.number_format($totalExtraAdult, 2).'</td>
		</tr>';
	}
	if($totalExtraChild > 0){
		$roomPaymentDetail .= '<tr>
			<td style="padding: 0 5px; ">Extra (Child)</td>
			<td style="text-align: right; padding: 0 5px; ">'.$currency.' '.number_format($totalExtraChild, 2).'</td>
		</tr>';
	}
	if(count($additional) > 0){
		$roomPaymentDetail .= '<tr><td style="padding: 5px; -webkit-print-color-adjust: exact; background: #e0dede; font-weight: bold;" colspan="2">Additional Service</td></tr>';
		foreach($additional as $method){
			$additionalData = \App\Components\BookingComponents::getAddtionalServicesData($method->additionalid);
			$roomPaymentDetail .= '<tr>
				<td style="padding: 0 5px; ">'.$additionalData->additional_name.' &times; '.$method->unit.'</td>
				<td style="text-align: right; padding: 0 5px; ">'.$currency.' '.number_format($method->total, 2).'</td>
			</tr>';
		}
	}
	$rightContent .= '<div style="-webkit-print-color-adjust: exact; background: #f7f7f7;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; font-size: 14px !important;">
				<tr style="-webkit-print-color-adjust: exact; background: #a9a9a9; color: #fff;">
					<td style="width: 40%; padding: 5px; ">Room No.'.$i.'</td>
					<td style="width: 60%; padding: 5px; text-align: right;">'.$currency.' '.number_format($summary['totalOfBooking'], 2).'</td>
				</tr>
				'.$roomPaymentDetail.'
			</table>
		</div>';
	
	
	$bottomContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #a9a9a9; color: #fff; font-size: 15px;">Room No.'.$i.'</div>';
	$bottomContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #f7f7f7; font-size: 14px !important;">
		<h4>Policy : '.$policy->policyname.'</h4>
		<div>'.$policy->description.'</div>

		</div>';

	$leftContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #f7f7f7;">
			<table cellpadding="0" cellspacing="0" style="width: 100%; font-size: 14px !important;">
				<tr>
					<td style="width: 40%;"><strong>Booking ID :</strong></td>
					<td style="width: 60%;">'.$summary['bookingid'].'</td>
				</tr>
				<tr>
					<td><strong>Room Type :</strong></td>
					<td>'.$summary['roomTypeName'].'</td>
				</tr>
				'.($summary['promotion'] != null ? '
				<tr>
					<td><strong>Promotion :</strong></td>
					<td>'.$summary['promotion']['name'].'</td>
				</tr>' : '').
				'<tr>
					<td><strong>No. of Adults :</strong></td>
					<td>'.$summary['noOfAdults'].'</td>
				</tr>
				<tr>
					<td><strong>No. of Children :</strong></td>
					<td>'.$summary['noOfChilds'].'</td>
				</tr>
				<tr>
					<td><strong>Extra Bed :</strong></td>
					<td>'.($summary['extraBed'] == true ? 'Yes' : 'No').'</td>
				</tr>
				<tr>
					<td><strong>Breakfast</strong></td>
					<td>'.($mealPlan == true ? 'Yes' : 'No').'</td>
				</tr>
			</table>
		</div>';


	// echo \View::make('mailingTemplate._summaryBlaytong', $summary)->render();

}

if(trim($summary['specialRequest']) != ''){
	$leftContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #6d6d6d; color: #fff; font-weight: bold; font-size: 15px;">SPECIAL REQUEST</div>';
	$leftContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #f7f7f7; font-size: 14px !important;">'.$summary['specialRequest'].'</div>';
}

$reservationPrice = $totalPrice / Config::get('hotelModel')->vat;
$taxAndServiceCharge = $totalPrice - $reservationPrice;
$totalDeposit = 0;
$totalBalance = $totalPrice;
if($payAlready == true){
	$totalDeposit = $totalPrice;
	$totalBalance = 0;
}

$rightContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #a9a9a9;"></div>';
$rightContent .= '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #f7f7f7">
	<table style="width: 100%; font-size: 14px !important;">
		<tr>
			<td style="width: 40%; text-align: right;"><strong>Reservation :</strong></td>
			<td style="width: 60%; text-align: right;">'.$currency.' '.number_format($reservationPrice, 2).'</td>
		</tr>
		<tr>
			<td style="text-align: right;"><strong>Taxes & service charge :</strong></td>
			<td style="text-align: right;">'.$currency.' '.number_format($taxAndServiceCharge, 2).'</td>
		</tr>
		<tr>
			<td style="text-align: right;"><strong>Grand Total :</strong></td>
			<td style="text-align: right;">'.$currency.' '.number_format($totalPrice, 2).'</td>
		</tr>
		<tr>
			<td style="text-align: right;"><strong>Deposit :</strong></td>
			<td style="text-align: right;">'.$currency.' '.number_format($totalDeposit, 2).'</td>
		</tr>
		<tr>
			<td style="text-align: right;"><strong>Balance :</strong></td>
			<td style="text-align: right;">'.$currency.' '.number_format($totalBalance, 2).'</td>
		</tr>
		<tr>
			<td style="text-align: right;" colspan="2"><i>** Inclusive of service charge and Government VAT</i></td>
		</tr>
	</table>
</div>';

$rightContent .= '<div style="font-size: 14px;">Deposit ('.$currency.' '.$totalDeposit.') has been charged to your credit card by '.Config::get('printSettings')->hotelname.' The remaining balance of '.$currency.' '.number_format($totalBalance, 2).' will be charged by '.Config::get('printSettings')->hotelname.'</div>';
echo '<table style="width: 100%;" cellspacing="0">
	<tr>
		<td style="width: 49%;" valign="top">'.$leftContent.'</td>
		<td></td>
		<td style="width: 49%;" valign="top">'.$rightContent.'</td>
	</tr>
</table>';


echo '<div style="padding: 5px; -webkit-print-color-adjust: exact; background: #6d6d6d; color: #fff; font-weight: bold; font-size: 15px; margin-top: 5px;">TERM & CONDITION</div>';
echo $bottomContent;

?>
<!-- <div align="center"><h3>TOTAL PRICE <?=number_format($totalPrice, 2);?> <?=$currency?></h3></div>
<div style="margin-top: 5px"><h3>{{$printSettings->hotelname}}</h3></div>
<div style="margin-top: 5px">Address : {{$printSettings->address}}</div>
<div style="margin-top: 5px">Email : {{$printSettings->email}}</div>
<div style="margin-top: 5px">Tel : {{$printSettings->tel}}</div>
<div style="margin-top: 5px">Website : {{$printSettings->website}}</div>
 -->
 </div>