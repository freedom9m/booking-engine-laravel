<div style="margin-top: 15px; border: 1px solid #f7f7f7;">
	<table style="background: #BF8C8C; width: 100%;">
		<tr>
			<td><strong>Booking ID : <?=$bookingid?></strong></td>
			<td align="right">Room No. <?=$no?></td>
		</tr>
	</table>
	<div style="clear: both; padding: 5px">
		<div style="margin-top: 5px;">
			<strong>Guest Name</strong> <?=$guestName?>
		</div>
		<div style="margin-top: 5px;">
			<strong>Room Type</strong> <?=$roomTypeName?>
		</div>
		<table border="1" cellpadding="0" cellspacing="0" style="min-width: 500px;border: 1px solid #000; border-spacing: 0; border-collapse: collapse;">
			<tr>
				<td><strong>NO. of Night</strong></td>
				<td><?=$noOfNight?></td>
				<td><strong>Room Charge</strong></td>
				<td><?=number_format($totalPrice, 2)?> <?=$currency?></td>
			</tr>
			<tr>
				<td><strong>Date of Arrival</strong></td>
				<td><?=$checkInDate?></td>
				<td><strong>Extra bed</strong></td>
				<td><?=number_format($totalExtra, 2)?> <?=$currency?></td>
			</tr>
			<tr>
				<td><strong>Date of Departure</strong></td>
				<td><?=$checkOutDate?></td>
				<td><strong>Tax & Service Charge</strong></td>
				<td><?=number_format($taxAndServiceCharge, 2)?> <?=$currency?></td>
			</tr>
			<tr>
				<td><strong>Total of booking</strong></td>
				<td><?=number_format($totalOfBooking, 2)?> <?=$currency?></td>
				<td></td>
				<td></td>
			</tr>
			<?php if($promotion != null){ ?>
			<tr>
				<td><strong>Promotion</strong></td>
				<td colspan="3"><?php echo $promotion['name'] ?></td>
			</tr>
			<?php 
			}
			if($totalAdditionalService > 0):
			?>
			<tr>
				<td colspan="3"><strong>Additional Services</strong></td>
				<td><?=number_format($totalAdditionalService, 2)?> <?=$currency?></td>
			</tr>
			<?php 
			endif;
			?>
			<tr>
				<td><strong>Special Request : </strong></td>
				<td colspan="3"><?=$specialRequest?></td>
			</tr>
		</table>
		<?php
		 if($totalAdditionalService>0): ?>
		<hr>
		<strong>Additional Lists.</strong>
		<table class="table table-condensed table-striped table-bordered">
			<thead>
				<tr>
					<td>Additional Service</td>
					<td>Price</td>
				</tr>
			</thead>
			<tbody>
			<?php 
			foreach($additional as $method)
			{
				$additionalData = \App\Components\BookingComponents::getAddtionalServicesData($method->additionalid);
				echo '<tr>
					<td>'.$additionalData->additional_name.'</td>
					<td>'.number_format($method->total, 2).' '.$currency.'</td>
				</tr>';
			}
			?>	
			</tbody>
		</table>
		<?php endif; ?>
	</div>
</div>
<?php if(isset($policy)): ?>
<div style="margin-top: 15px; padding: 5px">
	<h4>Policy : <?php echo $policy->policyname ?></h4>
	<div><?php echo $policy->description ?></div>
</div>
<?php endif; ?>