<?php
$currency = \Config::get('params.currencyCode');
$i=0;
$totalBooking = count($data);
$totalPrice = 0;
foreach($data as $array){
	//print_r($array);
	extract($array);
	if($i==0){
		$contactName = $guest->prefix.' '.$guest->firstname.' '.$guest->lastname;
		$contactEmail = $guest->email;
		echo '<div id="contactBox">';
		$printSettings = \Config::get('printSettings');
		echo '<div style="margin-top: 5px"><h3>'.$printSettings->hotelname.'</h3></div>';
		echo '<div style="margin-top: 5px"><strong>Dear. '.$contactName.'</strong></div>';
		echo '<div style="margin-top: 5px"><strong>Reference ID :</strong> '.$booking->agentid.'</div>';
		echo '<div style="margin-top: 5px"><strong>TOTAL Rooms. :</strong> '.$totalBooking.'</div>';
		echo '<div style="margin-top: 5px"><strong>Contact Name :</strong> '.$contactName.'</div>';
		echo '</div>';
	}
	
	$totalAdditionalService = 0;
	foreach($additional as $ads){
		$totalAdditionalService += $ads->total;
	}

	$totalOfBooking = $booking->total+$totalAdditionalService;
	$taxAndServiceCharge = $totalOfBooking-($totalOfBooking/Config::get('hotelModel')->vat);

	$totalPrice += $totalOfBooking;

	//print_r($booking);
	$summary = [
		'bookingid' 				=> $booking->bookingid,
		'no'						=> $i+1,
		'guestName'					=> $guest->guest_prefix.' '.$guest->guest_firstname.' '.$guest->guest_lastname,
		'checkInDate'				=> $booking->checkindate,
		'checkOutDate'				=> $booking->checkoutdate, //date('Y-m-d', strtotime('-1 days', strtotime($booking->checkoutdate))),
		'noOfNight'					=> \App\Components\Functions::dateDiff($booking->checkindate, $booking->checkoutdate),
		'roomTypeName'				=> $roomtype->roomtypename,
		'promotion'					=> $array['promotion'],
		'totalPrice'				=> $booking->totalprice,
		'totalExtra'				=> $booking->totalextra,
		'totalAdditionalService'	=> $totalAdditionalService,
		'taxAndServiceCharge'		=> $taxAndServiceCharge,
		'totalOfBooking'			=> $totalOfBooking,
		'additional'				=> $additional,
		'specialRequest'			=> $guest->customernote,
		'currency'					=> $currency,
	];

	$i++;
	$nextRoomType = null;
	$nextPromotion = null;
	if(isset($data[$i]['roomtype']->roomtypeid)){
		$nextRoomType = $data[$i]['roomtype']->roomtypeid;
	}
	if(isset($data[$i]['roomtype']->promotion)){
		$nextPromotion = $promotion;
	}

	if($nextRoomType != $roomtype->roomtypeid || $nextPromotion != $promotion){
		$summary['policy'] = $policy;
	}

	echo \View::make('mailingTemplate._summary', $summary)->render();

}

?>
<div align="center"><h3>TOTAL PRICE <?=number_format($totalPrice, 2);?> <?=$currency?></h3></div>
<div style="margin-top: 5px"><h3>{{$printSettings->hotelname}}</h3></div>
<div style="margin-top: 5px">Address : {{$printSettings->address}}</div>
<div style="margin-top: 5px">Email : {{$printSettings->email}}</div>
<div style="margin-top: 5px">Tel : {{$printSettings->tel}}</div>
<div style="margin-top: 5px">Website : {{$printSettings->website}}</div>
