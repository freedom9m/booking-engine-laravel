
@extends('layouts.web')

@section('title', 'Hotel Lists')

@section('headerScript')
	
@endsection

@section('content')
<div class="row">
	<div class="col-md-3">
		<?php echo View::make('searchBox', ['keyword' => $keyword])->render() ?>
	</div>
	<div class="col-md-9">
		Select some hotel you want to book.
		<div>We found <span class="totalHotels">{{ $hotelList->total() }} hotels</span></div>
			@foreach ($hotelList as $method)
			<div class="media propertyList">
				<div class="media-content">
					<div class="row">
						<div class="col-xs-5 col-sm-4">
							<div class="hotelPicture">
								<a href="{{ url('hotel/'.$method->slug) }}" class="hotelName">
								@if ($method->logo != null && File::exists('hotelImages/'.$method->id.'/'.$method->logo))
									<img src="{{ asset('hotelImages/'.$method->id.'/'.$method->logo)}}", style="width: 100%; max-height: 150px;">
								@else
									<img src="{{ asset('assets/images/hotelPicture.jpg') }}" style="width: 100%; max-height: 150px;">
								@endif
							</div>
						</div>
						<div class="col-xs-7 col-sm-6">
							<a href="{{ url('hotel/'.$method->slug) }}" class="hotelName">{{ $method->hotelname }}</a> 
							<span class="starRate">
								<!-- @for($i = 0; $i<$method->star; $i++)★@endfor -->
							</span>
							<div class="hotelFacilities">
								
							</div>
							<div class="hotelDetails">
								{{ $method->description }}
							</div>
						</div>
						<div class="col-sm-2">
						@if($method->debugMode == 1)
							<div class="temporarilyDisabled">Temporarily disabled</div>
						@else
							<div>{{ link_to_route('reservation', 'Book Now', ['slug' => $method->slug], ['class' => 'btn btn-default']) }}</div>
						@endif

						</div>
					</div>
				</div>
			</div>
			@endforeach
		{{ $hotelList->render() }}
		
	</div>
</div>
@endsection

@section('footerScript')


@endsection