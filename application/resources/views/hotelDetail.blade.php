<?php 
$image = null;
if(File::exists('hotelImages/'.$model->id.'/logo.jpg')){
	$image = asset('hotelImages/'.$model->id.'/logo.jpg');
}
elseif (File::exists('hotelImages/'.$model->id.'/logo.jpeg')){
	$image = asset('hotelImages/'.$model->id.'/logo.jpeg');
}
elseif (File::exists('hotelImages/'.$model->id.'/logo.png')){
	$image = asset('hotelImages/'.$model->id.'/logo.png');
}

?>
@extends('layouts.web')

@section('title', $model->hotelname)

@section('headerScript')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/packages/jqueryUi/jquery-ui.min.css')}}">
@endsection

@section('content')
<div>
	
</div>
<ol class="breadcrumb">
  <li><a href="{{ url('/') }}">Hotels</a></li>
  <li class="active"><a href="#">{{ $model->hotelname }}</a></li>
</ol>
<div class="hotelDetailBlock">
	<div class="contentBox">
		<h1>{{$model->hotelname}}</h1>

		<p class="clearfix"> 
			@if($image!=null) 
			<img src="{{ asset($image)}}" class="hotelDetailImage"> 
			@endif 
			{{$model->description}}
		</p>
	</div>
	<div class="reservationBox">
		@if($model->debugMode == 1)
			<div class="temporarilyDisabled">Temporarily disabled</div>
		@else

			{!! Form::open(array('url' => 'reservation/'.$model->slug, 'method'=>'get', 'id' => 'reservationForm')) !!}
				<div class="calendarBox">
					<div class="dateBox datepickerBox" data-element="#bookingform-checkindate">
						<div class="boxTitle">Arrival</div>
						<span class="daydate"></span>
						<span class="month"></span>
						<div align="left">
							<input type="text" value="<?=date('Y-m-d')?>" name="checkInDate" name="checkInDate" id="bookingform-checkindate">
						</div>
					</div>
					<div class="dateBox datepickerBox" data-element="#bookingform-checkoutdate">
						<div class="boxTitle">Departure</div>
						<span class="daydate"></span>
						<span class="month"></span>
						<div align="left">
							<input type="text" value="<?=date('Y-m-d', strtotime('+1 days', strtotime(date('Y-m-d'))))?>" name="checkOutDate" name="checkOutDate" id="bookingform-checkoutdate">
						</div>
					</div>
				</div>

				<div class="btnAndCode">
					<div id="codeText">
						<div>{{trans('messages.pcQuestion1')}}</div>
						<a href="javascript:void(0);">{{trans('messages.pcQuestion2')}}</a>
					</div>
					<div id="codeBox">
						<div class="input-group" id="checkoutdate">
							<div class="input-group-addon">Promotion Code</div>
							<input type="text" value="" name="promotionCode" id="bookingform-promotioncode" class="form-control">
							<div class="input-group-addon" id="closeCodeBtn">
								<span class="glyphicon glyphicon-remove"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="btnbookquick" id="findAvailableBtn">
					<div class="check1">CHECK AVAILABILITY</div>
					<div class="check2">Best price guaranteed</div>
				</div>

				<input type="hidden" id="noOfNight" value="1">
			{!! Form::close() !!}
		@endif
	</div>
</div>

@endsection

@section('footerScript')

    <script src="{{asset('assets/packages/jqueryUi/jquery-ui.min.js')}}"></script>
	<script src="{{asset('assets/packages/moment/moment.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
	<script>

	    jQuery.fn.setElementDate = function (date){
	        var element = $(this).parent().parent();
	        if(typeof date == 'undefined'){
	            date = $(this).val();
	        }
	        element.find('.daydate').html(moment(date).format('DD'));
	        element.find('.month').html(moment(date).format('MMM YYYY'));
	    }
		$(document).ready(function() {
			$('body').on('click', '#findAvailableBtn', function(){
				$('#reservationForm').submit();
			});

			$('body').on('click', '#codeText a', function(){
				$(this).parent().fadeOut(function(){
					$('#codeBox').fadeIn();	
				});
			});

			$('body').on('click', '#closeCodeBtn', function(){
				$('#codeBox').fadeOut(function(){
					if($('#bookingform-promotioncode').val()!=''){
						$('#bookingform-promotioncode').val('');
						$('#findAvailableBtn').trigger('click');
					}
					$('#codeText').fadeIn();
				});
			});

			$('#bookingform-checkindate').setElementDate();

			$('#bookingform-checkindate').datepicker({
				dateFormat: 'yy-mm-dd', 
				minDate: 0,
				onSelect: function(dateText) {
					$(this).val(dateText);
					var date = moment(dateText);
					$(this).setElementDate(date);
					var minDate = moment(dateText).add(1, 'days').format('YYYY-MM-DD');
					var endDate = moment(dateText).add(parseInt($('#noOfNight').val()), 'days').format('YYYY-MM-DD');
					$('#bookingform-checkoutdate').datepicker("setDate", endDate);
					$('#bookingform-checkoutdate').datepicker( "option", "minDate", minDate );
					$('#bookingform-checkoutdate').setElementDate(endDate);
				}
			});

			$('#bookingform-checkoutdate').setElementDate();

			$('#bookingform-checkoutdate').datepicker({
				dateFormat: 'yy-mm-dd', 
				minDate: 1,
				onSelect: function(dateText) {
					$(this).val(dateText);
					var date = moment(dateText);
					$(this).setElementDate(date);
				}
			});

			$('#avaibilityStartDate').datepicker({
				dateFormat: 'yy-mm-dd', 
				minDate: 0,
				onSelect: function(dateText) {
				}
			});


			$('body').on('click', '.avaibilityNav', function(){
				var action = $(this).data('action');
				var startDate = $('#avaibilityTitle').data('startdate');
				if(action=='backward')
				{
					startDate = moment(startDate).add(-10, 'days').format('YYYY-MM-DD');
				}else{
					startDate = moment(startDate).add(10, 'days').format('YYYY-MM-DD');
				}
				if(startDate<moment().format('YYYY-MM-DD')){
					startDate = moment().format('YYYY-MM-DD');
				}
				$('#avaibilityStartDate').datepicker("setDate", moment(startDate).format('YYYY-MM-DD'));
			});

			$('body').on('click', '.datepickerBox', function(){
				var element = $(this).data('element');
				$(element).show().focus().hide();
			});
		});
	</script>

@endsection