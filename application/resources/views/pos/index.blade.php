@extends('layouts.pos')

@section('headerScript')

@endsection

@section('content')
<div class="container-fluid posTopMenu">
	<div id="menu">
		<div id="searchBox">
			<div class="row">
				<div class="col-md-6">
					<input type="number" class="form-control" placeholder="bill id">
				</div>
			</div>
		</div>
		<div id="menuBox">
			<button class="menuButton" type="button">Menus</button>
			<button class="menuButton active" type="button">Quick Menu</button>
		</div>
	</div>
</div>
<div class="container-fluid posBox">

	<div id="orderPanel">
		<div class="row">
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
	<div id="rightPanel">
		<div id="menuPanel">
			
		</div>
		<div id="controlPanel">
			
		</div>
	</div>
</div>

@endsection

@section('footerScript')

@endsection