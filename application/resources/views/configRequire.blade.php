@extends('layouts.web')

@section('title', 'Hotel Not Found!')

@section('headerScript')
<style>
    .title {
        text-align: center;
        font-size: 72px;
        margin-bottom: 40px;
    }
</style>    
@endsection

@section('content')
    <div class="title">Hotel Configuration Failer!</div>
@endsection

@section('footerScript')

@endsection
