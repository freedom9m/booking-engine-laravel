@extends('layouts.web')

@section('title', 'Database Error')

@section('headerScript')
<style>
    .title {
        text-align: center;
        font-size: 72px;
        margin-bottom: 40px;
    }
</style>    
@endsection

@section('content')
    <div class="title">HOTEL DATABASE ERROR!</div>
@endsection

@section('footerScript')

@endsection