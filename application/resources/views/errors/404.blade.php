@extends('layouts.web')

@section('title', 'Page Not Found')

@section('headerScript')
<style>
    .title {
        text-align: center;
        font-size: 72px;
        margin-bottom: 40px;
    }
</style>	
@endsection

@section('content')
	<div class="title">Page Not Found</div>
@endsection

@section('footerScript')

@endsection