<?php 
use \App\Components\Functions;
$currency = \Config::get('params.currencyCode');;
?>
@extends('layouts.booking')

@section('title', 'Confirm')

@section('headerScript')

@endsection

@section('content')
<div class="modalBox" id="modalBox">
	<div class="modalBoxContentArea">
		<div class="closeModalBox">&times;</div>
		<div class="row">
			<div id="modalBoxContent"></div>
		</div>
	</div>
</div>
<?php 
$bankUrl = 'reservation/'.\Config::get('slug').'/step3';
$formName = 'bookingForm';

if($bankApi->bankUrl != null || $bankApi->loadAPI == true)
{
	if($bankApi->loadAPI == true){
		$bankUrl = 'reservation/'.\Config::get('slug').'/bankProcess';
	}else{
		$bankUrl = $bankApi->bankUrl;
	}
	$formName = $bankApi->formName;
}

 ?>
<div class="container">
{!! Form::open(array('url' => $bankUrl, 'name'=>$formName, 'id' => 'bookingForm')) !!}


<div class="margin-bottom-15px">
<?php 
$i=0;
$totalBooking = count($data);
$totalPrice = 0;
foreach($data as $array){
	//print_r($array);
	extract($array);
	if($i==0){
		$contactName = $guest->prefix.' '.$guest->firstname.' '.$guest->lastname;
		$contactEmail = $guest->email;
		echo '<div id="contactBox">';
		echo '<div class="margin-top-5px"><strong>'.trans('messages.dear').'. '.$contactName.'</strong></div>';
		// echo '<div class="margin-top-5px"><strong>'.trans('messages.fillWarning').'</strong></div>';
		echo '<div class="margin-top-5px"><strong>'.trans('messages.referenceId').' :</strong> '.$booking->agentid.'</div>';
		echo '<div class="margin-top-5px"><strong>'.trans('messages.totalRooms').' :</strong> '.$totalBooking.'</div>';
		echo '<div class="margin-top-5px"><strong>'.trans('messages.contactname').' :</strong> '.$contactName.'</div>';
		echo '</div>';
	}

	$totalExtra = $booking->totalextra;
	$totalExtraAdult = $totalExtra;
	$totalExtraChild = 0;
	
	$totalAdditionalService = 0;
	if(count($additional) > 0){
		foreach($additional as $ads){
			$totalAdditionalService += $ads->total;
		}
	}

	if(count($bookingChildren) > 0){
		foreach($bookingChildren as $childData){
			$totalExtraAdult -= $childData->extraprice;
			$totalExtraChild += $childData->extraprice;
		}
	}



	$totalOfBooking = $booking->total+$totalAdditionalService;
	$taxAndServiceCharge = $totalOfBooking-($totalOfBooking/1.177);

	$totalPrice += $totalOfBooking;

	//print_r($booking);
	$summary = [
		'bookingid' 				=> $booking->bookingid,
		'no'						=> $i+1,
		'guestName'					=> $guest->guest_prefix.' '.$guest->guest_firstname.' '.$guest->guest_lastname,
		'checkInDate'				=> $booking->checkindate,
		'checkOutDate'				=> $booking->checkoutdate, //date('Y-m-d', strtotime('-1 days', strtotime($booking->checkoutdate))),
		'noOfNight'					=> Functions::dateDiff($booking->checkindate, $booking->checkoutdate),
		'roomTypeName'				=> $roomtype->roomtypename,
		'promotion'					=> $promotion,
		'totalPrice'				=> $booking->totalprice,
		'totalExtraAdult'			=> $totalExtraAdult,
		'totalExtraChild'			=> $totalExtraChild,
		'totalAdditionalService'	=> $totalAdditionalService,
		'taxAndServiceCharge'		=> $taxAndServiceCharge,
		'totalOfBooking'			=> $totalOfBooking,
		'additional'				=> $additional,
		'specialRequest'			=> $guest->customernote,
		'currency'					=> $currency,
	];

	$i++;
	$nextRoomType = null;
	$nextPromotion = null;
	if(isset($data[$i]['roomtype']->roomtypeid)){
		$nextRoomType = $data[$i]['roomtype']->roomtypeid;
	}
	if(isset($data[$i]['roomtype']->promotion)){
		$nextPromotion = $promotion;
	}

	if($nextRoomType != $roomtype->roomtypeid || $nextPromotion != $promotion){
		$summary['policy'] = $policy;
	}

	echo View::make('booking.step2.summary', $summary)->render();

}
if($bankApi->moneyField != null and $bankApi->refField != null){
	$bankApi->{$bankApi->moneyField} = $totalPrice;
	$bankApi->{$bankApi->refField} = $model['referenceId'];
}
?>

@foreach($bankApi->fields as $field)
	@if(!in_array($field, $bankApi->noShowFields))
		{{ Form::hidden($field, $bankApi->{$field}) }}
	@endif
@endforeach

{{ $bankApi->checkField() }}


@if(method_exists($bankApi, 'generateAutoFields'))
	{{ $bankApi->generateAutoFields($model) }}
@endif
</div>
<div align="center"><h3 id="priceText"><?=trans('messages.totalPrice')?> <?=number_format($totalPrice, 2);?> <?=$currency?></h3></div>
 	{{ Form::hidden('referenceId', $model['referenceId']) }}
	{{ Form::hidden('contactName', $contactName) }}
	{{ Form::hidden('contactEmail', $contactEmail) }}
	{{ Form::hidden('summaryPrice', $totalPrice) }}

<div align="center">
	<label for="policy" id="policycfm"><input type="checkbox" class="checkpolicy" id="policy"> <?=trans('messages.confirmContinueText')?></label>
</div>
<div align="center"><button class="btn btn-default" id="continueBtn" type="button"><?=trans('messages.continueBtn')?></button></div>
{!! Form::close() !!}
</div>
@endsection

@section('footerScript')
<script>
	$(document).ready(function() {
		$('body').on('click', '#continueBtn', function(){
		    if($('#policy').is(':checked')==true){
		        $('#bookingForm').submit();
		    }else{
		        alert('<?=trans('messages.confirmContinueText')?>');
		    }
		});
		$('body').on('click', '.policyButton', function(){
			var policyData = $(this).parent().find('.policyDetail').html();
			$('.modalBoxContentArea').addClass('smallBox');
			$('#modalBoxContent').html('<h1 class="modalTitle">Policy</h1>' + policyData);
			$('#modalBox').fadeIn();
			$('body').css('overflow', 'hidden');
		});
		$('body').on('click', '.modalBox', function(e){
			if(e.target == this){
				$('.closeModalBox').trigger('click');
			}
		});
		$('body').on('click', '.closeModalBox', function(){
			$(this).parent().parent().fadeOut();
			$('body').css('overflow', '');
		});
	});
</script>
@endsection
