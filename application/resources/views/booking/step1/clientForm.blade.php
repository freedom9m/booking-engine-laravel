<?php 
use \App\Components\Functions;
use \App\Components\BookingComponents;
$roomtypeInfo = $data['roomtypeInfo'];
$rateInfo = $data['rateInfo'];
$discountData = $data['discountData'];
$additionalServices = $data['additionalServices'];
$occupancyData = $data['occupancyData'];
unset($data['roomtypeInfo']);
unset($data['rateInfo']);
unset($data['discountData']);
unset($data['additionalServices']);
unset($data['occupancyData']);
$formArrayName = '['.$roomtypeInfo->roomtypeid.']['.$data['datatype'].']['.$data['proid'].']['.$index.']';
$currency = Config::get('params.currencyCode');

$extraBed=$occupancyData->minadult;
$maxPlusBed=$occupancyData->maxadult;
$minChildren=$occupancyData->minchildren;
$maxChildren=$occupancyData->maxchildren;
$maxInfantAge=$occupancyData->maxinfantage;
$maxChildrenAge=$occupancyData->maxchildrenage;
$minAdultAge=$maxChildrenAge+1;
$plusExtraInfant=$occupancyData->plusextrainfant;
$plusExtraChild=$occupancyData->plusextrachild;
$plusExtraAdult=$occupancyData->plusextraadult;



$discountType = array(1=>'Percentage', 2=>'Price Discount', 3=>'Free night');
$numNight = $rateInfo['numNight'];
$pricePerNight = $rateInfo['price'];
$priceExtra = $rateInfo['extraBedPirce'];
$priceExtraChild = $rateInfo['extraBedChildPrice'];
$priceAllNight = $pricePerNight*$numNight;
$priceFirstPromotion = $priceAllNight;
$newPrice = $priceAllNight;
$freeNight = null;

$bookingInfo = $widget['bookingInfo'];
$checkInDate = $bookingInfo['checkInDate'];
$checkOutDate = $bookingInfo['checkOutDate'];
if($discountData!=null){
	$crossPromotion = false;
	if(isset($discountData['crossPromotion']) && $discountData['crossPromotion']!=null){
		$crossPromotion = true;
		$totalNightToLastDayOfPromotion = Functions::dateDiff($checkInDate, $discountData['departure'])+1; //function diff will minus 1 day
		$priceFirstPromotion = $pricePerNight*$totalNightToLastDayOfPromotion;
		$priceLastPromotion = $priceAllNight-$priceFirstPromotion;
		$totalNightToLastDayOfBooking = Functions::dateDiff($discountData['departure'], $checkOutDate)-1; //minus check out day
	}

	$discountCalculated = BookingComponents::calculateDiscount([
		'discountData' => $discountData,
		'numNight' => $crossPromotion == true ? $totalNightToLastDayOfPromotion : $numNight,
		'priceAllNight' => $priceFirstPromotion,
		'pricePerNight' => $pricePerNight,
		'newPrice' => $newPrice,
		'freeNight' => $freeNight,
	]);


	extract($discountCalculated);
	/*if($discountData['discounttype']==3){ //free night
		$freeNight = Functions::freeNightCalculate([
			'totalNight'	=> $numNight,
			'stay'			=> $discountData['minstay'],
			'freeNight'		=> $discountData['discount'],
		]);

		$newPrice = $priceAllNight - ($pricePerNight*$freeNight);
		$discountPrice = $priceAllNight - $newPrice;
	}else if($discountData['discounttype']==2){ //by price
		$discountPrice = $discountData['discount'] * $numNight;
		$newPrice = $priceAllNight - $discountPrice;
	}else if($discountData['discounttype']==1){ //percentage
		// for discount extra
		//$priceExtra = $priceExtra-(($discountData['discount']/100)*$priceExtra);
		//$priceExtraChild = $priceExtraChild-(($discountData['discount']/100)*$priceExtraChild);

		$newPrice = $priceAllNight - ($discountData['discount']/100)*$priceAllNight;
		$discountPrice = $priceAllNight - $newPrice;
	}*/

	if($crossPromotion == true){

		/*if($discountData['crossPromotion']['discounttype']==3){ //free night
			$freeNight += Functions::freeNightCalculate([
				'totalNight'	=> $totalNightToLastDayOfBooking,
				'stay'			=> $discountData['crossPromotion']['minstay'],
				'freeNight'		=> $discountData['crossPromotion']['discount'],
			]);

			$newPriceLastPromotion = $priceLastPromotion - ($pricePerNight*$freeNight);
			$discountPriceLastPromotion = $priceLastPromotion - $newPrice;
		}else if($discountData['crossPromotion']['discounttype']==2){ //by price
			$discountPriceLastPromotion = $discountData['crossPromotion']['discount'] * $totalNightToLastDayOfBooking;
			$newPriceLastPromotion = $priceLastPromotion - $discountPriceLastPromotion;
		}else if($discountData['crossPromotion']['discounttype']==1){ //percentage
			$newPriceLastPromotion = $priceLastPromotion - ($discountData['crossPromotion']['discount']/100)*$priceLastPromotion;
			$discountPriceLastPromotion = $priceLastPromotion - $newPriceLastPromotion;
		}*/

		$discountCrossPromotionCalculated = BookingComponents::calculateDiscount([
			'discountData' => $discountData['crossPromotion'],
			'numNight' => $totalNightToLastDayOfBooking,
			'priceAllNight' => $priceLastPromotion,
			'pricePerNight' => $pricePerNight,
			'newPrice' => $newPrice,
			'freeNight' => null,
		]);

		$newPriceLastPromotion = $discountCrossPromotionCalculated['newPrice'];
		$discountPriceLastPromotion = $discountCrossPromotionCalculated['discountPrice'];
		
		$newPrice += $newPriceLastPromotion;
		$discountPrice += $discountPriceLastPromotion;
		if($discountCrossPromotionCalculated['freeNight']!=null){
			$freeNight += $discountCrossPromotionCalculated['freeNight'];
		}
	}

}
$priceWithOutTax = $newPrice / Config::get('hotelModel')->vat;
$priceTax  = $newPrice - $priceWithOutTax;
//echo Functions::dateDiff($rateInfo['checkInDate'], $rateInfo['checkOutDate']);

$totalPrice = $priceWithOutTax;
?>

<div class="clearfix booking-form-list">
	{{Form::hidden('roomkey['.$roomtypeInfo->roomtypeid.']['.$data['datatype'].']['.$data['proid'].'][]', $index)}}
	<button class="removeRoomBtn" type="button" style="display: none;">&times;</button>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6">
				<h4><?=trans('messages.roomNo')?>.# <span class="noOfRoomLabel"><?=$index+1?></span></h4>
				<div class="row">
					<div class="col-xs-12">
						<button type="button" class="copyAbove"><?=trans('messages.copyAboveBtn')?></button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
					<?php echo Form::label('guest[guest_prefix]'.$formArrayName, '* '.trans('messages.prefix'), ['class' => 'control-label']); ?>
    				<?php echo Form::select('guest[guest_prefix]'.$formArrayName, [
							'Mr.'	=> 'Mr.',
							'Miss.'	=> 'Miss.',
							'Mrs.'	=> 'Mrs.',
							'Dr.'	=> 'Dr.',
						], null, ['class' => 'form-control', 'data-input'=>'prefix', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Prefix']) ?>
					</div>
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-6">
								<?php echo Form::label('guest[guest_firstname]'.$formArrayName, '* '.trans('messages.firstname'), ['class' => 'control-label']); ?>
								<?php echo Form::text('guest[guest_firstname]'.$formArrayName, null, ['class'=>'form-control', 'data-input'=>'firstname', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Firstname']); ?>
							</div>
							<div class="col-sm-6">
								<?php echo Form::label('guest[guest_lastname]'.$formArrayName, '* '.trans('messages.lastname'), ['class' => 'control-label']); ?>
								<?php echo Form::text('guest[guest_lastname]'.$formArrayName, null, ['class'=>'form-control', 'data-input'=>'lastname', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Lastname']); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<?php 
						$listHolder = range(1, $extraBed+$maxPlusBed);
						$list = array_combine($listHolder, $listHolder);
						echo Form::label('guest[guest_noofguest]'.$formArrayName, '* '.trans('messages.noOfGuest'), ['class' => 'control-label']);
    					echo Form::select('guest[guest_noofguest]'.$formArrayName, $list, null, [
								'class' => 'form-control extraOpt',
								'data-extraname' => 'adults',
								'data-extrarate' => $priceExtra,
								'data-freenight' => $freeNight,
								'data-extrabed' => $extraBed,
								'data-maxplusbed' => $maxPlusBed,
								'data-validation' => '[NOTEMPTY,V!=0]',
								'data-validation-label' => 'No.Guest',
							]);
						?>
					</div>
					<div class="col-sm-4">
						<?php 
						$listHolder = range(0, $minChildren+$maxChildren);
						$list = array_combine($listHolder, $listHolder);
						echo Form::label('guest[guest_children]'.$formArrayName, trans('messages.children'), ['class' => 'control-label']);
    					echo Form::select('guest[guest_children]'.$formArrayName, $list, null, [
								'class' => 'form-control extraOpt',
								'data-extraname' => 'childs',
								'data-extrachildrate' => $priceExtraChild,
								'data-minchildren' => $minChildren,
								'data-maxchildren' => $maxChildren,
								'data-ageinputname' => 'guest[guest_ageofchildren]'.$formArrayName,
								'data-maxinfantage' => $maxInfantAge,
								'data-maxchildrenage' => $maxChildrenAge,
								'data-minadultage' => $minAdultAge,
								'data-plusextrainfant' => $plusExtraInfant,
								'data-plusextrachild' => $plusExtraChild,
								'data-plusextraadult' => $plusExtraAdult,
							]);
						?>
						<span class="smallTxt">age : 0 - {{$maxChildrenAge}}</span>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 ageOfChildrenBox">
						<div>Age of Children</div>
						<div id="ageOfChildrenForm"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<?php echo Form::label('guest[guest_country]'.$formArrayName, trans('messages.countryOfPassport'), ['class' => 'control-label']); ?>
	    				<?php echo Form::select('guest[guest_country]'.$formArrayName, $widget['countries'], null, ['class' => 'form-control']); ?>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div><h4><?=$roomtypeInfo->roomtypename?></h4></div>
				<div class="clearfix margin-top-5px">
					<div class="pull-left"><strong><?=$discountData==null?trans('messages.price'):trans('messages.priceBeforeDiscount')?></strong></div>
					<div class="pull-right"><?=number_format($rateInfo['price'], 2)?> x <?=$numNight?> = <span class="priceAllNight"><?=number_format($priceAllNight, 2);?></span> <?=$currency?></div>
				</div>
				<?php if($discountData!=null){ ?>
				<div class="clearfix margin-top-5px">
					<div class="pull-left"><strong><?=trans('messages.promotion')?></strong></div>
					<div class="pull-right">
						<span class="promotionInfo">
							<?php
							echo $discountData['name'];
							if($crossPromotion == true)
							{
								echo ' ('.date('d M, Y', strtotime($checkInDate)).' to '.date('d M, Y', strtotime($discountData['departure'])).')';
							}
							?>
						</span>
					</div>
				</div>
				<div class="clearfix margin-top-5px">
					<div class="pull-right">
						<?php 
						if($discountData['discounttype']==3){ //free night
							echo $freeNight . ' Free night.';
						}else if($discountData['discounttype']==2){ //by price
							echo number_format($discountData['discount'], 2) . ' ' . $currency;
						}else if($discountData['discounttype']==1){ //percentage
							echo $discountData['discount'] . (isset($discountType[$discountData['discounttype']]) ? ' '.$discountType[$discountData['discounttype']] : 'Unknow');
						}
						 ?>
					</div>
				</div>
				<?php if($crossPromotion == true){ ?>
				<div class="clearfix margin-top-5px">
					<div class="pull-left"><strong><?=trans('messages.promotion')?></strong></div>
					<div class="pull-right">
						<span class="promotionInfo">
							<?php
							echo $discountData['crossPromotion']['name'];
							if($crossPromotion == true)
							{
								if($totalNightToLastDayOfBooking>1){
									echo ' ('.date('d M, Y', strtotime('+1 days', strtotime($discountData['departure']))).' to '.date('d M, Y', strtotime('-1 days', strtotime($checkOutDate))).')';
								}else{
									echo ' ('.date('d M, Y', strtotime('+1 days', strtotime($discountData['departure']))).')';
								}
							}
							?>
						</span>
					</div>
				</div>
				<div class="clearfix margin-top-5px">
					<div class="pull-right">
						<?php 
						if($discountData['crossPromotion']['discounttype']==3){ //free night
							echo $freeNight . ' Free night.';
						}else if($discountData['crossPromotion']['discounttype']==2){ //by price
							echo number_format($discountData['crossPromotion']['discount'], 2) . ' ' . $currency;
						}else if($discountData['crossPromotion']['discounttype']==1){ //percentage
							echo $discountData['crossPromotion']['discount'] . (isset($discountType[$discountData['crossPromotion']['discounttype']]) ? ' '.$discountType[$discountData['crossPromotion']['discounttype']] : 'Unknow');
						}
						 ?>
					</div>
				</div>
				<?php } ?>
				<div class="clearfix margin-top-5px">
					<div class="pull-left"><strong><?=trans('messages.discount')?></strong></div>
					<div class="pull-right"><span class="priceDiscount"><?=number_format($discountPrice, 2)?></span> <?=$currency?></div>
				</div>
				<div class="clearfix  margin-top-5px padding-left-15px">
					<div class="pull-left"><strong><?=trans('messages.priceAfterDiscount')?></strong></div>
					<div class="pull-right"><span class="priceAfterDiscount"><?=number_format($newPrice, 2)?></span> <?=$currency?></div>
				</div>
				<?php } ?>
				<div class="clearfix margin-top-15px">
					<div class="pull-left"><strong><?=trans('messages.extraBed')?></strong></div>
					<div class="pull-right"><span class="priceExtrabed">0.00</span> <?=$currency?></div>
				</div>
				<?php if(count($additionalServices)>0){ ?>
				<div class="clearfix margin-top-15px">
					<div id="additionalServiceText"><strong><?=trans('messages.additionalServiceText')?></strong></div>
					<div class="additionalServiceList" align="center">not selected</div>
					<div class="clearfix">
						<div class="pull-right"><a href="javascript:void(0);" class="manageAdS"><?=trans('messages.manageAdditionalServiceBtn')?></a></div>
					</div>
				</div>
				<div class="additionalServiceBox">
					<h4><?=trans('messages.additionalServiceText')?></h4>
					<div class="additionalServicesList">
					<?php 
						foreach($additionalServices as $ads){
							echo '<div class="additional-item">'.
								'<div class="item-name">'.$ads->additional_name.'</div>'.
								'<div class="item-price"><span class="data-item-price" data-price="'.$ads->price.'">'.number_format($ads->price, 2).'</span> / '.$ads->priceper.'</div>'.
								'<div class="item-input">'.
									'<div class="input-group input-group-sm additionalInput">'.
										'<div class="input-group-btn"><button class="btn" data-button="minus">-</button></div>'.
										'<input type="text" name="guest[additionalService]'.$formArrayName.'['.$ads->additionalid.']" value="0" class="form-control" data-itemid="'.$ads->additionalid.'" readonly>'.
										'<div class="input-group-btn"><button class="btn" data-button="plus">+</button></div>'.
									'</div>'.
								'</div>'.
								'<div class="item-icon"><img src="'.Config::get('params.pmsLink').Config::get('imgPath').'/additional/'.$ads->icon.'" class="img-additionalService"></div>'.
							'</div>';
						}
					 ?>
					</div>
					<div class="clearfix">
						<div class="pull-right"><a href="javascript:void(0);" class="hideadditionalService"><?=trans('messages.hideBtn')?></a></div>
					</div>
				</div>
				<?php } ?>
				<div class="clearfix margin-top-15px">
					<div class="pull-left"><strong><?=trans('messages.totalPriceNoTax')?></strong></div>
					<div class="pull-right"><span class="priceNoTaxAndService"><?=number_format($priceWithOutTax, 2);?></span> <?=$currency?></div>
				</div>
				<div class="clearfix margin-top-5px">
					<div class="pull-left"><strong><?=trans('messages.totalTaxAndService')?></strong></div>
					<div class="pull-right"><span class="priceTaxAndService"><?=number_format($priceTax, 2);?></span> <?=$currency?></div>
				</div>
				<div class="clearfix margin-top-5px">
					<div class="pull-left"><strong><?=trans('messages.totalPrice')?></strong></div>
					<div class="pull-right"><span class="priceTotal"></span> <?=$currency?></div>
				</div>
			</div>
		</div>
	</div>
</div>