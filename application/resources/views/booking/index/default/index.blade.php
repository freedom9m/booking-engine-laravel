<?php 
$currencyCode = \Config::get('params.currencyCode');

 ?>
@extends('layouts.booking')

@section('title', 'Booking Engine ')

@section('headerScript')
<link rel="stylesheet" href="{{asset('assets/packages/lightslider/css/lightslider.min.css')}}">
<style>
#containerBox {
  padding: 5px 5px 0;
}
#availableCalendarBtn {
  margin-top: 5px;
}
@media (max-width: 991px) {
  #containerBox {
    padding: 5px 5px 0 !important;
  }
  select {
  	padding: 0 !important;
  }
  .booking-form{
  	max-width: 100% !important;
  }
  #langBox{
	margin-top: -34px !important;
  }
}
#langBox {
    float: right;
    width: 100%;
    max-width: 200px;
    margin-top: -35px;
    text-align: right;
}
#langBox a {
    padding: 5px;
    background: #fff;
    /* border-radius: 16px; */
    border: 1px solid;
}
</style>
@endsection

@section('content')
<div class="modalBox" id="modalBox">
	<div class="modalBoxContentArea">
		<div class="closeModalBox">&times;</div>
		<div class="row">
			<div id="modalBoxContent"></div>
		</div>
	</div>
</div>
<a href="#" class="scrollToTop">Scroll To Top</a>
<div class="container">
	<div id="langBox">
		<a href="?locale=en">EN</a>
		<a href="?locale=th">TH</a>
	</div>
</div>

<div id="availabilityBox">
	<div id="availabilityBody">
		<div id="navigator"><span id="closeAvaibilityBtn">&times;</span></div>
		<div align="center"><input type="text" class="form-control" value="<?=$checkInDate?>" id="avaibilityStartDate"></div>
		<div id="availabilityLoading"></div>
		<div>
			<div align="center" id="avaibilityTitle">
				<span class="avaibilityNav" data-action="backward"><<</span> 
				<span id="avaibilityFrom"></span> 
				to 
				<span id="avaibilityTo"></span> 
				<span class="avaibilityNav" data-action="forward">>></span>
			</div>
			<div id="avaibilityTable" class="table-responsive">
				<table class="table table-condensed table-bordered">
					<thead>
						<tr id="headDay"></tr>
						<tr id="headNumb"></tr>
					</thead>
					<tbody id="avaibility"></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

{!! Form::open(array('url' => 'reservation/'.\Config::get('slug').'/step1', 'id' => 'bookingForm')) !!}
<div class="line-bottom margin-bottom-5px bg-white" id="form-ctrl">
	<div class="container">
		<div class="booking-form">
			<div class="row">

				<div class="col-md-2">
					<div class="topicquick">{{trans('messages.quickReserv1')}}</div>
					<div class="topicquick2">{{trans('messages.quickReserv2')}}</div>
				</div>
				<div class="col-md-5">
					<div class="calendarBox">
						<div class="dateBox datepickerBox bgdate" data-element="#bookingform-checkindate">
							<div class="dateTitle">{{trans('messages.checkIn')}}</div>
							<span class="daydate"></span>
							<span class="month"></span>
							<div align="left">
								<input type="text" value="<?=$checkInDate?>" name="checkInDate" name="checkInDate" id="bookingform-checkindate">
							</div>
						</div>

						<div class="navBox">{{trans('messages.to')}}</div>

						<div class="dateBox datepickerBox bgdate" data-element="#bookingform-checkoutdate">
							<div class="dateTitle">{{trans('messages.checkOut')}}</div>
							<span class="daydate"></span>
							<span class="month"></span>
							<div align="left">
								<input type="text" value="<?=$checkOutDate?>" name="checkOutDate" name="checkOutDate" id="bookingform-checkoutdate">
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-2 btnAndCode">
					<div id="codeText">
						<div>{{trans('messages.pcQuestion1')}}</div>
						<a href="javascript:void(0);">{{trans('messages.pcQuestion2')}}</a>
					</div>
					<div id="codeBox">
						<div class="input-group" id="checkoutdate">
							<input type="text" value="<?=$promotionCode?>" name="promotionCode" id="bookingform-promotioncode" class="form-control">
							<div class="input-group-addon" id="closeCodeBtn">
								<span class="glyphicon glyphicon-remove"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-3 btnAndCode">
					<div class="btnbookquick" id="findAvailableBtn">
						<div class="check1">{{trans('messages.findAvaibilityBtn1')}}</div>
						<div class="check2">{{trans('messages.findAvaibilityBtn2')}}</div>
					</div>
					<button type="button" id="availableCalendarBtn" class="btn">
						<span class="glyphicon glyphicon-eye-open"></span> {{trans('messages.avaibilityChartBtn')}}
					</button>
				</div>
			</div>
		</div>
	</div>	
</div>

<div class="container">
	<div class="clearfix" id="rtBox">
		<div class="pull-left recommendText">
			<h2>{{trans('messages.chooseRoomText')}}</h2>
			<div>{{trans('messages.recommendText')}}</div>
		</div>
		<div class="pull-right">
			<?php //echo Base::app()->params['tripadvisorCode'] ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8" id="roomList">
		</div>
		<div class="col-md-4">
			<div class="summaryPanel sticky">
				<div id="displayRoomSelectedBtn"><a href="javascript:void(0);" id="showDetailBtn"><span class="glyphicon glyphicon-triangle-top" title="show detail"></span></a></div>
				<div id="priceCol"><span class="totalPriceTxt">{{trans('messages.totalPrice')}}</span></div>
				<div id="summaryCol"><span class="currency"><?php echo $currencyCode?></span> <span class="totalPrice" id="summaryPrice">0.00</span></div>
				<div id="boxSelected">
					<div id="roomsCol"><span id="summaryRooms">0</span> {{trans('messages.rooms')}}</span> <span id="noOfNight"></span> {{trans('messages.night')}}</div>
					<div id="roomSelectedBox"></div>
				</div>
				<div id="roomMaximum"> {{trans('messages.maximumRooms')}} : <?php //echo $maximum?></div>
				<div id="buttonCol" align="center"><button type="button" class="btn bookNowBtn" id="bookNowBtn">{{trans('messages.bookNowBtn')}}</button></div>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}
@endsection

@section('footerScript')
<script src="{{asset('assets/packages/moment/moment.js')}}"></script>
<script src="{{asset('assets/packages/lightslider/js/lightslider.min.js')}}"></script>
<script src="{{asset('assets/packages/readmoreJs/readmore.min.js')}}"></script>
<script>
$(document).ready(function() {
	var currency = '<?=$currencyCode?>';
	var maximumBooks = 5;
	var isAllotment = false;

	jQuery.fn.setElementDate = function (date){
		var element = $(this).parent().parent();
		if(typeof date == 'undefined'){
			date = $(this).val();
		}
		element.find('.daydate').html(moment(date).format('DD'));
		element.find('.month').html(moment(date).format('MMM YYYY'));
	}


	var app = {

		timer:0,

		run: function()
		{
			var thisApp = this;
			var noOfNight = this.diffDays();

			$('#bookingform-checkindate').setElementDate();

			$('#bookingform-checkindate').datepicker({
				dateFormat: 'yy-mm-dd', 
				minDate: 0,
				onSelect: function(dateText) {
					$(this).val(dateText);
					var date = moment(dateText);
					$(this).setElementDate(date);
					var minDate = moment(dateText).add(1, 'days').format('YYYY-MM-DD');
					var endDate = moment(dateText).add(parseInt($('#noOfNight').text()), 'days').format('YYYY-MM-DD');
					$('#bookingform-checkoutdate').datepicker("setDate", endDate);
					$('#bookingform-checkoutdate').datepicker( "option", "minDate", minDate );
					$('#bookingform-checkoutdate').setElementDate(endDate);
					thisApp.loadRoomList();
					thisApp.updateLink();
				}
			});

			$('#bookingform-checkoutdate').setElementDate();

			$('#bookingform-checkoutdate').datepicker({
				dateFormat: 'yy-mm-dd', 
				minDate: 1,
				onSelect: function(dateText) {
					$(this).val(dateText);
					var date = moment(dateText);
					$(this).setElementDate(date);
					thisApp.loadRoomList();
					thisApp.updateLink();
				}
			});

			$('#avaibilityStartDate').datepicker({
				dateFormat: 'yy-mm-dd', 
				minDate: 0,
				onSelect: function(dateText) {
					thisApp.loadAvaibility(dateText, null);
				}
			});


			$('body').on('click', '.avaibilityNav', function(){
				var action = $(this).data('action');
				var startDate = $('#avaibilityTitle').data('startdate');
				if(action=='backward')
				{
					startDate = moment(startDate).add(-10, 'days').format('YYYY-MM-DD');
				}else{
					startDate = moment(startDate).add(10, 'days').format('YYYY-MM-DD');
				}
				if(startDate<moment().format('YYYY-MM-DD')){
					startDate = moment().format('YYYY-MM-DD');
				}
				$('#avaibilityStartDate').datepicker("setDate", moment(startDate).format('YYYY-MM-DD'));
				thisApp.loadAvaibility(moment(startDate).format('YYYY-MM-DD'), null);
			});

			$('body').on('click', '.datepickerBox', function(){
				var element = $(this).data('element');
				$(element).show().focus().hide();
			});


			var this_;
			var max = 0;
			$('body').on('focusin', '.roomInput', function(){

				this_ = $(this);
				if($.inArray(this_.data('type'), ['promotion'/*, 'code'*/]) != -1 && isAllotment == true){
					max = parseInt($('#availableRoom_'+this_.data('idroomtype')).data('availabletariff'));
				}else{
					max = parseInt($('#availableRoom_'+this_.data('idroomtype')).val());
				}
				$(this).data('val', this_.val());

			}).change(function(){

				var currentVal = parseInt(this_.data('val'));
				max += currentVal;

				var val = parseInt(this_.val());
				max -= val;

				var isPromotion = false;
				//var rateSamePro = this_.data('ratesamepro');
				if(isAllotment == false){
					$('#availableRoom_'+this_.data('idroomtype')).val(max);
				}else{
					/*if(rateSamePro==true){
						$('#availableRoom_'+this_.data('idroomtype')).data('availabletariff', max);
						$('#availableRoom_'+this_.data('idroomtype')).val(max);
					}else{*/
						if($.inArray(this_.data('type'), ['promotion'/*, 'code'*/]) != -1){
							$('#availableRoom_'+this_.data('idroomtype')).data('availabletariff', max);
							isPromotion = true;
						}else{
							$('#availableRoom_'+this_.data('idroomtype')).val(max);
						}
					/*}*/
				}

				var totalRoomThisType = 0;
				$.each($('.roomInput[data-idroomtype="'+this_.data('idroomtype')+'"]'), function(index, val){
					totalRoomThisType += parseInt($(this).val());
				});
				$.each($('.roomInput[data-idroomtype="'+this_.data('idroomtype')+'"]'), function(index, val){
					if($(this) != this_){
						var isPromotion_ = false;
						if($.inArray($(this).data('type'), ['promotion'/*, 'code'*/]) != -1){
							isPromotion_ = true;
						}

						if((isPromotion_ == isPromotion) || isAllotment == false){ //rateSamePro == true ||
							if(max==0){
								$(this).parent().find('.bookBtn').prop('disabled', true);
							}else{
								$(this).parent().find('.bookBtn').prop('disabled', false);
							}

							/*var selectVal = parseInt($(this).val())+max;
							$.each($('option', this), function(){
								var optionVal = parseInt($(this).val());
								if(optionVal > max && optionVal > selectVal)
								{
									$(this).prop('disabled', true);
								}else{
									$(this).prop('disabled', false);
								}
							});*/
						}
					}
				});

				var totalRooms = thisApp.calculatePrice();
				if(totalRooms > maximumBooks){
					alert('Warning! You cannot book greater than '+maximumBooks+' rooms per time');
					this_.trigger('focusin');
					//this_.find('option[value="'+currentVal+'"]').prop('selected', true).trigger('change');
					this_.val(currentVal).trigger('change');
					var idRoomType = this_.data('idroomtype');
					$('.roomSelectedList[data-id="'+idRoomType+'"]').find('.selectedItem').last().slideUp(function(){
						$(this).remove();
					});
					//$('.bookNowBtn').prop('disabled', true);
				}else{
					$('.bookNowBtn').prop('disabled', false);
				}
				this_.trigger('focusin');

			});

			$('body').on('click', '.rate-button', function(){
				var toggle = $(this).data('toggle');
				var bookingBox = $(this).parent().parent().parent().find('.booking-box');
				var btnBox = $(this).parent().parent();
				if(toggle == 'close')
				{
					//bookingBox.removeClass('show-rate');
					bookingBox.slideUp();
					btnBox.removeClass('show-rate');
				}else{
					//bookingBox.addClass('show-rate');
					bookingBox.slideDown();
					btnBox.addClass('show-rate');
				}
			});

			/*$('body').on('keyup', '#bookingform-promotioncode', function(){
				if (thisApp.timer) {
			        clearTimeout(thisApp.timer);
			    }
			    thisApp.timer = setTimeout(thisApp.resetTimer(), 1500); 
			});*/

			$('body').on('click', '#findAvailableBtn', function(){
				thisApp.loadRoomList();
				thisApp.updateLink();
			});

			thisApp.loadRoomList();

			$('body').on('click', '.bookNowBtn', function(){
				var totalRooms = thisApp.calculatePrice();
				if(totalRooms > maximumBooks){
					alert('Warning! You cannot book greater than '+maximumBooks+' rooms per time');
				}else if(totalRooms > 0 && totalRooms <= maximumBooks){
					$('#bookingForm').submit();
				}else{
					alert('You must select some room.');
				}
			});

			$('body').on('click', '#availabilityBox', function(e){
				if( e.target != this ) {
					return false;
				}else{
					$(this).fadeOut();
					$('body').css('overflow', '');
				}
			});
			
			$('body').on('click', '#closeAvaibilityBtn', function(){
				$('#availabilityBox').fadeOut();
				$('body').css('overflow', '');
			})

			$('body').on('click', '#availableCalendarBtn', function(){
				$('body').css('overflow', 'hidden');
				thisApp.loadAvaibility($('#bookingform-checkindate').val(), null);
				$('#availabilityBox').fadeIn();
			});


			$('body').on('click', '.bookBtn', function(){
				var element = $(this).parent().find('.roomInput');
				element.trigger('focusin');
				var val = parseInt(element.val());
				element.val(val+1);
				element.trigger('change');
			});

			$('body').on('click', '.removeRoom', function(){
				var element = $(this).parent().parent();
				var idRoomType = element.data('id');
				var dataType = element.data('datatype');
				var proid = element.data('proid');
				element.slideUp(function(){
					var elementList = element.parent();
					$(this).remove();
					if(elementList.find('.selectedItem').length == 0){
						elementList.slideUp(function(){
							$(this).remove();
						});
					}
				});
				var inputElement = $('.roomInput[data-idroomtype="'+idRoomType+'"][data-type="'+dataType+'"][data-proid="'+proid+'"]');
				inputElement.trigger('focusin');
				var val = parseInt(inputElement.val());
				inputElement.val(val-1);
				inputElement.trigger('change');
			});

			$('body').on('click', '.roomDetail', function(){
				var roomtypeName = $(this).parent().parent().parent().parent().find('.roomtype-name').html();
				var gallery = $(this).parent().parent().parent().parent().find('.gallery img');
				var galleryList = '';
				var iGallery = 0;
				gallery.each(function(){
					iGallery++;
					var imgUrl = $(this).attr('src');
					galleryList += '<li data-thumb="'+imgUrl+'" data-src="'+imgUrl+'"><img src="'+imgUrl+'"/></li>';
				});
				var content = '<h1 class="modalTitle">Room Details and Pictures</h1><div class="row">'+
				'<div class="col-md-5"><ul id="imageGallery">' + galleryList + '</ul></div>'+
				'<div class="col-md-7"><h1>' + roomtypeName + '</h1>' +$(this).parent().find('section.roomDetailContent').html()+'</div>'+
				'</div>';

				$('#modalBoxContent').html(content);

				var slider = $('#imageGallery').lightSlider({
					gallery: true,
					item: 1,
					loop: true,
					autoWidth: true,
			        //auto: true,
			        speed: 800,
			        thumbItem: 8,
			        slideMargin: 0,
			        enableDrag: true,
			        currentPagerPosition: 'left',
			        onSliderLoad: function(el) {
			            /*el.lightGallery({
			                selector: '#imageGallery .lslide'
			            });*/
			        }   
			    });  

			    /*$('#imageGallery').on('mouseover', function(){
	    			slider.pause();    
			    });

			    $('#imageGallery').on('mouseout', function(){
					slider.play(); 
				});*/

				thisApp.displayModal('#modalBox');


				/*$('#dialogTitle').html('<h1>'+roomtypeName+'</h1>');
				$('#dialogBody').html(content);
				$('#dialogBox').modal('show');*/
			});


			$(window).scroll(function(){ // scroll event
				thisApp.resizeRoomSelectedBox();
			});

			
			/*$(window).scroll(function(){ // scroll event
				if($('#bookNowBtn').isOnScreen()){
					$('.scrollToTop').fadeOut();
				}else{
					$('.scrollToTop').fadeIn();
				}
			});*/

			$('.scrollToTop').click(function(){
				$('html, body').animate({scrollTop : 0},800);
				return false;
			});

			$(window).resize(function() {
				thisApp.resizeRoomSelectedBox();
			});
			

			thisApp.resizeRoomSelectedBox();

			$('body').on('click', '#showDetailBtn', function(){
				$('#boxSelected').slideToggle();
			});

			$('body').on('click', '.readPromotion', function(){
				var content = $(this).parent().parent().parent().parent().find('.rate-promotionDescription').html();
				var element = $(this).parent().parent().parent().parent().parent().find('.moreInfo');
				element.find('#infoContent').html(content);
				element.slideDown();
			});

			$('body').on('click', '.readPolicy', function(){
				var promotionName = $(this).parent().parent().parent().parent().parent().find('.rate-promotionName').text();
				var policyBody = $(this).parent().find('.policyBody').html();
				var policyTitle = $(this).parent().find('.policyTitle').text();
				var roomtypeName = $(this).parent().parent().parent().parent().parent().parent().find('.roomtype-name').text();
				var element = $(this).parent().parent().parent().parent().parent();
				element.find('#infoContent').html(policyBody);
				element.find('.moreInfo').slideDown();
			});

			$('body').on('click', '.closeInfo', function(){
				$(this).parent().parent().parent().slideUp();
			});

			$('body').on('click', '#codeText a', function(){
				$(this).parent().fadeOut(function(){
					$('#codeBox').fadeIn();	
				});
			});

			if($('#bookingform-promotioncode').val()!=''){
				$('#codeText a').trigger('click');
			}

			$('body').on('click', '#closeCodeBtn', function(){
				$('#codeBox').fadeOut(function(){
					if($('#bookingform-promotioncode').val()!=''){
						$('#bookingform-promotioncode').val('');
						$('#findAvailableBtn').trigger('click');
					}
					$('#codeText').fadeIn();
				});
			});

			$('body').on('click', '.closeModalBox', function(){
				$(this).parent().parent().fadeOut();
				$('body').css('overflow', '');
			});

			$(document).on('keyup', function(event){
				if(event.keyCode == 27 && $('#modalBox').is(':visible')){ //escape
					$('#modalBox').find('.closeModalBox').trigger('click');
				}
			});

			$('body').on('click', '.modalBox', function(e){
				if(e.target == this){
					$('.closeModalBox').trigger('click');
				}
			});

		},

		displayModal: function(element){
			$(element).fadeIn();
			$('body').css('overflow', 'hidden');
		},

		resizeRoomSelectedBox: function()
		{
			var topHeight = $('#header').height() + $('#rtBox').height() + $('.booking-form').height() + 21; //21 is margin 
			var selectedBoxHeight = $(window).height();
			if ($(window).scrollTop() > topHeight && $(window).width() > 974 ) { //if screen width > 974  and scroll top > 460 fix side bar
				if($('#footerBox').visibleHeight() > 0){
					selectedBoxHeight -= $('#footerBox').visibleHeight();
				}
				$(".sticky").css({"top": ($(window).scrollTop()) - topHeight + "px"});
				$('.sticky').css('max-height', selectedBoxHeight);
				$('#roomSelectedBox').css('max-height', selectedBoxHeight-140); //138
			}else{
				$('.sticky').css('top', '');
				$('.sticky').css('max-height', selectedBoxHeight);
				$('#roomSelectedBox').css('max-height', selectedBoxHeight-140); //138
			}
		},

		loadAvaibility: function(startDate, allRoomtype)
		{
			$.ajax({
				url: '{{ url('reservation', [\Config::get('slug'), 'getAvaibility']) }}',
				type: 'get',
				data: {startDate: startDate, allRoomtype: allRoomtype},
				dataType: 'json',
				beforeSend: function(){
					$('#avaibilityTable, #avaibilityTitle').slideUp(function(){
						$('#headDay, #headNumb, #avaibility').empty();
					});
					$('#availabilityLoading').html('<i class="fa fa-spinner fa-pulse"></i>');
				}
			}).error(function(){
				alert('Error cannot load avaibility');
			}).done(function(data){
				$('#avaibilityTitle').data('startdate', data.startDate);
				$('#avaibilityFrom').html(data.startDateText);
				$('#avaibilityTo').html(data.endDateText);
				$('#avaibilityTable, #avaibilityTitle').slideDown();
				$('#availabilityLoading').empty();
				var dayInfo = data.dayInfo;
				var roomInfo = data.roomInfo;
				var roomAvaibility = data.avaibility;
				$('#headDay').append('<th rowspan="2">Roomtypes / Days</th>');
				$.each(dayInfo, function(k, v) {
					$('#headDay').append('<th>'+v.day+'</th>');
					$('#headNumb').append('<th>'+v.num+'</th>');
				});

				$.each(roomInfo, function(k, v) {
					var html = '<tr><td>'+v+'</td>';
					$.each(roomAvaibility[k], function(day, variable) {
						var icon = '✕';
						var className = '';
						if(parseInt(variable.avaibility)>0){
							icon = '✓';
							className = 'available';
						}
						html += '<td class="avaibilityTimes '+className+'">'+icon+'</td>';
					})
					html += '</tr>';
					$('#avaibility').append(html);
				});
			}).always(function(){

			});
		},

		resetTimer: function()
		{
			this.timer = 0;
			this.loadRoomList();
			this.updateLink();
		},

		diffDays: function()
		{
			return moment($('#bookingform-checkoutdate').val()).diff(moment($('#bookingform-checkindate').val()), 'days');
		},

		disableInput: function()
		{
			
		},

		updateLink: function()
		{
			window.history.pushState('', '', '{{ url('reservation', [\Config::get('slug')]) }}?checkInDate='+$('input[name="checkInDate"]').val()+'&checkOutDate='+$('input[name="checkOutDate"]').val()+'&promotionCode='+$('input[name="promotionCode"]').val()); // update link

		},

		loadRoomList: function()
		{
			var thisApp = this;
			var params = {checkInDate: $('input[name="checkInDate"]').val(), checkOutDate: $('input[name="checkOutDate"]').val(), promotionCode: $('input[name="promotionCode"]').val()};
			$.ajax({
				url: '{{ url('reservation', [\Config::get('slug'), 'getRoomAvailable']) }}',
				type: 'get',
				dataType: 'html',
				data: params,
				beforeSend: function(){
					$('#roomList').html('<div class="uil-ripple-css" style="transform:scale(0.6);"><div>');
				}
			})
			.done(function(res) {
				$('#roomList').html(res);
			})
			.fail(function( jqXHR, textStatus, errorThrown ) {
				alert('Error load data');
			})
			.always(function() {
				thisApp.calculatePrice();
				$('.roomDescription').readmore({
					speed: 75,
					maxHeight: 40
				});
				$('.amenity').readmore({
					speed: 75,
					maxHeight: 40
				});

				isAllotment = $('#availableList').data('isallotment');
			});
		},

		calcFreeNight: function(totalNight, stay, freeNight)
		{
			if(stay!=0){
				var calc = parseInt(totalNight/stay);
			}else{
				var calc = 1;
			}
			var free = parseInt(calc * freeNight);

			return free;
		},

		calculatePrice: function(){
			var thisApp = this;
			var numNight=this.diffDays();
			var totalPrice=0;
			var totalRoom=0;
			var selectedList = {};

			$.each($('.roomInput'), function(index, val){
				var total = parseInt($(this).val());
				var idroomtype = $(this).data('idroomtype');
				var datatype = $(this).data('type');
				var proid = $(this).data('proid');
				var price = parseFloat($(this).data('price')==''?0:$(this).data('price'));
				var freenight = parseInt($(this).data('freenight'));
				var minstay = parseInt($(this).data('minstay'));
				var rateName = $(this).data('ratename');
				var totalRoomPrice = total*(price*numNight);
				var totalPriceThisRoom = 1*(price*numNight);
				var summaryFreeNight = 0;
				var totalFreeNight = 0;

				var arrayKey = idroomtype+'_'+datatype+'_'+proid;

				totalRoom += total;
				if(freenight > 0){
					summaryFreeNight = thisApp.calcFreeNight(numNight, minstay, freenight);
					totalFreeNight = price*(summaryFreeNight*total);
					totalRoomPrice -= totalFreeNight;
					totalPriceThisRoom -= totalFreeNight;
				}

				totalPrice += totalRoomPrice;


				if(selectedList.hasOwnProperty(idroomtype)==false){
					selectedList[arrayKey] = total;
				}else{
					selectedList[arrayKey] += total;
				}

				if($('.roomSelectedList[data-id="'+idroomtype+'"]').length==0 && selectedList[arrayKey]>0){
					var roomtypename = '<div class="roomSelectedList" data-id="'+idroomtype+'"><h4>'+$('.roomtype-name[data-id="'+idroomtype+'"]').text()+'</h4></div>';
					$('#roomSelectedBox').append(roomtypename);
				}

				var summary = totalRoomPrice.format(2) +' '+currency;
				var summaryWithFreeNight = summaryFreeNight > 0 ? '<div class="row"><div class="col-md-12">Free '+summaryFreeNight+' <i class="fa fa-moon-o" title="free night"></i></div></div>' : ''; //  = '+totalFreeNight.format(2)+' '+currency+' /////  x '+total+' <i class="fa fa-bed" title="rooms"></i>

				var element = $('.selectedItem[data-id="'+idroomtype+'"][data-datatype="'+datatype+'"][data-proid="'+proid+'"]');

				var priceDetail = '<div class="selectedItem" data-id="'+idroomtype+'" data-datatype="'+datatype+'" data-proid="'+proid+'" style="display: none">'+
				'<strong>' + rateName + '</strong>'+
				'<div class="clearfix">'+
				'<div class="pull-left">'+
				'<div>'+price.format(2)+' x '+numNight+' <i class="fa fa-moon-o" title="night"></i></div>'+
				summaryWithFreeNight+
				'</div>'+
				'<div class="pull-right" align="right">'+
				currency + ' ' + totalPriceThisRoom.format(2) +
				'</div>'+
				'</div>'+
				'<div class="clearfix removeBtn" align="center"><a href="javascript:void(0);" class="removeRoom">Remove</a></div>'+
				'</div>';

				if(total>0)
				{
					/*if(element.length>0)
					{
						element.replaceWith(priceDetail);
					}else{
						$('.roomSelectedList[data-id="'+idroomtype+'"]').append(priceDetail);
						$('.selectedItem').slideDown();
					}*/
					if(element.length<total){
						$('.roomSelectedList[data-id="'+idroomtype+'"]').append(priceDetail);
						$('.selectedItem').slideDown();
					}

				}else{
					element.slideUp(function(){
						$(this).remove();
						if($('.roomSelectedList[data-id="'+idroomtype+'"]').find('.selectedItem').length==0){
							$('.roomSelectedList[data-id="'+idroomtype+'"]').slideUp(function(){
								$(this).remove();
							});
						}
					});
				}

				$('#summaryRooms').html(totalRoom);
				$('#summaryPrice').html(totalPrice.format(2));
			});

			$('#noOfNight').html(numNight);

			if(totalRoom > 0){
				$('#roomSelectedBox').slideDown();
			}else{
				$('#roomSelectedBox').slideUp();
			}

			return totalRoom;
		}

	};

	app.run();
});	
</script>
@endsection