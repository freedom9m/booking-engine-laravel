<?php 
use App\Components\ArrayHelper;
use App\Components\BookingComponents;
$inventory = \Config::get('inventory');
?>
<div id="availableList" style="display: none;" data-isallotment="<?=$inventory == 'allotment' ? 'true' : 'false'?>">
	<?php 
	foreach($availableModel as $arrAvailable):
        if($inventory=='allotment'){ // allotment
        	$totalavailable=$arrAvailable->availabletariff;
        	/*if($arrAvailable['closeouttariff']>0){
        		$totalavailable = 0;
        	}*/
        }else{
        	$totalavailable=($arrAvailable->availablerate-($arrAvailable->inhouse/*+$arrAvailable->availabletariff*/));
        	/*if($arrAvailable['closeoutrate']>0){
        		$totalavailable = 0;
        	}*/
        }
        if($arrAvailable->closeouttariff>0 || $arrAvailable->closeoutrate>0){
        	$totalavailable = 0;
        }
        $fields = ['roomtypeid', 'rid', 'rcid', 'tariffid', 'roomtypename'];
        $attributes = '';
        foreach($arrAvailable as $key => $val)
        {
        	if(!in_array($key, $fields)){
        		$attributes .='data-'.$key.'="'.$val.'" ';
        	}
        }
        $totalavailable = $totalavailable < 0 ? 0 : $totalavailable;
        $roomavailableArray[$arrAvailable->roomtypeid]=$arrAvailable;
        echo '<input style="width:25px; text-align:center;" type="text" id="availableRoom_'.$arrAvailable->roomtypeid.'" value="'.$totalavailable.'" '.$attributes.' readonly> <span class="fontwhite">'.$arrAvailable->roomtypename.'</span><br>';
    endforeach;
    ?>
    </div>
<div id="roomList">
	<?php
	$i=0;
	$allRoomList = [];
	$sameRoomList = [];
	$lastRate = null;
	$sortByPrice = true;
	foreach($model as $data){
		$i++;
		if($data['sort'] != null){
			$sortByPrice = false;
		}
		if($data['policyid']==null){
			$policyId = BookingComponents::getPolicy();
		}else{
			$policyId = $data['policyid'];
		}
		$data['policyData'] = BookingComponents::getPolicy($policyId);
		$discountData = BookingComponents::calDiscount($data);
		$data['priceAfterDiscount']=$discountData['price'];
		if($data['priceAfterDiscount'] > 0){
			
			if(in_array($data['datatype'], ['nopromotion', 'code']) /*$data['datatype']=='nopromotion'*/)
			{
				$lastRate = $data;
			}else{
				array_push($sameRoomList, $data);
			}

			if(isset($model[$i]) && $model[$i]['roomtypeid'] == $data['roomtypeid']){
				//nothing on here
			}else{
				$sameRoomList = ArrayHelper::sortByKeyValue($sameRoomList, 'priceAfterDiscount', SORT_ASC);
				if($lastRate != null){
					array_push($sameRoomList, $lastRate);
				}
				list($firstData/*, $second, $third*/) = $sameRoomList; //$firstData = $sameRoomList[0]; // first rate
				//unset($sameRoomList[0]); //remove first rate 
				//$this->renderPartial('_view', ['data'=>$firstData, 'roomavailableArray'=>$roomavailableArray, 'more'=>$sameRoomList]);
				//echo '<hr class="no-margin-bottom">';
				$allRoomList[] = ['sort' => $firstData['sort'], 'lowerPrice' => $firstData['priceAfterDiscount'], 'roomList'=>$sameRoomList];
				$sameRoomList = [];
				$lastRate = null;
			}
		}
	}

	$allRoomList = ArrayHelper::sortByKeyValue($allRoomList, ($sortByPrice == true ? 'lowerPrice' : 'sort'), SORT_ASC);
	if(!empty($allRoomList)){
		$totalRoomList = count($allRoomList);
		$i = 0;
		foreach($allRoomList as $key => $data){
			$i++;
			$roomList = $data['roomList'];
			list($firstData) = $roomList; // first rate
			//print_r($rateGroup);

			echo View::make('booking.index.'.\Config::get('template').'._view', ['key'=>$key, 'data'=>$firstData, 'roomavailableArray'=>$roomavailableArray, 'more'=>$roomList , 'rateGroup'=>[/*$rateGroup*/]])->render();
			if($i != $totalRoomList) {
				echo '<hr class="no-margin-bottom">';
			}
		}
	}else{
		echo '<div class="closeout">'.trans('messages.noRoomAvailable').'</div>';
	}

	?>
</div>