<?php 
$inventory = \Config::get('inventory');
$currencyCode = \Config::get('params.currencyCode');
//print_r($data);
$calDiscount = App\Components\BookingComponents::calDiscount($data);
$occupancyData = App\Components\BookingComponents::getOccupancy($data['roomtypeid']);
$price = $calDiscount['price'];
$oldPrice = $calDiscount['oldPrice'];

/* original calculate */
/*$price = $data['rateprice'];
$oldPrice = null;

if($data['discounttype'] != null){
	$oldPrice = $price;
	$discount = $data['discount'];
	if($data['discounttype'] == 1){ //% Discount
		$price = $price - ($price * ($discount/100));
	}else if($data['discounttype'] == 2){ //Price Discount
		$price = $price - $discount;
	}else if($data['discounttype'] == 3){ //Free Night
		$oldPrice = null;
	}
}*/
if($data['datatype']=='code'){
}
 ?>
<div class="booking-rate rate">
	<div class="promotion">
		<div class="row roomDetails">
			<div class="col-sm-4 hidden-xs">
				@if(count($gallery)>0)
				<?php 
					foreach($gallery as $img){
						//$imagePath = str_replace($dir, $webPath, $gallery['file']);
						$imagePath = Storage::disk('pms')->getDriver()->getAdapter()->applyPathPrefix($img);
						$file = \Config::get('params.pmsLink').$img;
						if(in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), ['jpg', 'jpeg', 'gif', 'gift', 'png']) && is_file($imagePath)){
							echo '<img src="'.$file.'" class="gallery">';
							break;
						}
					}
				?>
				@else
					<img src="{{asset('assets/images/default.jpg')}}" class="gallery">
				@endif
			</div>
			<div class="col-sm-4">
				<div class="rate-promotionName"><?=$data['proname']?></div>
				<div class="rate-promotionDescription"><?=trim($data['prodescription']) == '' ? '-' : $data['prodescription'] ?></div>
				<div class="policy">
					<a href="javascript:void(0);" class="readPromotion"><span class="glyphicon glyphicon-info-sign"></span> {{trans('messages.detail')}}</a>
					<a href="javascript:void(0);" class="readPolicy"><span class="glyphicon glyphicon-exclamation-sign"></span> <?=trans('messages.policy'); ?></a>
					<section>
						<div class="policyBody">
							<?php if($data['policyData']!=null){ ?>
							<h2><?=$data['policyData']->policyname?></h2>
							<div><?=$data['policyData']->description?></div>
							<?php } ?>
						</div>
					</section>
				</div>
			</div>
			<div class="col-sm-2 col-xs-6">
				@if($occupancyData != null)
					@for($i=0;$i<$occupancyData->minadult ;$i++)<img src="{{asset('assets/images/guest.png')}}">@endfor
					@if($occupancyData->minchildren>0)
					@for($i=0;$i<$occupancyData->minchildren ;$i++)<img src="{{asset('assets/images/child.png')}}">@endfor
					@endif
				@endif
			</div>
			<div class="col-sm-2 col-xs-6">
				@if($data['breakfast'] == 1)
					<img src="{{asset('assets/images/breakfast.png')}}" title="{{trans('messages.breakfastIncluded')}}" class="img">
				@endif
			</div>
		</div>
	</div>
	<div class="rate-rightBox">
		<div class="max"><?=$data['minadult']?>&times;<i class="fa fa-user"></i></div>
		<?php 
		if($price > 0){ 
		$oldPrice = ($oldPrice == $price ? null : $oldPrice);
		?>
		<div class="price">
			<div class="old-price"><?=$oldPrice == null ? '' : $currencyCode . ' ' . number_format($oldPrice, 2)?></div>
			<div class="new-price"><?=$currencyCode . ' ' . number_format($price, 2)?> </div>
		</div>
		<div class="input">
			<?php 
			$max = 0;
			$rateSamePro = false;
			$closeOut = false;
			if(isset($roomavailableArray[$data['roomtypeid']])){
				$rate = $roomavailableArray[$data['roomtypeid']];
				/*if($rate['availablerate']<=$rate['availabletariff']){
					$rateSamePro = true;
					$max = $rate['availablerate'];
				}else{*/
					if($inventory == 'allotment'/*in_array($data['datatype'], ['promotion', 'code'])*/){
						if(in_array($data['datatype'], ['promotion', 'code'])){ //promotion get from tariffrate
							$max = $rate->availabletariff;
							if($rate->closeouttariff>0){
								$max = 0;
								$closeOut = true;
							}
						}else{ //get from source id 4 ratecategory
							$max = $rate->ratecategoryavailable;
							if($rate->closeoutratecategory>0){
								$max = 0;
								$closeOut = true;
							}
						}
					}else{
						/*$max = $rate->availablerate-$rate->inhouse;
						if($rate->closeoutrate>0){*/
						$max = $rate->availablerate-$rate->inhouse-$rate->closeoutrate;
						if($max <= 0){
							$max = 0;
							$closeOut = true;
						}
						/*
						component rate control ใน happy ปุ่มเปลี่ยน allotment เป็น inventory เป็นปุ่มหลอก ใช้งานไม่ได้
						*/
						/*if($rate['inhouse']>=$rate['availablerate']){ // a ru mi rai 
							$max = 0;
						}*/
					}
					if(\Config::get('hotelModel')->rateCloseBoth == 1){
				        if($rate->closeouttariff>0 || $rate->closeoutrate>0 || $rate->closeoutratecategory > 0){
							$max = 0;
							$closeOut = true;
				        }
			        }


				//}
			}
			if($closeOut === false){
				if($max>0){
				?>
				<input class="form-control roomInput" name="room[<?=$data['roomtypeid']?>][<?=$data['datatype']?>][<?=$data['proid']?>]" 
					data-idroomtype="<?=$data['roomtypeid']?>" 
					data-type="<?=$data['datatype']?>" 
					data-proid="<?=$data['proid']?>"
					data-price="<?=$price?>"
					data-freenight="<?=$data['discounttype'] == 3 ? $data['discount'] : 'false'?>"
					data-minstay="<?=$data['minstay']?>"
					data-ratename="<?=htmlentities($data['proname'])?>"
					data-ratesamepro="<?=var_export($rateSamePro)?>"
					value="0"
					type="hidden"
					>
				<button type="button" class="btn bookBtn">{{trans('messages.select')}}</button>
				<?php } else { echo '<span class="soldout">'.trans('messages.soldOut').'</span>'; } ?>
			<?php } else { echo '<span class="closeout">'.trans('messages.closeOut').'</span>'; } ?>
		</div>
		<?php }else{ ?>
		<div class="closeout">
			<?=trans('messages.closeOut');?>
		</div>
		<?php } ?>
	</div>
	<div class="moreInfo">
		<div id="infoContent">
			
		</div>
		<div class="clearfix">
			<div class="pull-right">
				<a href="javascript:void(0);" class="closeInfo"><?=trans('messages.closeBtn');?></a>
			</div>
		</div>
	</div>
</div>
