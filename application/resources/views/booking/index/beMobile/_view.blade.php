<?php 
$currencyCode = \Config::get('params.currencyCode');
$galleryId = App\Components\Functions::randomNumber();

$calDiscount = \App\Components\BookingComponents::calDiscount($data);
$price = $calDiscount['price'];

?>
<div class="roomBox">
	<div class="">
		<div class="info-gallery">
			<div class="clearfix propertyNameBox">
				<div class="pull-left">
					<h3 class="roomtype-name" data-id="<?=$data['roomtypeid']?>"><?=$data['roomtypename']?></h3>
				</div>
			</div>
			<?php 
			$files = \Storage::disk('pms')->files('roomtype/'. \Config::get('pathName') .'/'. $data['roomtypeid']);

			if(count($files)>0){

				$iPhoto = 0;
				$galleryNav = '';
				$galleryBody = '';
				foreach($files as $gallery){
					//$imagePath = str_replace($dir, $webPath, $gallery['file']);
					$imagePath = Storage::disk('pms')->getDriver()->getAdapter()->applyPathPrefix($gallery);
					$file = \Config::get('params.pmsLink').$gallery;
					if(in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), ['jpg', 'jpeg', 'gif', 'gift', 'png']) && is_file($imagePath)){
						$galleryNav .= '<li data-target="#'.$galleryId.'" data-slide-to="'.$iPhoto.'" class="'.($iPhoto == 0 ? 'active' : '').'"></li>';
						$galleryBody .= '<div class="item '.($iPhoto == 0 ? 'active' : '').'"><img src="'.$file.'" style="width: 100%; max-height: 400px"></div>';
						$iPhoto++;
					}
				}
			
			 ?>

			<div id="<?=$galleryId?>" class="carousel slide gallery visible-xs" data-ride="carousel">
			  <ol class="carousel-indicators">
			    <?php echo $galleryNav?>
			  </ol>
			  <div class="carousel-inner" role="listbox">
				<?php echo $galleryBody?>
			  </div>
			  <a class="left carousel-control" href="#<?php  echo $galleryId?>" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#<?php  echo $galleryId?>" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
			<?php 
			}else{
				//echo '<img src="'.asset('assets/images/default.jpg').'" class="gallery">';
			}
			 ?> 
		</div>
		<div class="info-detail">
			<div class="clearfix">
				<div class="pull-left">
					<a href="javascript:void(0);" class="roomDetail">
						<?=trans('messages.showDetailBtn'); ?> <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
					</a>
					<section class="roomDetailContent">
						<div class="roomDescription">
							<div><?=strip_tags($data['description'])?></div>
						</div>
						<hr class="descriptionLine">
						<h4><?=trans('messages.amentities')?></h4>
						<div class="amenity">
							<?php echo \App\Components\BookingComponents::getAmenity($data['roomtypeid']);?>
						</div>
					</section>
				</div>
				<div class="pull-right">
					<strong><?=trans('messages.rateFrom')?>
					<?=$currencyCode .' '. number_format($price, 2);?>
					<?=trans('messages.costFor1Night')?>
					</strong> 
				</div>
			</div>
			<div class="row" style="display: none;">
				<!-- <div class="col-xs-8 col-sm-9">
					room detail
				</div> -->
				<div class="col-xs-4 col-sm-3 firstRate">
					<?php
					if($price > 0){
					?>
					<div><strong><?=trans('messages.rateFrom')?></strong></div>
					<div>
					<?php
						echo $currencyCode . ' ' . number_format($price, 2);
					 ?>
					</div>
					<div><?=trans('messages.costFor1Night')?></div>
					<?php //$this->renderPartial('_rate', ['data'=>$data, 'roomavailableArray'=>$roomavailableArray]); ?>

					<?php }else{ ?>
					<div class="closeout"><?=trans('messages.closeOut')?></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="booking-block clearfix">
		<div class="booking-gallery" style="display: none;">
			@if(count($files)>0)
			<?php 
				foreach($files as $img){
					//$imagePath = str_replace($dir, $webPath, $gallery['file']);
					$imagePath = Storage::disk('pms')->getDriver()->getAdapter()->applyPathPrefix($img);
					$file = \Config::get('params.pmsLink').$img;
					if(in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), ['jpg', 'jpeg', 'gif', 'gift', 'png']) && is_file($imagePath)){
						echo '<img src="'.$file.'" class="gallery">';
						break;
					}
				}
			?>
			@else
				<img src="{{asset('assets/images/default.jpg')}}" class="gallery">
			@endif
		</div>
		<div class="booking-content">
			<?php
			$loadMore = false;
			if(count($more)>0){
				$i=0;
				$rateHtml = '';
				foreach($more as $list)
				{
					if($i <= 1){
						echo View::make('booking.index.'.\Config::get('template').'._rate', ['data'=>$list, 'roomavailableArray'=>$roomavailableArray, 'gallery' => $files])->render();
					}else{
						$loadMore = true;
						$rateHtml .= View::make('booking.index.'.\Config::get('template').'._rate', ['data'=>$list, 'roomavailableArray'=>$roomavailableArray, 'gallery' => $files])->render();
					}
					$i++;
				}
			?>
			<div class="booking-box bordered <?php //=$key==0?'show-rate':''?>">

				<div class="booking-rate-list">
					<?php echo $rateHtml; ?>
				</div>
			</div>
			<?php 
			}
			if($loadMore){
			?>
			<div class="rate-btn-group">
				<div align="center">
					<button type="button" class="rate-button show-button" data-toggle="show"><?=trans('messages.rateBtn-show')?> <span class="glyphicon glyphicon-menu-down"></span></button>
					<button type="button" class="rate-button close-button" data-toggle="close"><?=trans('messages.rateBtn-hide')?> <span class="glyphicon glyphicon-menu-up"></span></button>
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</div>

<!-- 	<div class="clearfix">
	<div class="pull-right visible-xs visible-sm">
		<button type="button" class="btn bookNowBtn"><?=trans('bookNowBtn')?></button>
	</div>
</div> -->
	<div class="row">

	</div>
</div>
<?php 

//print_r($data);

