<?php 
$currency = \Config::get('params.currencyCode');;


 ?>
<div class="margin-top-15px confirm-list">
	<div class="clearfix confirm-head">
		<div class="pull-left"><strong><?=trans('messages.bookingId')?> : <?=$bookingid?></strong></div>
		<div class="pull-right"><?=trans('messages.roomNo')?> <?=$no?></div>
	</div>
	<div class="confirm-body">
		<div class="row margin-top-5px">
			<div class="col-xs-5 col-sm-2"><strong><?=trans('messages.guestName')?></strong></div>
			<div class="col-xs-7 col-sm-10"><?=$guestName?></div>
		</div>
		<div class="row margin-top-5px">
			<div class="col-xs-5 col-sm-2"><strong><?=trans('messages.roomType')?></strong></div>
			<div class="col-xs-7 col-sm-10"><?=$roomTypeName?></div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.noOfNight')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=$noOfNight?></div>
				</div>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.dateOfArrival')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=$checkInDate?></div>
				</div>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.dateOfDeparture')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=$checkOutDate?></div>
				</div>
				<?php if($promotion != null){ ?>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.promotion');?></strong></div>
					<div class="col-xs-7 col-sm-8">
						<?php echo $promotion['name'] ?>
					</div>
				</div>
				<?php } ?>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.specialRequest')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=$specialRequest;?></div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.roomCharge')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=number_format($totalPrice, 2)?> <?=$currency?></div>
				</div>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.extraBedAdult')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=number_format($totalExtraAdult, 2)?> <?=$currency?></div>
				</div>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.extraBedChild')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=number_format($totalExtraChild, 2)?> <?=$currency?></div>
				</div>
				<?php 
				if($totalAdditionalService > 0):
				?>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.additionalServiceText')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=number_format($totalAdditionalService, 2)?> <?=$currency?></div>
				</div>
				<?php 
				endif;
				?>

				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.totalTaxAndService')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=number_format($taxAndServiceCharge, 2)?> <?=$currency?></div>
				</div>
				<div class="row margin-top-5px">
					<div class="col-xs-5 col-sm-4"><strong><?=trans('messages.totalOfBooking')?></strong></div>
					<div class="col-xs-7 col-sm-8"><?=number_format($totalOfBooking, 2)?> <?=$currency?></div>
				</div>
			</div>
		</div>
		
		<?php
		 if($totalAdditionalService>0): ?>
		<hr>
		<strong>Additional Lists.</strong>
		<table class="table table-condensed table-striped table-bordered">
			<thead>
				<tr>
					<td><?=trans('messages.additionalServiceText')?></td>
					<td><?=trans('messages.price')?></td>
				</tr>
			</thead>
			<tbody>
			<?php 
			foreach($additional as $method)
			{
				$additionalData = \App\Components\BookingComponents::getAddtionalServicesData($method->additionalid);
				echo '<tr>
					<td>'.$additionalData->additional_name.'</td>
					<td>'.number_format($method->total, 2).' '.$currency.'</td>
				</tr>';
			}
			?>	
			</tbody>
		</table>
		<?php endif; ?>
	</div>
</div>
<?php if(isset($policy)): ?>
<div class="margin-top-15px  policyBox">
	<h4 class="policyButton">Policy : <?php echo $policy->policyname ?></h4>
	<div class="policyDetail"><?php echo $policy->description ?></div>
</div>
<?php endif; ?>