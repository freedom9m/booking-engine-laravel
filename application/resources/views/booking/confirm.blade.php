@extends('layouts.booking')

@section('title', 'Payment Confirm')

@section('headerScript')
	
@endsection

@section('content')
<div class="container">
	<div align="center">
		<h4>{{trans('messages.reservationText')}} {{$referenceId}}</h4>
		{{trans('messages.reservationThankyouText')}}
		<hr>	
		{{trans('messages.thankyouText')}} {{Config::get('hotelModel')->hotelname}}
		<div class="websiteLink">click <a href="{{Config::get('printSettings')->website}}">here</a> going to website</div>
	</div>
</div>
@endsection

@section('footerScript')

@endsection
