@extends('layouts.booking')

@section('title', 'Credit card')

@section('headerScript')
	
@endsection

@section('content')

{!! Form::open(array('url' => 'reservation/'.\Config::get('slug').'/bankResponse', 'id' => 'bookingForm')) !!}
{{ Form::hidden('referenceId', $referenceId) }}
{{ Form::hidden('contactName', $contactName) }}
{{ Form::hidden('contactEmail', $contactEmail) }}
{{ Form::hidden('summaryPrice', $totalPrice) }}

<div class="container">
    <div class="row">
        <div class="col-sm-8">
			<div class="form-group">
	        	{{ Form::label('cardHolder', trans('messages.cardHolder'), ['class' => 'control-label']) }}
				{{ Form::text('cardHolder', null, ['class'=>'form-control cc-holdername']) }}
			</div>
        </div>
        <div class="col-sm-4">
			<div class="form-group">
				{{ Form::label('cardType', trans('messages.cardType'), ['class' => 'control-label']) }}
				{{ Form::select('cardType', [
						'Visa'	=> 'Visa',
						'MasterCard'	=> 'MasterCard',
					], null, ['class'=>'form-control cc-cardtype']) }}
			</div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
			<div class="form-group">
				{{ Form::label('cardNo', trans('messages.cardNo'), ['class' => 'control-label']) }}
				{{ Form::text('cardNo', null, ['placeholder'=>'•••• •••• •••• ••••', 'class'=>'form-control cc-number']) }}
			</div>
        </div>
        <div class="col-sm-2">
			<div class="form-group">
				{{ Form::label('expireMonth', trans('messages.cardExpireMonth'), ['class' => 'control-label']) }}
				{{ Form::select('expireMonth', array_combine(range(1, 12), range(1, 12)), null, ['class'=>'form-control cc-expmonth']) }}
			</div>
        </div>
        <div class="col-sm-3">
			<div class="form-group">
				{{ Form::label('expireYear', trans('messages.cardExpireYear'), ['class' => 'control-label']) }}
				{{ Form::select('expireYear', array_combine(range(date('Y'), date('Y')+7), range(date('Y'), date('Y')+7)), null, ['class'=>'form-control cc-expyear']) }}
			</div>
        </div>
        <div class="col-sm-2">
        </div>
    </div>

    <div align="center"><button class="btn btn-default" type="button" id="continue"><?=trans('messages.continueBtn')?></button></div>


</div>
{!! Form::close() !!}

@endsection

@section('footerScript')
<script type="text/javascript" src="{{asset('assets/packages/jquery-payment/lib/jquery.payment.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.cc-number').payment('formatCardNumber');
		$('.cc-exp').payment('formatCardExpiry');

		$.fn.toggleInputError = function(erred) {
		    this.parent('.form-group').toggleClass('has-error', erred);
		    return this;
		};

		$('.cc-holdername').keyup(function(){
		    $('.cc-holdername').toggleInputError(($('.cc-holdername').val().length<5?true:false));
		})

		$('.cc-number').keyup(function(){
		    $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
		})

		/*$('.cc-exp').keyup(function(){
		    $('.cc-exp').toggleInputError(!$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
		})*/

		$('.cc-expcard').change(function(){
		    $('.cc-expmonth').toggleInputError(!$.payment.validateCardExpiry($('#cc-expmonth').val(), $('#cc-expyear').val()));
		    $('.cc-expyear').toggleInputError(!$.payment.validateCardExpiry($('#cc-expmonth').val(), $('#cc-expyear').val()));
		})

		$('body').on('click', '#continue', function() {
		    //var cardType = $.payment.cardType($('.cc-number').val());
		    $('.cc-holdername').toggleInputError(($('.cc-holdername').val().length<5?true:false));
		    $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
		    $('.cc-expmonth').toggleInputError(!$.payment.validateCardExpiry($('.cc-expmonth').val(), $('.cc-expyear').val()));
		    $('.cc-expyear').toggleInputError(!$.payment.validateCardExpiry($('.cc-expmonth').val(), $('.cc-expyear').val()));
		    //$('.cc-cvv').toggleInputError(($('.cc-cvv').val().length!=3?true:false));
		                       
		    //$('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
		    //$('.cc-brand').text(cardType);
		     
		    $('.validation').removeClass('text-danger text-success');
		    $('.validation').addClass($('.has-error').length ? 'text-danger' : 'text-success');

		    if($('.has-error').length==0){
		        $('#bookingForm').submit();
		    }
		});

	});
</script>
@endsection
