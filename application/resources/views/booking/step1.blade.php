<?php 
$checkInDate = $data['checkInDate'];
$checkOutDate = $data['checkOutDate'];
$promotionCode = $data['promotionCode'];
$countriesParams = Config::get('params.countries');
$countries = array_combine($countriesParams, $countriesParams);
 ?>
@extends('layouts.booking')

@section('title', 'Guest Information')

@section('headerScript')

@endsection

@section('content')
<div class="container">
	
{!! Form::open(array('url' => 'reservation/'.\Config::get('slug').'/step2', 'id' => 'bookingForm')) !!}
	{{ Form::hidden('checkInDate', $checkInDate) }}
	{{ Form::hidden('checkOutDate', $checkOutDate) }}
	<?php $noOfNight = App\Components\Functions::dateDiff($checkInDate, $checkOutDate);?>
	<div class="row">
		<div class="col-sm-12">
			<div class="bkBox">
				<strong>{{trans('messages.checkIn')}}</strong> <span>{{date('d M, Y', strtotime($checkInDate))}}</span>
				<strong>{{trans('messages.checkOut')}}</strong> <span>{{date('d M, Y', strtotime($checkOutDate))}}</span>
				<strong>{{trans('messages.noOfNight')}}</strong> <span>{{$noOfNight}}</span>

				<div class="clearfix" style="display: none;">
					<div class="bookingInformation">
						<div class="b-month"><?=date('M', strtotime($checkInDate));?></div>
						<div class="b-date"><?=date('d', strtotime($checkInDate));?></div>
						<div class="b-year"><?=date('Y', strtotime($checkInDate));?></div>
					</div>
					<div id="dateRangeSeparator">
						<span class="noHighlight spacer"></span>
						<span class="noHighlight topLeft"></span><span class="noHighlight topRight"></span>
						<span class="noHighlight spacer"></span>
						<span class="noHighlight seperatorText">TO</span>
						<span class="noHighlight spacer"></span>
						<span class="noHighlight bottomLeft"></span><span class="noHighlight bottomRight"></span>
						<span class="noHighlight spacer"></span>
					</div>
					<div class="bookingInformation">
						<div class="b-month"><?=date('M', strtotime($checkOutDate));?></div>
						<div class="b-date"><?=date('d', strtotime($checkOutDate));?></div>
						<div class="b-year"><?=date('Y', strtotime($checkOutDate));?></div>
					</div>
				</div>
			</div>
			<div class="bookingAlert" style="display: none;"><?=trans('messages.fillFormText')?></div>
		</div>
		<div class="col-sm-12">
			<div id="contactBox">
				<div class="row" style="display: none;">
					<div class="col-sm-6">
						<label for="copyGuestName"><input type="checkbox" id="copyGuestName"> <?=trans('messages.copyFieldBtn')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2">
					{{ Form::label('prefix', '* '.trans('messages.prefix'), ['class' => 'control-label', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Prefix']) }}
    				{{ Form::select('prefix', [
							'Mr.'	=> 'Mr.',
							'Miss.'	=> 'Miss.',
							'Mrs.'	=> 'Mrs.',
							'Dr.'	=> 'Dr.',
						], null, ['class' => 'form-control']) }}
					</div>
					<div class="col-sm-4">
						{{ Form::label('firstname', '* '.trans('messages.firstname'), ['class' => 'control-label']) }}
						{{ Form::text('firstname', null, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Firstname']) }}
					</div>
					<div class="col-sm-4">
						{{ Form::label('lastname', '* '.trans('messages.lastname'), ['class' => 'control-label']) }}
						{{ Form::text('lastname', null, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Lastname']) }}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						{{ Form::label('address', '* '.trans('messages.address'), ['class' => 'control-label']) }}
						{{ Form::text('address', null, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Address']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('city', trans('messages.city'), ['class' => 'control-label']) }}
						{{ Form::text('city', null, ['class' => 'form-control', 'data-validation-label' => 'City']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('zip', trans('messages.zip'), ['class' => 'control-label']) }}
						{{ Form::text('zip', null, ['class' => 'form-control', 'data-validation-label' => 'Zipcode']) }}
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						{{ Form::label('countryofpassport', trans('messages.countryOfPassport'), ['class' => 'control-label']) }}
	    				{{ Form::select('countryofpassport', $countries, null, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY]', 'data-validation-label' => 'Country of Passport']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('phone', trans('messages.phone'), ['class' => 'control-label']) }}
						{{ Form::text('phone', null, ['class' => 'form-control']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('email', '* '.trans('messages.email'), ['class' => 'control-label']) }}
						{{ Form::email('email', null, ['class' => 'form-control', 'data-validation' => '[NOTEMPTY,EMAIL]', 'data-validation-label' => 'Email']) }}
					</div>
					<div class="col-sm-3">
						{{ Form::label('confirmEmail', '* '.trans('messages.confirmEmail'), ['class' => 'control-label']) }}
						{{ Form::email('confirmEmail', null, ['class' => 'form-control', 'data-validation' => '[V==email]', 'data-validation-message' => 'Confirm Email must be equal to Email.', 'data-validation-label' => 'Confirm Email']) }}
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div id="guestBox">
		<?php 
		echo \App\Widgets\ListView::run([
			'id'		=> 'clientInput',
			'data'		=> $data['rooms'],
			'viewData'	=> ['bookingInfo'=>['checkInDate'=>$checkInDate, 'checkOutDate'=>$checkOutDate], 'countries'=>$countries],
			'template'	=> '<div class="items">{{items}}</div>',
			'itemView'	=> 'booking.step1.clientForm',
			/*'itemView' => function($data, $index, $widget){ 
				return $data['roomtypeid'];
			},*/
		]);

		 ?>
	</div>
	<div id="specialRequestBox">
		<span id="toggleRequest"><?=trans('messages.specialRequestText')?> <span class="glyphicon glyphicon-plus btnpluss"></span></span>
		<div id="requestInput">
			<div class="row">
				<div class="col-sm-5">
					{{ Form::textarea('specialrequest', null, ['class' => 'form-control', 'cols' => false, 'rows' => 4]) }}
				</div>
				<div class="col-sm-7">
					<?=trans('messages.specialRequestMessage')?>
				</div>
			</div>
		</div>
	</div>
	<div id="summaryBox" align="center">
		<div id="priceText"><?=trans('messages.totalPrice')?> <span id="totalPrice"></span>  <?=Config::get('params.currencyCode')?></div>
		<div><button type="submit" class="btn btn-default"><?=trans('messages.continueBtn')?></button></div>
	</div>
{!! Form::close() !!}

</div>
@endsection

@section('footerScript')

<script>
	$(document).ready(function() {
		$("#bookingForm").validate({submit: {settings: {inputContainer: ".form-group"},},});

		var app = {
			calculatePrice: function(){
				var totalPrice = 0;
				$('.booking-form-list').each(function(index, val){

				    var adultElement=$(this).find('select[data-extraname="adults"]')
				    var freenight=adultElement.data('freenight');
				    var extrabed=adultElement.data('extrabed');
				    var extrarate=adultElement.data('extrarate');
				    var maxplusbed=adultElement.data('maxplusbed');

				    var childElement=$(this).find('select[data-extraname="childs"]')
				    var minchildren=childElement.data('minchildren');
				    var maxchildren=childElement.data('maxchildren');
				    var extrachildrate=childElement.data('extrachildrate');

				    var maxInfantAge = childElement.data('maxinfantage');
				    var maxChildrenAge = childElement.data('maxchildrenage');
				    var minAdultAge = maxChildrenAge+1;

				    var plusExtraInfant = childElement.data('plusextrainfant');
				    var plusExtraChild = childElement.data('plusextrachild');
				    var plusExtraAdult = childElement.data('plusextraadult');

				    var numnight=parseInt('<?=$noOfNight?>');
				    var totalextra=0;
				    var totalchild=0;

				    var maxAgeOfChildren = 0;
				    if(adultElement.val()>extrabed){
				    	if(plusExtraAdult == 1){
				        	var plusbed=adultElement.val()-extrabed;
				        	totalextra=(extrarate*plusbed)*(numnight);
				        	//totalextra=(extrarate*plusbed)*(numnight-freenight); // for discount extra
				        }
				    }else{
				        totalextra=0;
				    }
				    if(childElement.val() > 0){
				    	var i = 0;
				    	var iExtrabed = parseInt(adultElement.val());
				    	$(this).find('.ageOfChildrenList').each(function(index){
				    		if(iExtrabed < extrabed){
				    			iExtrabed++;
				    		}else{
					    		var childAge = parseInt($(this).val());
					    		if(childAge < minAdultAge){
					    			i++; 
					    		}
					    		var plus = false;
					    		var isAdult = false;
					    		if(maxInfantAge >= childAge && plusExtraInfant == 1){
					    			plus = true;
					    		}else if(maxInfantAge < childAge && maxChildrenAge >=  childAge && plusExtraChild == 1){
					    			plus = true;
					    		}else if(maxChildrenAge < childAge && minAdultAge <= childAge && plusExtraAdult == 1){
					    			isAdult = true;
					    			plus = true;
					    		}
					    		if(plus == true && isAdult == false && i > minchildren){
					    			totalextra += extrachildrate * numnight;
					    		}else if(plus == true && isAdult == true){
					    			totalextra += extrarate * numnight;
					    		}
					    		/*if(parseInt($(this).val()) > maxAgeOfChildren){
					    			maxAgeOfChildren = parseInt($(this).val());
					    		}*/

				    		}
				    	});
				    }

				    /*if(childElement.val()>minchildren){
				        var pluschild=childElement.val()-minchildren;
				        totalchild=(extrachildrate*pluschild)*(numnight);
				        //totalchild=(extrachildrate*pluschild)*(numnight-freenight); // for discount extra
				        //totalextra +=totalchild;
				    }*/
 
				    var summaryExtra = totalextra+totalchild;
					//var summaryExtra = parseFloat($(this).find('.priceExtrabed').html().replaceText(',', ''));

				    $(this).find('.priceExtrabed').html((summaryExtra).format(2));

					var priceAllNight = $(this).find('.priceAllNight').html();
					var sumAdditionalPrice = 0;

					if($(this).find('.priceAfterDiscount').length==1){
						priceAllNight = $(this).find('.priceAfterDiscount').html();
					}

					priceAllNight = parseFloat(priceAllNight.replaceText(',', ''));
					$(this).find('.additional-item-price').each(function(){
						// sumAdditionalPrice += additionalPrice = parseFloat($(this).html().replaceText(',', ''));
						sumAdditionalPrice += parseFloat($(this).html().replaceText(',', ''));
					});



					var totalItemPrice = priceAllNight+sumAdditionalPrice+summaryExtra;
					var priceWithoutTax = totalItemPrice/<?=Config::get('hotelModel')->vat?>;
					var tax = totalItemPrice - priceWithoutTax;
					$(this).find('.priceNoTaxAndService').html(priceWithoutTax.format(2));
					$(this).find('.priceTaxAndService').html(tax.format(2));
					$(this).find('.priceTotal').html(totalItemPrice.format(2));

					totalPrice += totalItemPrice;

				});

				$('#totalPrice').html(totalPrice.format(2));
			}
		};

		$('body').on('change', '#copyGuestName', function(){
			var prefix = $('#prefix').val();
			var firstname = $('#firstname').val();
			var lastname = $('#lastname').val();
			var checked = $(this).is(':checked');
			$.each($('.booking-form-list'), function(index, val){
				if(checked == true){
					$(this).find('select[data-input="prefix"]').find('option[value="'+prefix+'"]').prop('selected', true);
					$(this).find('input[data-input="firstname"]').val(firstname);
					$(this).find('input[data-input="lastname"]').val(lastname);
				}else{
					$(this).find('select[data-input="prefix"]').find('option[value="Mr."]').prop('selected', true);
					$(this).find('input[data-input="firstname"]').val('');
					$(this).find('input[data-input="lastname"]').val('');
				}
			});
		});

		$('body').on('click', '.copyAbove', function(){
			var mainLayout = $(this).parent().parent().parent().parent().parent().parent();  // start at .row inside .booking-form-list
			var prefix = $('#prefix').val();
			var firstname = $('#firstname').val();
			var lastname = $('#lastname').val();

			mainLayout.find('select[data-input="prefix"]').find('option[value="'+prefix+'"]').prop('selected', true);
			mainLayout.find('input[data-input="firstname"]').val(firstname);
			mainLayout.find('input[data-input="lastname"]').val(lastname);
		});

		$('body').on('click', '.manageAdS', function(){
			var mainLayout = $(this).parent().parent().parent().parent();
			mainLayout.find('.additionalServiceBox').slideToggle(function(){

			});
		});

		$('body').on('click', '.hideadditionalService', function(){
			$(this).parent().parent().parent().slideUp(function(){

			});
		});

		$('body').on('click', '#toggleRequest', function(){
			$('#requestInput').slideToggle();
		});

		$('body').on('change', '.extraOpt', function(){
			var inputType = $(this).data('extraname');
			var mainLayout = $(this).parent().parent().parent().parent().parent().parent();
		      
		    var adultElement=mainLayout.find('select[data-extraname="adults"]')
		    //var freenight=adultElement.data('freenight');
		    var extrabed=adultElement.data('extrabed');
		    /*var extrarate=adultElement.data('extrarate');
		    var maxplusbed=adultElement.data('maxplusbed');*/

		    var childElement=mainLayout.find('select[data-extraname="childs"]')
		    /*var minchildren=childElement.data('minchildren');
		    var maxchildren=childElement.data('maxchildren');
		    var extrachildrate=childElement.data('extrachildrate');*/

		    if(inputType=='adults'){  //adults    childs
		        childElement.find('option[value=0]').prop('selected', true);
		        mainLayout.find('.ageOfChildrenList').remove();
		        mainLayout.find('.ageOfChildrenBox').slideUp();
		    }else{
		        // adultElement.find('option[value='+extrabed+']').prop('selected', true);
		        var totalChildren = parseInt($(this).val());
		        var ageInputName = $(this).data('ageinputname');
		        var totalOnList = mainLayout.find('.ageOfChildrenList').length;
		        var minAdultAge = $(this).data('minadultage');
		        if(totalOnList > totalChildren){
		        	// remove on rec
		        	var totalToRemove = totalOnList - totalChildren;
		        	for(var i=0; i<totalToRemove; i++){
		        		var index = totalOnList - i;
		        		mainLayout.find('.ageOfChildrenList:nth-child('+index+')').remove();
		        	}
		        }else{
			        totalChildren -= totalOnList;
			        var ageOptions = '';
			        for(var i=0; i < minAdultAge; i++){ //<=
				        ageOptions += '<option value="'+i+'">'+i+(i == minAdultAge ? '+' : '')+'</option>';
			        }
			        for(var i=totalOnList; i<totalOnList+totalChildren; i++){
			        	var html = '<select name="'+ageInputName+'['+i+']" class="form-control ageOfChildrenList">'+ageOptions+'</select> ';
			        	mainLayout.find('#ageOfChildrenForm').append(html);
			        }
		        }
		        if(parseInt($(this).val()) > 0){
		        	mainLayout.find('.ageOfChildrenBox').slideDown();
		        }else{
		        	mainLayout.find('.ageOfChildrenBox').slideUp();
		        }

		    }

		    /*var numnight=parseInt('<?=$noOfNight?>');
		    var totalextra=0;
		    var totalchild=0;
		    if(adultElement.val()>extrabed){
		        var plusbed=adultElement.val()-extrabed;
		        totalextra=(extrarate*plusbed)*(numnight); //
		        //totalextra=(extrarate*plusbed)*(numnight-freenight); // for discount extra
		    }else{
		        totalextra=0;
		    }
		    if(childElement.val()>minchildren){
		        var pluschild=childElement.val()-minchildren;
		        totalchild=(extrachildrate*pluschild)*(numnight); //
		        //totalchild=(extrachildrate*pluschild)*(numnight-freenight); // for discount extra

		        
		        //totalextra +=totalchild;
		    }

		    mainLayout.find('.priceExtrabed').html((totalextra+totalchild).format(2));*/
			app.calculatePrice();

		});

		$('body').on('change', '.ageOfChildrenList', function(){
			app.calculatePrice();
		});

		$('body').on('click', '.additionalInput button', function(e){
			e.preventDefault();
			var buttonType = $(this).data('button');
			var input = $(this).parent().parent().find('input');
			var originalVal = parseInt(input.val());
			if(buttonType=='minus'){
				var newVal = originalVal-1;
				if(newVal < 0){
					alert('<?=trans('messages.additionalServiceLessTotalText');?>');
				}else{
					input.val(newVal);
				}
			}else if(buttonType=='plus'){
				var newVal = originalVal+1;
				input.val(newVal);
			}
			
			input.trigger('change');

		});

		$('select.extraOpt[data-extraname="childs"]').each(function(){
			if($(this).val() > 0){
				$(this).trigger('change');
			}
		});

		$('body').on('change', '.additionalInput input', function(){
			var input = $(this).parent().parent().find('input');
			var itemid = input.data('itemid');
			var item = $(this).parent().parent().parent();
			var mainLayout = $(this).parent().parent().parent().parent().parent().parent().parent();
			var newVal = $(this).val();
			if(newVal <= 0){
				mainLayout.find('.additional-item-listed[data-item-id="'+itemid+'"]').slideUp(function(){
					$(this).remove();
					if(mainLayout.find('.additional-item-listed').length==0){
						mainLayout.find('.additionalServiceList').html('not selected');
					}
					app.calculatePrice();
				});
			}else{
				if(mainLayout.find('.additionalServiceList').html() == 'not selected'){
					mainLayout.find('.additionalServiceList').empty();
				}
				var itemname = item.find('.item-name').html();
				var itemPrice = parseFloat(item.find('.data-item-price').data('price'));
				var total = newVal*itemPrice;
				var html = '<div class="clearfix additional-item-listed" data-item-id="'+itemid+'">'+
					'<div class="pull-left">'+itemname+'</div>'+
					'<div class="pull-right">'+itemPrice.format(2)+' x '+newVal+' = <span class="additional-item-price">'+total.format(2)+'</span></div>'+
				'</div>';
				if(mainLayout.find('.additional-item-listed[data-item-id="'+itemid+'"]').length==0){
					mainLayout.find('.additionalServiceList').append(html);
				}else{
					mainLayout.find('.additional-item-listed[data-item-id="'+itemid+'"]').replaceWith(html);
				}
				app.calculatePrice();
			}
		});

		$('body').on('click', '.removeRoom', function(){
			if($('.noOfRoomLabel').length > 1){
				if(confirm('Are you sure to delete this item?')){
					$(this).parent().slideUp(function(){
						$(this).remove();
						$('.noOfRoomLabel').each(function(index, ele){
							$(this).html(index+1);
						});
					});
				}
			}else{
				alert('Minimum No. of room is 0 (zero)');
			}
		});

		var tooltips = document.querySelectorAll('.additional-item .item-icon');

		window.onmousemove = function (e) {
		    var x = (e.clientX + 20) + 'px',
		        y = (e.clientY + 20) + 'px';
		    for (var i = 0; i < tooltips.length; i++) {
		        tooltips[i].style.top = y;
		        tooltips[i].style.left = x;
		    }
		};

		$('.additionalInput input').each(function(){
			$(this).trigger('change');
		});

		app.calculatePrice();

	});
</script>

@endsection